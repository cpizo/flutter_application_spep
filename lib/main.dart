import 'package:flutter/material.dart';
import 'package:flutter_application_spep/src/ui/business.dart';
import 'package:flutter_application_spep/src/ui/configBusiness.dart';
import 'package:flutter_application_spep/src/ui/coste.dart';
import 'package:flutter_application_spep/src/ui/estimation.dart';
import 'package:flutter_application_spep/src/ui/home.dart';
import 'package:flutter_application_spep/src/ui/insertWorkTeam.dart';
import 'package:flutter_application_spep/src/ui/login.dart';
import 'package:flutter_application_spep/src/ui/historyUsers.dart';
import 'package:flutter_application_spep/src/ui/sprint.dart';
import 'package:flutter_application_spep/src/ui/workTeam.dart';
import 'package:flutter_application_spep/src/repository/getToken.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  CookieManager cookieManager = CookieManager();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'SPEP',
        theme: ThemeData(
          fontFamily: 'Poppins',
          primaryColor: Colors.white,
          floatingActionButtonTheme: FloatingActionButtonThemeData(
            elevation: 0,
            foregroundColor: Colors.white,
          ),
          accentColor: Colors.redAccent,
          textTheme: TextTheme(
            headline1: TextStyle(fontSize: 22.0, color: Colors.redAccent),
            headline2: TextStyle(
              fontSize: 24.0,
              fontWeight: FontWeight.w700,
              color: Colors.redAccent,
            ),
            bodyText1: TextStyle(
              fontSize: 14.0,
              fontWeight: FontWeight.w400,
              color: Colors.blueAccent,
            ),
          ),
        ),
        home: cookieManager.getCookie('token').length > 0 &&
                cookieManager.getCookie('token') != ''
            ? Sprint()
            : LoginPage());
  }
}
