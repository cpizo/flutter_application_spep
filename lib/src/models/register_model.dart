// import 'package:flutter_application_spep/src/models'

class Register {
  String username;
  String password;
  String massage;
  String email;

  Register(
      {this.username = '',
      this.password = '',
      this.massage = '',
      this.email = ''});

  factory Register.fromJson(Map<String, dynamic> parsedJson) {
    return Register(massage: parsedJson['message']);
  }

  Map<String, dynamic> toJson() => {
        'username': username,
        'password': password,
        'email': email,
        'role': ["$username" + ",user"]
      };
}
