// import 'package:flutter_application_spep/src/models'

class Productbacklog {
  String complejidad;
  String criterioAceptacion;
  int productbaklogId;
  String objetivo;
  String funsionalidad;
  String prioridad;
  String rol;
  int id_poryect;

  Productbacklog(
      {this.complejidad = '',
      this.criterioAceptacion = '',
      this.productbaklogId = 0,
      this.id_poryect = 0,
      this.objetivo = '',
      this.funsionalidad = '',
      this.prioridad = '',
      this.rol = ''});

  factory Productbacklog.fromJson(Map<String, dynamic> parsedJson) {
    return Productbacklog(
      complejidad: parsedJson['complejidad'],
      criterioAceptacion: parsedJson['criterio_aceptacion'],
      productbaklogId: parsedJson['productbaklog_id'],
      objetivo: parsedJson['objetivo'],
      funsionalidad: parsedJson['funsionalidad'],
      prioridad: parsedJson['prioridad'],
      rol: parsedJson['rol'],
      id_poryect: parsedJson['id_poryect'],
    );
  }

  Map<String, dynamic> toJsonRegistry() => {
        'complejidad': complejidad,
        'criterio_aceptacion': criterioAceptacion,
        'productbaklog_id': productbaklogId,
        'objetivo': objetivo,
        'funsionalidad': funsionalidad,
        'prioridad': prioridad,
        'rol': rol,
        'id_poryect': id_poryect,
      };

  Map<String, dynamic> toJsonProductbaklogID() => {
        'productbaklog_id': productbaklogId,
      };
}
