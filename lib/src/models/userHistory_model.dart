// import 'package:flutter_application_spep/src/models'

class UserHistory {
  String complejidad;
  String criterioAceptacion;
  int historiaUsuarioId;
  String funsionalidad;
  String prioridad;
  String razon;
  String nombre;
  String rol;
  String sprint;
  String encargado;
  String estado;
  int id_poryect;

  UserHistory(
      {this.complejidad = '',
      this.criterioAceptacion = '',
      this.historiaUsuarioId = 0,
      this.id_poryect = 0,
      this.funsionalidad = '',
      this.prioridad = '',
      this.razon = '',
      this.nombre = 'v',
      this.rol = '',
      this.sprint = '',
      this.estado = 'No iniciado',
      this.encargado = ''});

  factory UserHistory.fromJson(Map<String, dynamic> parsedJson) {
    return UserHistory(
        complejidad: parsedJson['complejidad'],
        criterioAceptacion: parsedJson['criterio_aceptacion'],
        historiaUsuarioId: parsedJson['id_Historia_usuario'],
        funsionalidad: parsedJson['funsionalidad'],
        prioridad: parsedJson['prioridad'],
        razon: parsedJson['razon'],
        rol: parsedJson['rol'],
        nombre: parsedJson['nombre'],
        sprint: parsedJson['sprint'],
        estado: parsedJson['estado'],
        id_poryect: parsedJson['id_poryect'],
        encargado: parsedJson['encargado']);
  }

  Map<String, dynamic> toJsonRegistry() => {
        'complejidad': complejidad,
        'criterio_aceptacion': criterioAceptacion,
        'funsionalidad': funsionalidad,
        'prioridad': prioridad,
        'razon': razon,
        'nombre': nombre,
        'rol': rol,
        'sprint': sprint,
        'estado': estado,
        'encargado': encargado,
        'id_Historia_usuario': historiaUsuarioId,
        'id_poryect': id_poryect
      };

  Map<String, dynamic> toJsonUserHistoryID() => {
        'historia_usuario_id': historiaUsuarioId,
      };
}
