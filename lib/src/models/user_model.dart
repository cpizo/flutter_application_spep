// import 'package:flutter_application_spep/src/models'

class User {
  String correo;
  String password;
  String rol;
  String username;
  int userId;
  int id_poryect;

  User(
      {this.correo = '',
      this.userId = 0,
      this.id_poryect = 0,
      this.password = '',
      this.rol = '',
      this.username = ''});

  factory User.fromJson(Map<String, dynamic> parsedJson) {
    return User(
      correo: parsedJson['correo'],
      password: parsedJson['password'],
      rol: parsedJson['rol'],
      userId: parsedJson['id_user'],
      username: parsedJson['username'],
      id_poryect: parsedJson['id_poryect'],
    );
  }

  Map<String, dynamic> toJsonRegistry() => {
        'correo': correo,
        'pass': password,
        'username': username,
        'rol': rol,
        'id_user': userId,
        'id_poryect': id_poryect,
      };

  Map<String, dynamic> toJsonUserID() => {
        'id_user': userId,
      };
}
