class Person {
  String rol;
  String direccion;
  int id_person;
  String nombre;
  String telefono;
  String valor_hora;
  int id_poryect;

  Person(
      {this.rol = '',
      this.direccion = '',
      this.id_person = 0,
      this.id_poryect = 0,
      this.nombre = '',
      this.telefono = '',
      this.valor_hora = ''});

  factory Person.fromJson(Map<String, dynamic> parsedJson) {
    return Person(
      rol: parsedJson['apellido'],
      direccion: parsedJson['direccion'],
      id_person: parsedJson['id_persona'],
      nombre: parsedJson['nombre'],
      telefono: parsedJson['telefono'],
      valor_hora: parsedJson['valor_hora'],
      id_poryect: parsedJson['id_poryect'],
    );
  }

  Map<String, dynamic> toJsonRegistry() => {
        'apellido': rol,
        'direccion': direccion,
        'id_person': id_person,
        'nombre': nombre,
        'telefono': telefono,
        'valor_hora': valor_hora,
        'id_poryect': id_poryect,
      };

  Map<String, dynamic> toJsonPersonID() => {
        'id_person': id_person,
      };
}
