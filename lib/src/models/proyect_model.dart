// import 'package:flutter_application_spep/src/models'

class Proyect {
  String duracion;
  int proyectoId;
  String historiaMax;
  String historiaMin;
  String nombre;
  String peso;
  String pivote;
  String tiempo;
  String compromiso;
  String fecha_inicio;
  String correo;
  int cant_sprint;
  int id_poryect;

  Proyect(
      {this.duracion = '',
      this.proyectoId = 0,
      this.id_poryect = 0,
      this.historiaMax = '',
      this.historiaMin = '',
      this.nombre = '',
      this.peso = '',
      this.pivote = '',
      this.tiempo = '',
      this.compromiso = '',
      this.fecha_inicio = '',
      this.correo = '',
      this.cant_sprint = 0});

  factory Proyect.fromJson(Map<String, dynamic> parsedJson) {
    return Proyect(
      duracion: parsedJson['duracion'],
      proyectoId: parsedJson['id_proyecto'],
      historiaMax: parsedJson['historia_max'],
      historiaMin: parsedJson['historia_min'],
      nombre: parsedJson['nombre'],
      peso: parsedJson['peso'],
      pivote: parsedJson['pivote'],
      tiempo: parsedJson['tiempo'],
      compromiso: parsedJson['compromiso'],
      fecha_inicio: parsedJson['fecha_inicio'],
      cant_sprint: parsedJson['cant_sprint'],
      id_poryect: parsedJson['id_poryect'],
      correo: parsedJson['correo'],
    );
  }

  Map<String, dynamic> toJsonRegistry() => {
        'duracion': duracion,
        'id_proyecto': proyectoId,
        'historia_max': historiaMax,
        'historia_min': historiaMin,
        'nombre': nombre,
        'peso': peso,
        'pivote': pivote,
        'tiempo': tiempo,
        'compromiso': compromiso,
        'fecha_inicio': fecha_inicio,
        'cant_sprint': cant_sprint,
        'id_poryect': id_poryect,
        'correo': correo,
      };

  Map<String, dynamic> toJsonProyectID() => {
        'proyecto_id': proyectoId,
      };
}
