import 'package:flutter_application_spep/src/repository/login_Api_Service.dart';
import 'package:flutter_application_spep/src/models/utils/session_storage.dart';
import 'package:flutter_application_spep/src/models/utils/session_model.dart';

class ManageToken {
  final sessionStorage = SessionStorage();
  final loginApiService = LoginAPiService();
  String? accessT = '';

//Manage local Storage Session
  Future saveLocalSessionStorage(
          Session session, String timeSession, String username) =>
      sessionStorage.saveSessionStorage(session, timeSession, username);

  Future getLocalAccessToken() async {
    return await sessionStorage.getAccessToken();
  }
}
