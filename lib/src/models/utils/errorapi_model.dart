class ErrorApiResponse {
  int status;
  String timestamp;
  String error;
  String message;
  String path;
  ErrorApiResponse(
      {this.status = 0,
      this.timestamp = '',
      this.error = '',
      this.message = '',
      this.path = ''});

  factory ErrorApiResponse.fromJson(Map<String, dynamic> json) {
    return ErrorApiResponse(
      status: json['status'],
      timestamp: json['timestamp'],
      error: json['error'],
      message: json['message'],
      path: json['path'],
    );
  }

  Map<String, dynamic> toJson() => {
        'status': status,
        'timestamp': timestamp,
        'error': error,
        'message': message,
        'path': path,
      };
}
