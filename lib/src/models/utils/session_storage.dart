import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_application_spep/src/models/utils/session_model.dart';

class SessionStorage {
  static final _storage = FlutterSecureStorage();

  Future<Map<String, String>> getAllValues() async {
    return _storage.readAll();
  }

  Future<Future<String?>> getValueforKey(String key) async {
    return _storage.read(key: key);
  }

  Future deleteValue(String key) async {
    await _storage.delete(key: key);
  }

  Future deleteAllValues() async {
    await _storage.deleteAll();
  }

  Future writeValue(String key, String value) async {
    await _storage.write(key: key, value: value);
  }

  Future saveSessionStorage(
      Session session, String timeSession, String username) async {
    await _storage.write(key: 'accessToken', value: session.token);
    await _storage.write(key: 'timeSession', value: timeSession);
    await _storage.write(key: 'username', value: username);
  }

  Future<String?> getAccessToken() async {
    return await _storage.read(key: 'accessToken');
  }
}
