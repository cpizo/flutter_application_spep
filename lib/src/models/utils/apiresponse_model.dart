class ApiResponse {
  int statusResponse;
  String message;
  Object object;

  ApiResponse({this.statusResponse = 0, this.message = '', this.object = 0});

  factory ApiResponse.fromJson(Map<String, dynamic> json) {
    return ApiResponse(
      statusResponse: json['statusResponse'],
      object: json['object'],
    );
  }

  Map<String, dynamic> toJson() => {
        'statusResponse': statusResponse,
        'object': object,
      };
}
