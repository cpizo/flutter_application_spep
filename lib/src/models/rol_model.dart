// import 'package:flutter_application_spep/src/models'

class Rol {
  String nombre;
  String rolId;
  int id_poryect;

  Rol({this.nombre = '', this.rolId = '', this.id_poryect = 0});

  factory Rol.fromJson(Map<String, dynamic> parsedJson) {
    return Rol(
      nombre: parsedJson['nombre'],
      rolId: parsedJson['rol_id'],
      id_poryect: parsedJson['id_poryect'],
    );
  }

  Map<String, dynamic> toJsonRegistry() => {
        'nombre': nombre,
        'rol_id': rolId,
        'id_poryect': id_poryect,
      };

  Map<String, dynamic> toJsonRolID() => {
        'rol_id': rolId,
      };
}
