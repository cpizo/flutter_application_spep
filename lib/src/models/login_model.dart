// import 'package:flutter_application_spep/src/models'

class Login {
  String username;
  String password;
  String token;
  int id;
  String email;

  Login(
      {this.username = '',
      this.password = '',
      this.token = '',
      this.id = 0,
      this.email = ''});

  factory Login.fromJson(Map<String, dynamic> parsedJson) {
    return Login(
        username: parsedJson['username'],
        token: parsedJson['accessToken'],
        id: parsedJson['id'],
        email: parsedJson['email']);
  }

  Map<String, dynamic> toJson() => {'username': username, 'password': password};
}
