class WorkTeam {
  String nombre;
  String rol;
  int equipoTrabajoId;
  int id_poryect;

  WorkTeam(
      {this.nombre = '',
      this.rol = '',
      this.equipoTrabajoId = 0,
      this.id_poryect = 0});

  factory WorkTeam.fromJson(Map<String, dynamic> parsedJson) {
    return WorkTeam(
      nombre: parsedJson['nombre'],
      rol: parsedJson['rol'],
      equipoTrabajoId: parsedJson['equipo_trabajo_id'],
      id_poryect: parsedJson['id_poryect'],
    );
  }

  Map<String, dynamic> toJsonRegistry() => {
        'nombre': nombre,
        'rol': rol,
        'equipo_trabajo_id': equipoTrabajoId,
        'id_poryect': id_poryect,
      };

  Map<String, dynamic> toJsonWorkTeamID() => {
        'equipo_trabajo_id': equipoTrabajoId,
      };
}
