import 'dart:convert';
import 'dart:io';
import 'package:f_logs/model/flog/flog.dart';
import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/models/utils/errorapi_model.dart';
import 'package:flutter_application_spep/src/models/person_model.dart';
import 'package:flutter_application_spep/src/repository/getToken.dart';
import 'package:flutter_application_spep/src/repository/repository/GeneralRepository.dart';
import 'package:http/http.dart' as http;

class PersonAPiService {
  PersonAPiService();

  CookieManager cookieManager = CookieManager();

  Future<ApiResponse> getAllPerson(String? accessT) async {
    final _repository = GeneralRepository();
    accessT = await _repository.getLocalAccessToken();
    var listPerson = <Person>[];
    //  List<Person> listPerson = List();
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/persona/proyecto/' + cookieManager.getCookie('id'));
    // print(cookieManager.getCookie('id'));
    var res = await http.get(url, headers: {
      HttpHeaders.authorizationHeader: "Bearer " + accessT.toString()
    });
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    // listNote = List();
    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        listPerson.add(Person.fromJson(i));
        return i;
      });

      apiResponse.object = listPerson;
      // apiResponse.object = listNote;
    } else {}

    return apiResponse;
  }

  Future<ApiResponse> getPersonbyId(int id, accessToken) async {
    Person _person;
    ErrorApiResponse _error;
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'id': id.toString(),
    };
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/person', queryParameters);
    var res = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + accessToken});

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _person = Person.fromJson(resBody);
      apiResponse.object = _person;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> insertPerson(Person _person) async {
    ErrorApiResponse _error;
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(_person.toJsonRegistry());
    Uri url = Uri.http(
        'ec2-3-87-64-108.compute-1.amazonaws.com:8080', '/api/persona');
    var res = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader:
              "Bearer " + cookieManager.getCookie('token')
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _person = Person.fromJson(resBody);

      apiResponse.object = _person;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson.toString());
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> updatePerson(Person person, accessT, id) async {
    final _repository = GeneralRepository();
    accessT = await _repository.getLocalAccessToken();
    Person _person;
    ErrorApiResponse _error;
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(person.toJsonRegistry());
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/persona/' + id.toString());
    var res = await http.put(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + accessT.toString()
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _person = Person.fromJson(resBody);
      apiResponse.object = _person;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson.toString());
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> deletePerson(id, accessT) async {
    final _repository = GeneralRepository();
    accessT = await _repository.getLocalAccessToken();
    Person _person;
    ErrorApiResponse _error;
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);

    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/persona/' + id.toString());
    var res = await http.delete(url, headers: {
      HttpHeaders.authorizationHeader: "Bearer " + accessT.toString()
    });

    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
    } else {}

    return apiResponse;
  }
}
