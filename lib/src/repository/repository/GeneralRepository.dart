import 'package:flutter_application_spep/src/models/utils/manageToken.dart';
import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/models/utils/session_storage.dart';
import 'package:flutter_application_spep/src/models/proyect_model.dart';
import 'package:flutter_application_spep/src/models/utils/session_model.dart';
import 'package:flutter_application_spep/src/repository/login_Api_Service.dart';
import 'package:flutter_application_spep/src/models/login_model.dart';
import 'package:flutter_application_spep/src/repository/userHistory_Api_Service.dart';
import 'package:flutter_application_spep/src/models/userHistory_model.dart';
import 'package:flutter_application_spep/src/models/person_model.dart';
import 'package:flutter_application_spep/src/repository/person_Api_Service .dart';
import 'package:flutter_application_spep/src/repository/proyect_Api_Service.dart';
import 'package:flutter_application_spep/src/repository/getToken.dart';

class GeneralRepository with ManageToken {
  final loginApiService = LoginAPiService();
  final useHistoryService = UserHistoryAPiService();
  final proyectService = ProyectAPiService();
  final personApiService = PersonAPiService();
  final sessionStorage = SessionStorage();
  String? _accessT = '';

  CookieManager cookieManager = CookieManager();

  //get token local storage
  Future<String?> getLocalAccessToken() async {
    _accessT = await sessionStorage.getAccessToken();
    return await cookieManager.getCookie('token');
  }

  //Save local storage
  Future<dynamic> saveLocalSessionStorage(
      Session session, String timeSession, String username) async {
    await sessionStorage.saveSessionStorage(session, timeSession, username);
  }

  //Loguin
  Future<ApiResponse> signin(Login loginObject) =>
      loginApiService.signin(loginObject, 'getLocalAccessToken()');

  //Historia de usuario
  Future<ApiResponse> insertUserHistory(UserHistory _userHistoryObject) =>
      useHistoryService.insertUserHistory(_userHistoryObject, accessT);

  Future<ApiResponse> getAllUserHistory() =>
      useHistoryService.getAllUserHistory(accessT);

  Future<ApiResponse> updateUserHistory(UserHistory _userHistoryObject) =>
      useHistoryService.updateUserHistory(
          _userHistoryObject, accessT, _userHistoryObject.historiaUsuarioId);

  Future<ApiResponse> deleteUserHistory(UserHistory _userHistoryObject) =>
      useHistoryService.deleteUserHistory(
          _userHistoryObject.historiaUsuarioId, accessT);

  //Persona
  Future<ApiResponse> insertPerson(Person _personObject) =>
      personApiService.insertPerson(_personObject);

  Future<ApiResponse> getAllPerson() async =>
      await personApiService.getAllPerson(accessT);

  Future<ApiResponse> updatePerson(Person _personObject) async =>
      await personApiService.updatePerson(
          _personObject, accessT, _personObject.id_person);

  Future<ApiResponse> deletePerson(Person _personObject) =>
      personApiService.deletePerson(_personObject.id_person, accessT);

  //Proyecto
  Future<ApiResponse> insertProyect(Proyect _proyectObject) =>
      proyectService.insertProyect(_proyectObject, accessT);

  Future<ApiResponse> updateProyect(Proyect _proyectObject) => proyectService
      .updateProyect(_proyectObject, accessT, _proyectObject.proyectoId);

  Future<ApiResponse> getAllProyect() => proyectService.getAllProyect(accessT);

  Future<ApiResponse> getProyect(Proyect _proyectObject) =>
      proyectService.getProyect(_proyectObject, accessT);

  Future<ApiResponse> deleteProyect(Proyect _proyectObject) =>
      proyectService.deleteProyecto(_proyectObject.proyectoId, accessT);
}
