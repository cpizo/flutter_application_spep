import 'dart:convert';
import 'dart:io';
import 'package:f_logs/model/flog/flog.dart';
import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/models/utils/errorapi_model.dart';
import 'package:flutter_application_spep/src/models/productbacklog_model.dart';
import 'package:flutter_application_spep/src/repository/getToken.dart';
import 'package:http/http.dart' as http;

class ProductbacklogAPiService {
  Productbacklog _productbacklog;
  ErrorApiResponse _error;
  ProductbacklogAPiService(this._error, this._productbacklog);
  CookieManager cookieManager = CookieManager();

  Future<ApiResponse> getAllProductbacklog(String accessToken) async {
    //  List<Person> listPerson = List();
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/productbacklog/proyecto/' + cookieManager.getCookie('id'));
    var res = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + accessToken});
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    // listNote = List();
    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        // listNote.add(Note.fromJson(i));
        return i;
      });
      // apiResponse.object = listNote;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> getProductbacklogbyId(int id, accessToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'id': id.toString(),
    };
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/productbacklog', queryParameters);
    var res = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + accessToken});

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _productbacklog = Productbacklog.fromJson(resBody);
      apiResponse.object = _productbacklog;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> insertProductbacklog(
      Productbacklog productbacklog, String accessToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(productbacklog.toJsonRegistry());
    Uri url = Uri.http(
        'ec2-3-87-64-108.compute-1.amazonaws.com:8080', '/api/productbacklog');
    var res = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "aplication/json",
          HttpHeaders.authorizationHeader: "Bearer " + accessToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _productbacklog = Productbacklog.fromJson(resBody);
      apiResponse.object = _productbacklog;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson.toString());
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> updateProductbacklog(
      Productbacklog productbacklog, String accessToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(productbacklog.toJsonRegistry());
    Uri url = Uri.http(
        'ec2-3-87-64-108.compute-1.amazonaws.com:8080', '/api/productbacklog');
    var res = await http.put(url,
        headers: {
          HttpHeaders.contentTypeHeader: "aplication/json",
          HttpHeaders.authorizationHeader: "Bearer " + accessToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _productbacklog = Productbacklog.fromJson(resBody);
      apiResponse.object = _productbacklog;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson.toString());
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteProductbacklog(int id, accessToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'id': id.toString(),
    };
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/productbacklog', queryParameters);
    var res = await http.delete(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + accessToken});

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _productbacklog = Productbacklog.fromJson(resBody);
      apiResponse.object = _productbacklog;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }
}
