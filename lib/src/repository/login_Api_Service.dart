import 'dart:convert';
import 'dart:html';
import 'dart:io';
import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/models/login_model.dart';
import 'package:flutter_application_spep/src/models/utils/session_model.dart';
import 'package:flutter_application_spep/src/repository/repository/GeneralRepository.dart';
import 'package:http/http.dart' as http;

class LoginAPiService {
  LoginAPiService();

  Future<ApiResponse> signin(Login _login, String accesT) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);

    final _repository = GeneralRepository();

    var body = json.encode(_login.toJson());
    Uri uri = Uri.http(
        "ec2-3-87-64-108.compute-1.amazonaws.com:8080", '/api/auth/signin');
    var res = await http.post(uri,
        headers: {HttpHeaders.contentTypeHeader: "application/json"},
        body: body);
    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    //document.cookie = "token2=exassdddda; max-age=360;";
    if (apiResponse.statusResponse == 200) {
      _login = Login.fromJson(resBody);
      apiResponse.object = _login;
      var token = _login.token;
      var idUser = _login.id;
      var emailUser = _login.email;
      var userName = _login.username;
      //print(res.body);
      Session tokenSesion = Session(token: token);
      _repository.saveLocalSessionStorage(
          tokenSesion, '36000000', 'pizocristian');
      document.cookie = "token=$token; max-age=3600;";

      _repository.saveLocalSessionStorage(
          tokenSesion, '36000000', 'pizocristian');
      document.cookie = "id=$idUser; max-age=3600;";

      _repository.saveLocalSessionStorage(
          tokenSesion, '36000000', 'pizocristian');
      document.cookie = "email=$emailUser; max-age=3600;";

      _repository.saveLocalSessionStorage(
          tokenSesion, '36000000', 'pizocristian');
      document.cookie = "userName=$userName; max-age=3600;";
    } else {
      apiResponse.statusResponse == 400;
    }
    return apiResponse;
  }
}
