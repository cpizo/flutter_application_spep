import 'dart:convert';
import 'dart:io';
import 'package:f_logs/model/flog/flog.dart';
import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/models/utils/errorapi_model.dart';
import 'package:flutter_application_spep/src/models/rol_model.dart';
import 'package:flutter_application_spep/src/repository/getToken.dart';
import 'package:http/http.dart' as http;

class RolAPiService {
  Rol _rol;
  ErrorApiResponse _error;
  RolAPiService(this._error, this._rol);
  CookieManager cookieManager = CookieManager();

  Future<ApiResponse> getAllRol(String accessToken) async {
    //  List<Rol> listRol = List();
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/rol/proyecto/' + cookieManager.getCookie('id'));
    var res = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + accessToken});
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    // listRol = List();
    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        // listRol.add(Rol.fromJson(i));
        return i;
      });
      // apiResponse.object = listRol;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> getRolbyId(int id, accessToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'id': id.toString(),
    };
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/rol', queryParameters);
    var res = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + accessToken});

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _rol = Rol.fromJson(resBody);
      apiResponse.object = _rol;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> insertRol(Rol rol, String accessToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(rol.toJsonRegistry());
    Uri url =
        Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080', '/api/rol');
    var res = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "aplication/json",
          HttpHeaders.authorizationHeader: "Bearer " + accessToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _rol = Rol.fromJson(resBody);
      apiResponse.object = _rol;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson.toString());
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> updateRol(Rol rol, String accessToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(rol.toJsonRegistry());
    Uri url =
        Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080', '/api/rol');
    var res = await http.put(url,
        headers: {
          HttpHeaders.contentTypeHeader: "aplication/json",
          HttpHeaders.authorizationHeader: "Bearer " + accessToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _rol = Rol.fromJson(resBody);
      apiResponse.object = _rol;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson.toString());
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteRol(int id, accessToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'id': id.toString(),
    };
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/rol', queryParameters);
    var res = await http.delete(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + accessToken});

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _rol = Rol.fromJson(resBody);
      apiResponse.object = _rol;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }
}
