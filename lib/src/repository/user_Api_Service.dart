import 'dart:convert';
import 'dart:io';
import 'package:f_logs/model/flog/flog.dart';
import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/models/utils/errorapi_model.dart';
import 'package:flutter_application_spep/src/models/user_model.dart';
import 'package:flutter_application_spep/src/repository/getToken.dart';
import 'package:http/http.dart' as http;

class UserAPiService {
  User _user;
  ErrorApiResponse _error;
  UserAPiService(this._error, this._user);
  CookieManager cookieManager = CookieManager();

  Future<ApiResponse> getAllUser(String accessToken) async {
    //  List<User> listUser = List();
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/apis/user/proyecto/' + cookieManager.getCookie('id'));
    var res = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + accessToken});
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    // listUser = List();
    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        // listUser.add(User.fromJson(i));
        return i;
      });
      // apiResponse.object = listUser;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> getUserbyId(int id, accessToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'id': id.toString(),
    };
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/apis/user', queryParameters);
    var res = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + accessToken});

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _user = User.fromJson(resBody);
      apiResponse.object = _user;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> insertUser(User user, String accessToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(user.toJsonRegistry());
    Uri url =
        Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080', '/apis/user');
    var res = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "aplication/json",
          HttpHeaders.authorizationHeader: "Bearer " + accessToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _user = User.fromJson(resBody);
      apiResponse.object = _user;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson.toString());
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> updateUser(User user, String accessToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(user.toJsonRegistry());
    Uri url =
        Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080', '/apis/user');
    var res = await http.put(url,
        headers: {
          HttpHeaders.contentTypeHeader: "aplication/json",
          HttpHeaders.authorizationHeader: "Bearer " + accessToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _user = User.fromJson(resBody);
      apiResponse.object = _user;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson.toString());
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteUser(int id, accessToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'id': id.toString(),
    };
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/apis/user', queryParameters);
    var res = await http.delete(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + accessToken});

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _user = User.fromJson(resBody);
      apiResponse.object = _user;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }
}
