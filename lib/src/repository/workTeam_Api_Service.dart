import 'dart:convert';
import 'dart:io';
import 'package:f_logs/model/flog/flog.dart';
import 'package:flutter_application_spep/src/models/person_model.dart';
import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/models/utils/errorapi_model.dart';
import 'package:flutter_application_spep/src/models/workTeam_model.dart';
import 'package:flutter_application_spep/src/repository/getToken.dart';
import 'package:http/http.dart' as http;

class WorkTeamAPiService {
  WorkTeamAPiService();

  CookieManager cookieManager = CookieManager();

  Future<ApiResponse> getAllWorkTeam() async {
    var listWorkTeam = <WorkTeam>[];
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url =
        Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080', '/api/equipo');
    var res = await http.get(url, headers: {
      HttpHeaders.authorizationHeader:
          "Bearer " + cookieManager.getCookie('token')
    });
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    // listWorkTeam = List();
    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        listWorkTeam.add(WorkTeam.fromJson(i));

        //print(i);
        return i;
      });
      apiResponse.object = listWorkTeam;
    } else {}

    return apiResponse;
  }

  Future<ApiResponse> getWorkTeambyId(int id, accessToken) async {
    WorkTeam _workTeam;
    ErrorApiResponse _error;
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'id': id.toString(),
    };
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/equipo', queryParameters);
    var res = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + accessToken});

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _workTeam = WorkTeam.fromJson(resBody);
      apiResponse.object = _workTeam;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> insertWorkTeam(Person _person) async {
    ErrorApiResponse _error;
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(_person.toJsonRegistry());
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/persona/proyecto/' + cookieManager.getCookie('id'));
    // print(body);
    var res = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader:
              "Bearer " + cookieManager.getCookie('token')
        },
        body: body);

    //var resBody = json.decode(res.body);
    //apiResponse.statusResponse = res.statusCode;
    //if (apiResponse.statusResponse == 200) {
    //_person = Person.fromJson(resBody);
    //apiResponse.object = _person;
    //} else {
    // _error = ErrorApiResponse.fromJson(resBody);
    //FLog.error(text: _error.toJson.toString());
    //apiResponse.object = _error;
    //}

    return apiResponse;
  }

  Future<ApiResponse> updateWorkTeam(
      WorkTeam workTeam, String accessToken) async {
    WorkTeam _workTeam;
    ErrorApiResponse _error;
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(workTeam.toJsonRegistry());
    Uri url =
        Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080', '/api/equipo');
    var res = await http.put(url,
        headers: {
          HttpHeaders.contentTypeHeader: "aplication/json",
          HttpHeaders.authorizationHeader: "Bearer " + accessToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _workTeam = WorkTeam.fromJson(resBody);
      apiResponse.object = _workTeam;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson.toString());
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteWorkTeam(int id, accessToken) async {
    WorkTeam _workTeam;
    ErrorApiResponse _error;
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'id': id.toString(),
    };
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/equipo', queryParameters);
    var res = await http.delete(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + accessToken});

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _workTeam = WorkTeam.fromJson(resBody);
      apiResponse.object = _workTeam;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }
}
