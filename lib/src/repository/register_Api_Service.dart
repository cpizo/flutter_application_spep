import 'dart:convert';
import 'dart:io';
import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/models/register_model.dart';
import 'package:http/http.dart' as http;

class RegisterAPiService {
  Register _register;
  RegisterAPiService(this._register);

  Future<ApiResponse> signup() async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);

    var body = json.encode(_register.toJson());
    Uri uri = Uri.http(
        "ec2-3-87-64-108.compute-1.amazonaws.com:8080", '/api/auth/signup');
    var res = await http.post(uri,
        headers: {HttpHeaders.contentTypeHeader: "application/json"},
        body: body);
    print(body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _register = Register.fromJson(resBody);
      apiResponse.object = _register;
    } else {}
    return apiResponse;
  }

  Future<ApiResponse> updatePassword() async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);

    var body = json.encode(_register.toJson());
    Uri uri = Uri.http("ec2-3-87-64-108.compute-1.amazonaws.com:8080",
        '/api/auth/usernameupdate/1');
    var res = await http.put(uri,
        headers: {HttpHeaders.contentTypeHeader: "application/json"},
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _register = Register.fromJson(resBody);
      apiResponse.object = _register;
    } else {}
    return apiResponse;
  }
}
