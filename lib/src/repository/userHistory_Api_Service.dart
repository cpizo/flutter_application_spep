import 'dart:convert';
import 'dart:io';
import 'package:f_logs/model/flog/flog.dart';
import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/models/utils/errorapi_model.dart';
import 'package:flutter_application_spep/src/models/userHistory_model.dart';
import 'package:flutter_application_spep/src/repository/getToken.dart';
import 'package:flutter_application_spep/src/repository/repository/GeneralRepository.dart';
import 'package:http/http.dart' as http;
import 'dart:html';
import 'dart:convert' show utf8;

class UserHistoryAPiService {
  UserHistoryAPiService();
  CookieManager cookieManager = CookieManager();

  Future<ApiResponse> getAllUserHistory(String? accessT) async {
    final _repository = GeneralRepository();
    var accessT = await _repository.getLocalAccessToken();

    var listUserHistory = <UserHistory>[];
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/historia/proyecto/' + cookieManager.getCookie('id'));
    var res = await http.get(url, headers: {
      HttpHeaders.authorizationHeader: "Bearer  " + accessT.toString()
    });

    var resBody = json.decode(utf8.decode(res.bodyBytes));
    apiResponse.statusResponse = res.statusCode;

    // listUserHistory = List();
    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        listUserHistory.add(UserHistory.fromJson(i));
        //print(i);
        return i;
      });

      apiResponse.object = listUserHistory;
    } else {}

    return apiResponse;
  }

  Future<ApiResponse> getUserHistorybyId(UserHistory _userHistory) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'id': _userHistory.toString(),
    };
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/historia', queryParameters);
    var res = await http
        .get(url, headers: {HttpHeaders.authorizationHeader: "Bearer "});

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _userHistory = UserHistory.fromJson(resBody);
      apiResponse.object = _userHistory;
    } else {}

    return apiResponse;
  }

  Future<ApiResponse> insertUserHistory(_userHistory, String? accessT) async {
    final _repository = GeneralRepository();
    accessT = await _repository.getLocalAccessToken();

    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(_userHistory.toJsonRegistry());
    Uri uri = Uri.http(
        'ec2-3-87-64-108.compute-1.amazonaws.com:8080', '/api/historia');
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + accessT.toString()
        },
        body: body);
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _userHistory = UserHistory.fromJson(resBody);
      apiResponse.object = _userHistory;
    } else {}

    return apiResponse;
  }

  Future<ApiResponse> updateUserHistory(
      _userHistory, String? accessT, id) async {
    final _repository = GeneralRepository();
    accessT = await _repository.getLocalAccessToken();

    ErrorApiResponse _error;
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(_userHistory.toJsonRegistry());
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/historia/' + id.toString());
    var res = await http.put(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + accessT.toString()
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _userHistory = UserHistory.fromJson(resBody);
      apiResponse.object = _userHistory;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson.toString());
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteUserHistory(id, String? accessT) async {
    final _repository = GeneralRepository();
    accessT = await _repository.getLocalAccessToken();
    UserHistory _userHistory;

    ApiResponse apiResponse = ApiResponse(statusResponse: 0);

    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/historia/' + id.toString());
    var res = await http.delete(url, headers: {
      HttpHeaders.authorizationHeader: "Bearer " + accessT.toString()
    });

    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
    } else {}

    return apiResponse;
  }
}
