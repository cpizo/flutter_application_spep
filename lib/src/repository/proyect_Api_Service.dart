import 'dart:convert';
import 'dart:io';
import 'package:f_logs/model/flog/flog.dart';
import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/models/proyect_model.dart';
import 'package:flutter_application_spep/src/models/utils/errorapi_model.dart';
import 'package:flutter_application_spep/src/repository/getToken.dart';
import 'package:flutter_application_spep/src/repository/repository/GeneralRepository.dart';
import 'package:http/http.dart' as http;

class ProyectAPiService {
  ProyectAPiService();

  Future<ApiResponse> getAllProyect(String? accessT) async {
    final _repository = GeneralRepository();
    var accessT = await _repository.getLocalAccessToken();
    CookieManager cookieManager = CookieManager();

    var listProyect = <Proyect>[];
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/proyecto/proyecto/' + cookieManager.getCookie('id'));
    var res = await http.get(url, headers: {
      HttpHeaders.authorizationHeader: "Bearer " + accessT.toString()
    });

    var resBody = json.decode(utf8.decode(res.bodyBytes));

    apiResponse.statusResponse = res.statusCode;

    // listNote = List();
    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        listProyect.add(Proyect.fromJson(i));
        return i;
      });
      apiResponse.object = listProyect;
    } else {}

    return apiResponse;
  }

  Future<ApiResponse> getProyectobyId(int id, accessToken, _proyect) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'id': id.toString(),
    };
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/proyecto', queryParameters);
    var res = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + accessToken});

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _proyect = Proyect.fromJson(resBody);
      apiResponse.object = _proyect;
    } else {}

    return apiResponse;
  }

  Future<ApiResponse> insertProyect(_proyect, String? accessT) async {
    final _repository = GeneralRepository();
    accessT = await _repository.getLocalAccessToken();

    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(_proyect.toJsonRegistry());
    Uri url = Uri.http(
        'ec2-3-87-64-108.compute-1.amazonaws.com:8080', '/api/proyecto');
    var res = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + accessT.toString()
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _proyect = Proyect.fromJson(resBody);
      apiResponse.object = _proyect;
    } else {}

    return apiResponse;
  }

  Future<ApiResponse> getProyect(_proyect, String? accessT) async {
    final _repository = GeneralRepository();
    accessT = await _repository.getLocalAccessToken();

    var listProyect = <Proyect>[];

    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(_proyect.toJsonRegistry());
    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/proyecto/proyecto');
    var res = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + accessT.toString()
        },
        body: body);

    var resBody = json.decode(utf8.decode(res.bodyBytes));

    apiResponse.statusResponse = res.statusCode;

    // listNote = List();
    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        listProyect.add(Proyect.fromJson(i));
        return i;
      });
      apiResponse.object = listProyect;
    } else {}

    return apiResponse;
  }

  Future<ApiResponse> updateProyect(_proyect, String? accessT, id) async {
    final _repository = GeneralRepository();
    accessT = await _repository.getLocalAccessToken();

    ErrorApiResponse _error;
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(_proyect.toJsonRegistry());

    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/proyecto/' + id.toString());
    var res = await http.put(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + accessT.toString()
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _proyect = Proyect.fromJson(resBody);
      apiResponse.object = _proyect;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson.toString());
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteProyecto(
    int id,
    accessT,
  ) async {
    final _repository = GeneralRepository();
    accessT = await _repository.getLocalAccessToken();
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);

    Uri url = Uri.http('ec2-3-87-64-108.compute-1.amazonaws.com:8080',
        '/api/proyecto/' + id.toString());
    var res = await http.delete(url, headers: {
      HttpHeaders.authorizationHeader: "Bearer " + accessT.toString()
    });

    apiResponse.statusResponse = res.statusCode;

    return apiResponse;
  }
}
