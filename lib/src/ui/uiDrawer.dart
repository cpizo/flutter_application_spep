import 'package:flutter/material.dart';
import 'package:flutter_application_spep/src/repository/getToken.dart';
import 'package:flutter_application_spep/src/ui/coste.dart';
import 'package:flutter_application_spep/src/ui/estimation.dart';
import 'package:flutter_application_spep/src/ui/login.dart';
import 'package:flutter_application_spep/src/ui/home.dart';
import 'package:flutter_application_spep/src/ui/workTeam.dart';
import 'package:flutter_application_spep/src/ui/historyUsers.dart';
import 'package:flutter_application_spep/src/ui/configBusiness.dart';
import 'package:flutter_application_spep/src/ui/sprint.dart';
import 'dart:html';

class UiDrawer extends StatelessWidget {
  CookieManager cookieManager = CookieManager();
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(cookieManager.getCookie('userName')),
            accountEmail: Text(cookieManager.getCookie('email')),
            currentAccountPicture: FlutterLogo(),
            otherAccountsPictures: <Widget>[FlutterLogo()],
            onDetailsPressed: () {},
            decoration: BoxDecoration(
                gradient:
                    LinearGradient(colors: [Colors.deepOrange, Colors.black])),
          ),
          ListTile(
            title: Text("HOME"),
            leading: Icon(Icons.home),
            onTap: () => showHome(context),
          ),
          ListTile(
            title: Text("Equipo de trabajo"),
            leading: Icon(Icons.account_tree_outlined),
            onTap: () => showWork(context),
          ),
          ListTile(
            title: Text("Product Backlog"),
            leading: Icon(Icons.verified_user_outlined),
            onTap: () => showHU(context),
          ),
          ListTile(
            title: Text("Estimación y Priorización"),
            leading: Icon(Icons.addchart),
            onTap: () => showEstimation(context),
          ),
          ListTile(
            title: Text("Sprint"),
            leading: Icon(Icons.task_alt_outlined),
            onTap: () => showSprint(context),
          ),
          ListTile(
            title: Text("Costos del proyecto"),
            leading: Icon(Icons.attach_money),
            onTap: () => showCoste(context),
          ),
          ListTile(
            title: Text("Configuración del proyecto"),
            leading: Icon(Icons.business),
            onTap: () => showBusinee(context),
          ),
          ListTile(
            title: Text("Cerrar session"),
            leading: Icon(Icons.login),
            onTap: () => showLogin(context),
          )
        ],
      ),
    );
  }

  void showHome(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => Home(),
    ));
  }

  void showEstimation(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => Estimation(),
    ));
  }

  void showHU(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => HistoryUsers(),
    ));
  }

  void showWork(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => WorkTeam(),
    ));
  }

  void showLogin(BuildContext context) {
    document.cookie = "token=; max-age=0;";
    document.cookie = "id=; max-age=0;";
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => LoginPage(),
    ));
  }

  void showSprint(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => Sprint(),
    ));
  }

  void showCoste(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => Coste(),
    ));
  }

  void showBusinee(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => ConfigBusiness(),
    ));
  }
}
