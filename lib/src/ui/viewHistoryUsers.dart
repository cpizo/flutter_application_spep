import 'package:flutter/material.dart';
import 'package:flutter_application_spep/src/ui/historyUsers.dart';
import 'package:flutter_application_spep/src/ui/uiDrawer.dart';
import 'package:flutter_application_spep/src/bloc/historyUser_bloc.dart';
import 'package:flutter_application_spep/src/models/userHistory_model.dart';

class ViewHistoryUsers extends StatefulWidget {
  var data;
  ViewHistoryUsers({this.data});
  static const routeName = '/extractArguments';
  @override
  _ViewHistoryUsersState createState() => _ViewHistoryUsersState();
}

class UserData {
  String id = '';
}

class _ViewHistoryUsersState extends State<ViewHistoryUsers> {
  final scaffolKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> globalFormkey = new GlobalKey<FormState>();
  bool hidePassword = true;

  UiDrawer uiDrawer = UiDrawer();
  UserData user = UserData();
  final HistoryUserBloc historyUserBloc = HistoryUserBloc();
  final fieldTextRequiero = TextEditingController();
  final fieldTextFinalidad = TextEditingController();
  final fieldTextCriterios = TextEditingController();
  List<String> itemRol = ['cliente'];
  String rol = 'cliente';
  String funsionalidad = '';
  String razon = '';
  String criterios = '';
  List<String> itemComplejidad = ['1', '2', '3', '5', '8', '13', '21', '?'];
  String complejidad = '1';
  List<String> itemPrioridad = ['S', 'M', 'C', 'W'];
  String prioridad = 'S';
  List<UserHistory> userHistory = [];
  var _TextControllerRequiero;

  @override
  void initState() {
    //  print(widget.data);
    getAllHU();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 224, 229, 255),
      key: scaffolKey,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        centerTitle: true,
        title: Text("Product Backlog"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Center(
              child: Container(
                width: 1200,
                padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                margin: EdgeInsets.symmetric(vertical: 45, horizontal: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Color.fromARGB(255, 255, 255, 255),
                  boxShadow: [
                    BoxShadow(
                        color: Theme.of(context).hintColor.withOpacity(0.2),
                        offset: Offset(0, 10),
                        blurRadius: 20)
                  ],
                ),
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(bottom: 25),
                      child: Row(
                        children: [
                          Text('Ver Historia de usuarios',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 25,
                              )),
                        ],
                      ),
                    ),
                    StreamBuilder(
                      stream: historyUserBloc.userHistory,
                      builder: (BuildContext contex,
                          AsyncSnapshot<dynamic> snapData) {
                        if (!snapData.hasData) {
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                        return cargarHU(snapData.data, historyUserBloc, contex);
                      },
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      drawer: uiDrawer,
    );
  }

  void getAllHU() {
    historyUserBloc.getAllHU();
    historyUserBloc.userHistory.listen((List<UserHistory> userHistory) {
      //print(userHistory[0].complejidad);
      this.userHistory = userHistory;
    });
  }

  Widget cargarHU(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    var hu;

    snapData.forEach((userHistory) {
      if (userHistory.historiaUsuarioId.toString() == widget.data) {
        hu = userHistory;
      }
    });

    return Column(children: [
      Row(
        children: [
          Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Rol',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                  ),
                  Container(
                    width: 450,
                    margin: const EdgeInsets.only(bottom: 5),
                    child: DropdownButton<String>(
                      value: rol,
                      items:
                          itemRol.map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: null,
                      hint: Text("Select item"),
                      disabledHint: Text("Disabled"),
                      elevation: 8,
                      style: TextStyle(
                          color: Color.fromARGB(255, 76, 94, 175),
                          fontSize: 16),
                      icon: Icon(Icons.arrow_drop_down_circle),
                      iconDisabledColor: Colors.red,
                      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
                      isExpanded: true,
                    ),
                  ),
                  Container(
                    width: 450,
                    margin: const EdgeInsets.only(bottom: 15),
                    child: TextFormField(
                      readOnly: true,
                      initialValue: hu.funsionalidad,
                      onChanged: (text) {
                        hu.funsionalidad = text;
                      },
                      decoration: InputDecoration(
                          labelText:
                              'Requiero/ necesito/ deseo/ espero/ quiero:'),
                    ),
                  ),
                  Container(
                    width: 450,
                    child: TextFormField(
                      readOnly: true,
                      initialValue: hu.razon,
                      onChanged: (text) {
                        hu.razon = text;
                      },
                      decoration:
                          InputDecoration(labelText: "Con la finalidad de:"),
                    ),
                  )
                ],
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: 600,
                padding: const EdgeInsets.all(20),
                margin: const EdgeInsets.only(left: 60),
                child: TextFormField(
                    readOnly: true,
                    initialValue: hu.criterioAceptacion,
                    onChanged: (text) {
                      hu.criterioAceptacion = text;
                    },
                    keyboardType: TextInputType.multiline,
                    maxLines: 10,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: "Ingrese criterios de aceptación:",
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                width: 1,
                                color: Color.fromARGB(255, 82, 128, 255))))),
              )
            ],
          ),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [],
      )
    ]);
  }

  bool validar() {
    if (funsionalidad != '' && razon != '' && criterios != '') {
      return true;
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Error"),
              content: Text("Llene todos los campos"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Ok'))
              ],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
            );
          },
          barrierDismissible: false);
      return false;
    }
  }

  void editHU(hu) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Alerta"),
            content: Text("Desear actualizar la historia de usuario?"),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Cancelar')),
              FlatButton(
                  onPressed: () {
                    historyUserBloc.updateUserHistory(
                      hu.rol,
                      hu.funsionalidad,
                      hu.razon,
                      hu.criterioAceptacion,
                      hu.complejidad,
                      hu.prioridad,
                      hu.sprint,
                      hu.encargado,
                      hu.estado,
                      hu.historiaUsuarioId,
                    );
                    getAllHU();
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => HistoryUsers()),
                    );
                  },
                  child: Text('Actualizar'))
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          );
        },
        barrierDismissible: false);
  }

  Widget btnEdit(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    return FlatButton(
      padding: EdgeInsets.symmetric(
        vertical: 22,
        horizontal: 48,
      ),
      onPressed: () {},
      child: Text(
        'Calular',
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).cursorColor,
      shape: StadiumBorder(),
    );
  }
}
