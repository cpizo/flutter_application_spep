import 'package:flutter/material.dart';
import 'package:flutter_application_spep/src/models/proyect_model.dart';
import 'package:flutter_application_spep/src/ui/uiDrawer.dart';
import 'package:flutter_application_spep/src/ui/tableHU.dart';
import 'package:flutter_application_spep/src/bloc/historyUser_bloc.dart';
import 'package:flutter_application_spep/src/bloc/proyect_bloc.dart';
import 'package:flutter_application_spep/src/bloc/proyect_bloc2.dart';
import 'package:flutter_application_spep/src/models/userHistory_model.dart';
import 'dart:html';

class Estimation extends StatefulWidget {
  @override
  _EstimationState createState() => _EstimationState();
}

class UserData {
  String username = '';
  String password = '';
}

class _EstimationState extends State<Estimation> {
  final scaffolKey = GlobalKey<ScaffoldState>();

  GlobalKey<FormState> globalFormkey = new GlobalKey<FormState>();
  bool hidePassword = true;

  UiDrawer uiDrawer = UiDrawer();
  UserData user = UserData();
  TableHU tableHU = TableHU();
  final HistoryUserBloc historyUserBloc = HistoryUserBloc();
  final ProyectBloc proyectBloc = ProyectBloc();
  final ProyectBloc2 proyectBloc2 = ProyectBloc2();
  final fieldTextRequiero = TextEditingController();
  final fieldTextFinalidad = TextEditingController();
  final fieldTextCriterios = TextEditingController();
  final fieldTextCantidadSprints = TextEditingController();
  final fieldTextCompromiso = TextEditingController();
  List<String> itemRol = ['1', '2', '3', '5', '8', '13', '21', '?'];
  String rol = '1';
  String funsionalidad = '';
  String razon = '';
  String criterios = '';
  List<String> itemComplejidad = ['1', '2', '3', '5', '8', '13', '21', '?'];
  String complejidad = '1';
  List<String> itemPrioridad = ['S', 'M', 'C'];
  String prioridad = 'S';
  List<String> itemDuration = ['1', '2', '3', '4'];
  String duration = '1';
  List<UserHistory> userHistory = [];
  List<Proyect> proyect = [];
  int pesoProyecto = 0;
  int compromisoSprint = 0;
  String cantidadSprints = '';
  String duracion = '';
  String equipoTrabajo = '';
  String historiaMax = '';
  String historiaMin = '';
  String nombre = '';
  String peso = '';
  String pivote = '';
  String tiempo = '1';
  String compromiso = '1';
  int cant_sprint = 0;
  var Seleccione = 'Seleccione';
  List<Proyect> proyectList = [];

  DateTime selectedDate = DateTime.now();

  initialUserHistory() {
    historyUserBloc.getAllHU();
  }

  @override
  void initState() {
    getAllHU();
    pintar();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 224, 229, 255),
      key: scaffolKey,
      appBar: AppBar(
        title: Text("Estimación y priorización"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.update),
            onPressed: () {
              getAllHU();
              pintar();
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: FittedBox(
          child: Column(
            children: <Widget>[
              Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                Container(
                  width: 700,
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.symmetric(vertical: 40, horizontal: 20),
                  margin: EdgeInsets.symmetric(vertical: 35, horizontal: 80),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                          color: Theme.of(context).hintColor.withOpacity(0.2),
                          offset: Offset(0, 10),
                          blurRadius: 20)
                    ],
                  ),
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              'SELECCIONE EL PROYECTO',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 15),
                            ),
                            StreamBuilder(
                              stream: proyectBloc2.proyects,
                              builder: (BuildContext contex,
                                  AsyncSnapshot<dynamic> snapData) {
                                if (!snapData.hasData) {
                                  return Center(
                                    child: CircularProgressIndicator(),
                                  );
                                }
                                return nombreProyecto(
                                    snapData.data, proyectBloc2, contex);
                              },
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(bottom: 25),
                        child: Row(
                          children: [
                            Text('Historia de usuarios',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25,
                                )),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          StreamBuilder(
                            stream: historyUserBloc.userHistory,
                            builder: (BuildContext contex,
                                AsyncSnapshot<dynamic> snapData) {
                              if (!snapData.hasData) {
                                return Center(
                                  child: CircularProgressIndicator(),
                                );
                              }
                              return cargaLista(
                                  snapData.data, historyUserBloc, contex);
                            },
                          ),
                        ],
                      ),
                      StreamBuilder(
                        stream: proyectBloc.proyect,
                        builder: (BuildContext contex,
                            AsyncSnapshot<dynamic> snapData) {
                          if (!snapData.hasData) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                          return cargaData(snapData.data, proyectBloc, contex);
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  width: 800,
                  padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                  margin: EdgeInsets.symmetric(vertical: 45, horizontal: 20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Color.fromARGB(255, 255, 255, 255),
                    boxShadow: [
                      BoxShadow(
                          color: Theme.of(context).hintColor.withOpacity(0.2),
                          offset: Offset(0, 10),
                          blurRadius: 20)
                    ],
                  ),
                  child: FittedBox(
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(bottom: 25),
                          child: Row(
                            children: [
                              Text(
                                  'Datos de estimación, priorización y tamaño del proyecto',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 25,
                                  )),
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            Column(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'HISTORIA MÍNIMA',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15),
                                    ),
                                    Container(
                                      width: 250,
                                      margin: const EdgeInsets.only(bottom: 5),
                                      child: StreamBuilder(
                                        stream: historyUserBloc.userHistory,
                                        builder: (BuildContext contex,
                                            AsyncSnapshot<dynamic> snapData) {
                                          if (!snapData.hasData) {
                                            return Center(
                                              child:
                                                  CircularProgressIndicator(),
                                            );
                                          }
                                          return cargaHUMin(snapData.data,
                                              historyUserBloc, contex);
                                        },
                                      ),
                                    ),
                                    Text(
                                      'HISTORIA MÁXIMA',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15),
                                    ),
                                    Container(
                                      width: 250,
                                      margin: const EdgeInsets.only(bottom: 5),
                                      child: StreamBuilder(
                                        stream: historyUserBloc.userHistory,
                                        builder: (BuildContext contex,
                                            AsyncSnapshot<dynamic> snapData) {
                                          if (!snapData.hasData) {
                                            return Center(
                                              child:
                                                  CircularProgressIndicator(),
                                            );
                                          }
                                          return cargaHUMax(snapData.data,
                                              historyUserBloc, contex);
                                        },
                                      ),
                                    ),
                                    Text(
                                      'PIVOTE',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15),
                                    ),
                                    Container(
                                      width: 250,
                                      margin: const EdgeInsets.only(bottom: 5),
                                      child: StreamBuilder(
                                        stream: historyUserBloc.userHistory,
                                        builder: (BuildContext contex,
                                            AsyncSnapshot<dynamic> snapData) {
                                          if (!snapData.hasData) {
                                            return Center(
                                              child:
                                                  CircularProgressIndicator(),
                                            );
                                          }
                                          return cargaPivote(snapData.data,
                                              historyUserBloc, contex);
                                        },
                                      ),
                                    ),
                                    Text(
                                      'DURACIÓN DEL SPRINT',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15),
                                    ),
                                    Container(
                                      width: 250,
                                      margin: const EdgeInsets.only(bottom: 5),
                                      child: StreamBuilder(
                                        stream: historyUserBloc.userHistory,
                                        builder: (BuildContext contex,
                                            AsyncSnapshot<dynamic> snapData) {
                                          if (!snapData.hasData) {
                                            return Center(
                                              child:
                                                  CircularProgressIndicator(),
                                            );
                                          }
                                          return cargaTiempo(snapData.data,
                                              historyUserBloc, contex);
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Container(
                              width: 300,
                              padding: const EdgeInsets.all(20),
                              margin: const EdgeInsets.only(left: 60),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'PESO DEL PROYECTO',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15),
                                  ),
                                  StreamBuilder(
                                    stream: historyUserBloc.userHistory,
                                    builder: (BuildContext contex,
                                        AsyncSnapshot<dynamic> snapData) {
                                      if (!snapData.hasData) {
                                        return Center(
                                          child: CircularProgressIndicator(),
                                        );
                                      }
                                      return cargaText(snapData.data,
                                          historyUserBloc, contex);
                                    },
                                  ),
                                  Text(
                                    'COMPROMISO POR SPRINT',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15),
                                  ),
                                  StreamBuilder(
                                    stream: proyectBloc.proyect,
                                    builder: (BuildContext contex,
                                        AsyncSnapshot<dynamic> snapData) {
                                      if (!snapData.hasData) {
                                        return Center(
                                          child: CircularProgressIndicator(),
                                        );
                                      }
                                      return cargaCompromiso(
                                          snapData.data, proyectBloc, contex);
                                    },
                                  ),
                                  Text(
                                    'SPRINT NECESARIOS',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15),
                                  ),
                                  StreamBuilder(
                                    stream: historyUserBloc.userHistory,
                                    builder: (BuildContext contex,
                                        AsyncSnapshot<dynamic> snapData) {
                                      if (!snapData.hasData) {
                                        return Center(
                                          child: CircularProgressIndicator(),
                                        );
                                      }
                                      return cargaNumSprint(snapData.data,
                                          historyUserBloc, contex);
                                    },
                                  ),
                                  Text(
                                    'Fecha inicio del sprint',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15),
                                  ),
                                  StreamBuilder(
                                    stream: proyectBloc.proyect,
                                    builder: (BuildContext contex,
                                        AsyncSnapshot<dynamic> snapData) {
                                      if (!snapData.hasData) {
                                        return Center(
                                          child: CircularProgressIndicator(),
                                        );
                                      }
                                      return fecha(
                                          snapData.data, proyectBloc, contex);
                                    },
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            StreamBuilder(
                              stream: historyUserBloc.userHistory,
                              builder: (BuildContext contex,
                                  AsyncSnapshot<dynamic> snapData) {
                                if (!snapData.hasData) {
                                  return Center(
                                    child: CircularProgressIndicator(),
                                  );
                                }
                                return cargaSprint(
                                    snapData.data, historyUserBloc, contex);
                              },
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ])
            ],
          ),
        ),
      ),
      drawer: uiDrawer,
    );
  }

  Widget cargaLista(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    if (snapData.length == 0) {
      return Text('No hay datos');
    } else {
      return DataTable(
        columns: [
          DataColumn(
            label: Text('ID Historia de usuario'),
          ),
          DataColumn(
            label: Text('Complejidad'),
          ),
          DataColumn(
            label: Text('Prioridad'),
          ),
          DataColumn(
            label: Text('Ver más'),
          ),
        ],
        rows: this
            .userHistory
            .map(
              (userHistory) => DataRow(
                cells: [
                  DataCell(
                    Text('HU-' + userHistory.historiaUsuarioId.toString()),
                  ),
                  DataCell(
                    DropdownButton<String>(
                      value: userHistory.complejidad,
                      items:
                          itemRol.map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        setState(() {
                          userHistory.complejidad = newValue!;
                          guardarHU(userHistory);
                        });
                      },
                      hint: Text("Select item"),
                      disabledHint: Text("Disabled"),
                      elevation: 8,
                      style: TextStyle(
                          color: Color.fromARGB(255, 76, 94, 175),
                          fontSize: 16),
                      icon: Icon(Icons.arrow_drop_down_circle),
                      iconDisabledColor: Colors.red,
                      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
                      isExpanded: true,
                    ),
                    // Text(userHistory.complejidad),
                  ),
                  DataCell(
                    DropdownButton<String>(
                      value: userHistory.prioridad,
                      items: itemPrioridad
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        setState(() {
                          userHistory.prioridad = newValue!;
                          guardarHU(userHistory);
                        });
                      },
                      hint: Text("Select item"),
                      disabledHint: Text("Disabled"),
                      elevation: 8,
                      style: TextStyle(
                          color: Color.fromARGB(255, 76, 94, 175),
                          fontSize: 16),
                      icon: Icon(Icons.arrow_drop_down_circle),
                      iconDisabledColor: Colors.red,
                      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
                      isExpanded: true,
                    ),
                  ),
                  DataCell(
                    IconButton(
                      icon: Icon(Icons.remove_red_eye_outlined),
                      onPressed: () {},
                      color: Color.fromARGB(255, 236, 170, 71),
                    ),
                  )
                ],
              ),
            )
            .toList(),
      );
    }
  }

  bool validar() {
    if (funsionalidad != '' && razon != '' && criterios != '') {
      return true;
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Error"),
              content: Text("Llene todos los campos"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Ok'))
              ],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
            );
          },
          barrierDismissible: false);
      return false;
    }
  }

  Widget cargaText(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    var lista = snapData.map((userHistory) {
      int.parse(userHistory.complejidad);
      return userHistory.complejidad;
    });

    var numeros = lista, suma = 0;
    numeros.forEach((numero) {
      suma += int.parse(numero);
    });

    peso = suma.toString();
    return TextFormField(
      decoration: InputDecoration(border: InputBorder.none),
      enabled: false,
      initialValue: peso,
      onChanged: (text) {
        razon = text;
      },
    );
  }

  Widget cargaHUMin(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    var lista = snapData.map((userHistory) {
      var id = userHistory.historiaUsuarioId.toString();
      return id;
    }).toList();

    //humin = lista[0].toString();
    return DropdownButton<String>(
      items: lista.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text('HU-' + value),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          historiaMin = newValue!;
          guardarProyect();
          getAllHU();
          pintar();
        });
      },
      hint: Text('HU-' + historiaMin),
      disabledHint: Text("Disabled"),
      elevation: 8,
      style: TextStyle(color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
      icon: Icon(Icons.arrow_drop_down_circle),
      iconDisabledColor: Colors.red,
      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
      isExpanded: true,
    );
  }

  Widget cargaHUMax(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    var lista = snapData.map((userHistory) {
      var id = userHistory.historiaUsuarioId.toString();
      return id;
    }).toList();

    var initialValue = '';

    if (snapData.length != 0) {
      initialValue = lista[0].toString();
    }
    // initialValue = lista[1].toString();
    return DropdownButton<String>(
      items: lista.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text('HU-' + value),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          historiaMax = newValue!;
          guardarProyect();
          getAllHU();
          pintar();
        });
      },
      hint: Text('HU-' + historiaMax),
      disabledHint: Text("Disabled"),
      elevation: 8,
      style: TextStyle(color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
      icon: Icon(Icons.arrow_drop_down_circle),
      iconDisabledColor: Colors.red,
      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
      isExpanded: true,
    );
  }

  Widget cargaPivote(List<UserHistory> snapData,
      HistoryUserBloc historyUserBloc, BuildContext context) {
    var lista = snapData.map((userHistory) {
      var id = userHistory.historiaUsuarioId.toString();
      return id;
    }).toList();

    var initialValue = '';

    if (snapData.length != 0) {
      initialValue = lista[0].toString();
    }

    return DropdownButton<String>(
      items: lista.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text('HU-' + value),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          pivote = newValue!;
          guardarProyect();
          getAllHU();
          pintar();
        });
      },
      hint: Text('HU-' + pivote),
      disabledHint: Text("Disabled"),
      elevation: 8,
      style: TextStyle(color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
      icon: Icon(Icons.arrow_drop_down_circle),
      iconDisabledColor: Colors.red,
      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
      isExpanded: true,
    );
  }

  Widget cargaSprint(List<UserHistory> snapData,
      HistoryUserBloc historyUserBloc, BuildContext context) {
    //  print('final');
    return FlatButton(
      padding: EdgeInsets.symmetric(
        vertical: 22,
        horizontal: 48,
      ),
      onPressed: () {
        calcularSprints(snapData);
      },
      child: Text(
        'Calcular',
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).cursorColor,
      shape: StadiumBorder(),
    );
  }

  Widget cargaCompromiso(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    //  print('final');
    if (snapData.length == 0) {
      return TextFormField(
          initialValue: '0',
          onChanged: (text) {
            if (text.length > 0) {
              compromiso = text;
              guardarProyect();
            } else {
              compromiso = '0';
            }
          });
    } else {
      return TextFormField(
          initialValue: snapData[0].compromiso,
          onChanged: (text) {
            if (text.length > 0) {
              compromiso = text;
              guardarProyect();
            } else {
              compromiso = '0';
            }
          });
    }
  }

  Widget cargaNumSprint(List<UserHistory> snapData,
      HistoryUserBloc historyUserBloc, BuildContext context) {
    calcularSprintss(snapData);
    //  print('final');
    return TextFormField(
        controller: fieldTextCantidadSprints,
        decoration: InputDecoration(border: InputBorder.none),
        enabled: false,
        onChanged: (text) {
          cantidadSprints = text;
        });
  }

  Widget cargaTiempo(List<UserHistory> snapData,
      HistoryUserBloc historyUserBloc, BuildContext context) {
    //  print('final');
    return DropdownButton<String>(
      items: itemDuration.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value + ' Semana(s)'),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          tiempo = newValue!;
          guardarProyect();
          getAllHU();
          pintar();
        });
      },
      hint: Text(tiempo + ' Semana(s)'),
      disabledHint: Text("Disabled"),
      elevation: 8,
      style: TextStyle(color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
      icon: Icon(Icons.arrow_drop_down_circle),
      iconDisabledColor: Colors.red,
      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
      isExpanded: true,
    );
  }

  void getAllHU() {
    historyUserBloc.getAllHU();
    proyectBloc.getProyect();
    proyectBloc2.getProyectCorreo();
    proyectBloc2.proyects.listen((List<Proyect> proyectListnew) {
      this.proyectList = proyectListnew;
    });
  }

  void pintar() {
    historyUserBloc.getAllHU();
    historyUserBloc.userHistory.listen((List<UserHistory> userHistory) {
      this.userHistory = userHistory;
    });
  }

  void calcularSprints(snapData) {
    if (int.parse(compromiso) > 0) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Alerta"),
              content: Text(
                  "Si calcula, la panatalla sprint se actualizará con base en el sistema"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Cancelar')),
                FlatButton(
                    onPressed: () {
                      alertaSprintok(snapData);
                      Navigator.pop(context);
                    },
                    child: Text('Ok'))
              ],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
            );
          },
          barrierDismissible: false);
    } else {
      exito();
    }
  }

  void calcularSprintss(snapData) {
    var numbSprint = 1;
    var contador = 0;

    snapData.forEach((numero) {
      contador += int.parse(numero.complejidad);

      if (contador <= int.parse(compromiso)) {
        // print('sprint ' +            numbSprint.toString() +        '  Hu_id ' +            numero.historiaUsuarioId.toString());
      } else {
        numbSprint += 1;
        contador = int.parse(numero.complejidad);
        // print('sprint ' +            numbSprint.toString() +            '  Hu_id ' +            numero.historiaUsuarioId.toString());
      }
    });
    duracion = (int.parse(tiempo) * numbSprint).toString();
    cant_sprint = numbSprint;
    fieldTextCantidadSprints.text = numbSprint.toString();
  }

  void guardarProyect() {
    fieldTextCompromiso.text = compromiso;
    proyectBloc
        .putProyect(
          duracion,
          equipoTrabajo,
          historiaMax,
          historiaMin,
          proyect[0].nombre,
          peso,
          pivote,
          tiempo,
          compromiso,
          '${selectedDate}',
          cant_sprint,
          proyect[0].proyectoId,
          proyect[0].id_poryect,
        )
        .then((value) => {getAllHU(), pintar()});
  }

  Widget cargaData(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    proyect = snapData;

    if (snapData.length != 0) {
      duracion = proyect[0].duracion;
      historiaMax = proyect[0].historiaMax;
      historiaMin = proyect[0].historiaMin;
      peso = proyect[0].peso;
      pivote = proyect[0].pivote;
      tiempo = proyect[0].tiempo;
      compromiso = proyect[0].compromiso;

      selectedDate = DateTime.parse(proyect[0].fecha_inicio);
    }

    return Container();
  }

  Widget fecha(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    proyect = snapData;

    if (snapData.length != 0) {
      selectedDate = DateTime.parse(proyect[0].fecha_inicio);
    } else {
      selectedDate = DateTime.now();
    }

    return ElevatedButton(
      onPressed: () {
        _selectDate(context);
      },
      child: Text(
          "${selectedDate.day}/${selectedDate.month}/${selectedDate.year}"),
    );
  }

  Widget AlertCalcular(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    proyect = snapData;

    duracion = proyect[0].duracion;
    historiaMax = proyect[0].historiaMax;
    historiaMin = proyect[0].historiaMin;
    peso = proyect[0].peso;
    pivote = proyect[0].pivote;
    tiempo = proyect[0].tiempo;
    compromiso = proyect[0].compromiso;

    return Container();
  }

  void alertaSprintok(snapData) {
    if (int.parse(compromiso) > 0) {
      var numbSprint = 1;
      var contador = 0;
      snapData.forEach((numero) {
        contador += int.parse(numero.complejidad);

        if (contador <= int.parse(compromiso)) {
          historyUserBloc.updateUserHistory(
            numero.rol,
            numero.funsionalidad,
            numero.razon,
            numero.criterioAceptacion,
            numero.complejidad,
            numero.prioridad,
            numbSprint.toString(),
            numero.encargado,
            numero.estado,
            numero.historiaUsuarioId,
          );
        } else {
          numbSprint += 1;
          contador = int.parse(numero.complejidad);
          historyUserBloc.updateUserHistory(
            numero.rol,
            numero.funsionalidad,
            numero.razon,
            numero.criterioAceptacion,
            numero.complejidad,
            numero.prioridad,
            numbSprint.toString(),
            numero.encargado,
            numero.estado,
            numero.historiaUsuarioId,
          );
        }
      });
      duracion = (int.parse(tiempo) * numbSprint).toString();

      fieldTextCantidadSprints.text = numbSprint.toString();

      guardarProyect();
    } else {
      Navigator.pop(context);
      exito();
    }
  }

  void guardarHU(hu) {
    historyUserBloc
        .updateUserHistory(
          hu.rol,
          hu.funsionalidad,
          hu.razon,
          hu.criterioAceptacion,
          hu.complejidad,
          hu.prioridad,
          hu.sprint,
          hu.encargado,
          hu.estado,
          hu.historiaUsuarioId,
        )
        .then((value) => {getAllHU(), pintar()});
  }

  _selectDate(BuildContext context) async {
    final DateTime? selected = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(2010),
      lastDate: DateTime(2025),
    );
    if (selected != null && selected != selectedDate)
      setState(() {
        selectedDate = selected;
        guardarProyect();
        getAllHU();
        pintar();
      });
  }

  void exito() {
    //print('hola');
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Alerta"),
            content: Text(
                "El compromiso por sprint debe de ser igual o mayor a la historia de usuario máxima"),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Aceptar'))
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          );
        },
        barrierDismissible: false);
  }

  void updateTIme() {}

  Widget nombreProyecto(
      List<Proyect> snapData, ProyectBloc2 proyectBloc2, BuildContext context) {
    var lista = snapData.map((proyecto) {
      var ne = {};
      ne['name'] = proyecto.nombre;
      ne['valor'] = proyecto.id_poryect;
      var id = proyecto.nombre.toString();
      return ne;
    }).toList();

    const List<String> list = <String>['One', 'Two', 'Three', 'Four'];
    String dropdownValue = list.first;

    return DropdownButton<Object>(
      value: null,
      icon: const Icon(Icons.arrow_downward),
      elevation: 16,
      hint: Text(Seleccione),
      style: const TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (Object? newValue) {
        // This is called when the user selects an item.
        setState(() {
          var id = newValue.toString().split("-")[1];
          document.cookie = "id=$id; max-age=3600;";
          Seleccione = newValue.toString().split("-")[0];
          getAllHU();
        });
      },
      items: lista.map<DropdownMenuItem<Object>>((value) {
        return DropdownMenuItem<Object>(
          value: value['name'].toString() + '-' + value['valor'].toString(),
          child: Text(value['name']),
        );
      }).toList(),
    );
  }
}
