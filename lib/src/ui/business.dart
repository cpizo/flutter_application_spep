import 'package:flutter/material.dart';
import 'package:flutter_application_spep/src/bloc/person_bloc.dart';
import 'package:flutter_application_spep/src/bloc/proyect_bloc.dart';
import 'package:flutter_application_spep/src/models/person_model.dart';
import 'package:flutter_application_spep/src/models/proyect_model.dart';
import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/repository/getToken.dart';
import 'package:flutter_application_spep/src/ui/editWorkTeam.dart';
import 'package:flutter_application_spep/src/ui/insertWorkTeam.dart';
import 'package:flutter_application_spep/src/ui/uiDrawer.dart';
import 'package:flutter_application_spep/src/ui/tableHU.dart';
import 'package:flutter_application_spep/src/bloc/historyUser_bloc.dart';
import 'package:flutter_application_spep/src/models/userHistory_model.dart';
import 'dart:math';
import 'dart:convert';

import 'dart:html';

import 'package:flutter_application_spep/src/ui/workTeam.dart';

void main() => runApp(new Business());

// Definir un componente con estado
class Business extends StatefulWidget {
  @override
  _BusinessState createState() => _BusinessState();
}

// Al definir un componente con estado, debe crear una clase de estado para el componente, que hereda de la clase de estado
class _BusinessState extends State<Business> {
  final scaffolKey = GlobalKey<ScaffoldState>();

  CookieManager cookieManager = CookieManager();

  GlobalKey<FormState> globalFormkey = new GlobalKey<FormState>();

  final ProyectBloc proyectBloc = ProyectBloc();
  Random random = new Random();
  final fieldTextNombre = TextEditingController();
  List<Proyect> proyectList = [];

  var Seleccione = 'Seleccione';

  @override
  void initState() {
    getAllProyect();
  }

  List<String> listPerson = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 224, 229, 255),
      key: scaffolKey,
      appBar: AppBar(
        title: Text("Proyectos"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.update),
            onPressed: () {},
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: 400,
              padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
              margin: EdgeInsets.symmetric(vertical: 45, horizontal: 20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Color.fromARGB(255, 255, 255, 255),
                boxShadow: [
                  BoxShadow(
                      color: Theme.of(context).hintColor.withOpacity(0.2),
                      offset: Offset(0, 10),
                      blurRadius: 20)
                ],
              ),
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 25),
                    child: FittedBox(
                      child: Row(
                        children: [
                          Text(
                              'Seleccione el proyecto con el que desea iniciar',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 25,
                              )),
                        ],
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        width: 300,
                        padding: const EdgeInsets.all(20),
                        margin: const EdgeInsets.only(left: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'SELECCIONE EL PROYECTO',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 15),
                            ),
                            StreamBuilder(
                              stream: proyectBloc.proyect,
                              builder: (BuildContext contex,
                                  AsyncSnapshot<dynamic> snapData) {
                                if (!snapData.hasData) {
                                  return Center(
                                    child: CircularProgressIndicator(),
                                  );
                                }
                                return nombreProyecto(
                                    snapData.data, proyectBloc, contex);
                              },
                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 0, bottom: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  FlatButton(
                                    padding: EdgeInsets.symmetric(
                                      vertical: 22,
                                      horizontal: 48,
                                    ),
                                    onPressed: () {
                                      obtenerId();
                                    },
                                    child: Text(
                                      'Continuar',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    color: Theme.of(context).cursorColor,
                                    shape: StadiumBorder(),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin:
                                  const EdgeInsets.only(top: 100.0, bottom: 50),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  FlatButton(
                                    color: Color.fromARGB(255, 73, 80, 167),
                                    padding: EdgeInsets.symmetric(
                                      vertical: 22,
                                      horizontal: 48,
                                    ),
                                    onPressed: () {
                                      nuevoProyecto();
                                    },
                                    child: Text(
                                      'Crear nuevo proyecto',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    //color: Theme.of(context).cursorColor,
                                    shape: StadiumBorder(),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void getAllProyect() {
    proyectBloc.getProyectCorreo();
    proyectBloc.proyect.listen((List<Proyect> proyectListnew) {
      this.proyectList = proyectListnew;
    });
  }

  Widget nombreProyecto(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    var lista = snapData.map((proyecto) {
      var ne = {};
      ne['name'] = proyecto.nombre;
      ne['valor'] = proyecto.id_poryect;
      var id = proyecto.nombre.toString();
      return ne;
    }).toList();

    return DropdownButton<Object>(
      value: null,
      items: lista.map<DropdownMenuItem<Object>>((value) {
        return DropdownMenuItem<Object>(
          value: value['name'].toString() + '-' + value['valor'].toString(),
          child: Text(value['name']),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          var id = newValue.toString().split("-")[1];
          document.cookie = "id=$id; max-age=3600;";
          Seleccione = newValue.toString().split("-")[0];
        });
      },
      hint: Text(Seleccione),
      disabledHint: Text("Disabled"),
      elevation: 8,
      style: TextStyle(color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
      icon: Icon(Icons.arrow_drop_down_circle),
      iconDisabledColor: Colors.red,
      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
      isExpanded: true,
    );
  }

  void nuevoProyecto() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Ingresa el nombre de su nuevo proyecto"),
            content: TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'nombre',
              ),
              controller: fieldTextNombre,
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Cancelar')),
              FlatButton(
                  onPressed: () {
                    crearProyecto();
                  },
                  child: Text('Continuar'))
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          );
        },
        barrierDismissible: false);
  }

  void crearProyecto() {
    var newId = random.nextInt(90000);
    proyectBloc.postProyectNew(newId, fieldTextNombre.text);
    document.cookie = "id=$newId; max-age=3600;";
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => WorkTeam(),
    ));
  }

  void obtenerId() {
    if (Seleccione != "Seleccione") {
      print(cookieManager.getCookie('id'));
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => WorkTeam(),
      ));
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Alerta"),
              content: Text("Por favor seleccione un proyecto"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Aceptar'))
              ],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
            );
          },
          barrierDismissible: false);
    }
  }
}
