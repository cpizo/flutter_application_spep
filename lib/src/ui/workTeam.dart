import 'package:flutter/material.dart';
import 'package:flutter_application_spep/src/bloc/person_bloc.dart';
import 'package:flutter_application_spep/src/bloc/proyect_bloc.dart';
import 'package:flutter_application_spep/src/models/proyect_model.dart';
import 'package:flutter_application_spep/src/ui/uiDrawer.dart';
import 'package:flutter_application_spep/src/bloc/historyUser_bloc.dart';
import 'package:flutter_application_spep/src/ui/tableHU.dart';
import 'package:flutter_application_spep/src/ui/insertWorkTeam.dart';
import 'package:flutter_application_spep/src/ui/editWorkTeam.dart';
import 'package:flutter_application_spep/src/models/person_model.dart';
import 'dart:html';

void main() => runApp(new WorkTeam());

// Definir un componente con estado
class WorkTeam extends StatefulWidget {
  @override
  _WorkTeamState createState() => _WorkTeamState();
}

class UserData {
  String username = '';
  String password = '';
}

// Al definir un componente con estado, debe crear una clase de estado para el componente, que hereda de la clase de estado
class _WorkTeamState extends State<WorkTeam> {
  final scaffolKey = GlobalKey<ScaffoldState>();

  GlobalKey<FormState> globalFormkey = new GlobalKey<FormState>();
  bool hidePassword = true;

  final ProyectBloc proyectBloc = ProyectBloc();
  UiDrawer uiDrawer = UiDrawer();
  UserData user = UserData();
  TableHU tableHU = TableHU();
  final HistoryUserBloc historyUserBloc = HistoryUserBloc();
  final PersonBloc personBloc = PersonBloc();
  final fieldTextRequiero = TextEditingController();
  final fieldTextFinalidad = TextEditingController();
  final fieldTextCriterios = TextEditingController();
  List<String> itemRol = ['1', '2', '3', '5', '8', '13', '21', '?'];
  String rol = '1';
  String funsionalidad = '';
  String razon = '';
  String criterios = '';
  List<String> itemComplejidad = ['1', '2', '3', '5', '8', '13', '21', '?'];
  String complejidad = '1';
  List<String> itemPrioridad = ['S', 'M', 'C', 'W'];
  String prioridad = 'S';
  List<Person> person = [];
  var Seleccione = 'Seleccione';
  List<Proyect> proyectList = [];

  initialUserHistory() {
    personBloc.getAllPersons();
    proyectBloc.getProyect();
  }

  @override
  void initState() {
    getAllHU();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 224, 229, 255),
      key: scaffolKey,
      appBar: AppBar(
        title: Text("Participantes del equipo"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.update),
            onPressed: () {
              getAllHU();
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Center(
              child: Container(
                width: 1000,
                padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                margin: EdgeInsets.symmetric(vertical: 35, horizontal: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Theme.of(context).hintColor.withOpacity(0.2),
                        offset: Offset(0, 10),
                        blurRadius: 20)
                  ],
                ),
                child: FittedBox(
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              'SELECCIONE EL PROYECTO',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 15),
                            ),
                            StreamBuilder(
                              stream: proyectBloc.proyect,
                              builder: (BuildContext contex,
                                  AsyncSnapshot<dynamic> snapData) {
                                if (!snapData.hasData) {
                                  return Center(
                                    child: CircularProgressIndicator(),
                                  );
                                }
                                return nombreProyecto(
                                    snapData.data, proyectBloc, contex);
                              },
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(bottom: 25),
                        child: Row(
                          children: [
                            Text('Participantes del proyecto',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25,
                                )),
                          ],
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          FlatButton(
                            padding: EdgeInsets.symmetric(
                              vertical: 18,
                              horizontal: 38,
                            ),
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => InsertWorkTeam(),
                              ));
                              // agregarHU();
                            },
                            child: Text(
                              'Agregar Participante',
                              style: TextStyle(color: Colors.white),
                            ),
                            color: Theme.of(context).cursorColor,
                            shape: StadiumBorder(),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          StreamBuilder(
                            stream: personBloc.person,
                            builder: (BuildContext contex,
                                AsyncSnapshot<dynamic> snapData) {
                              if (!snapData.hasData) {
                                return Center(
                                  child: CircularProgressIndicator(),
                                );
                              }
                              return cargaLista(
                                  snapData.data, personBloc, contex);
                            },
                          ),
                          StreamBuilder(
                            stream: proyectBloc.proyect,
                            builder: (BuildContext contex,
                                AsyncSnapshot<dynamic> snapData) {
                              if (!snapData.hasData) {
                                return Center(
                                  child: CircularProgressIndicator(),
                                );
                              }
                              return crearProyect(
                                  snapData.data, proyectBloc, contex);
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      drawer: uiDrawer,
    );
  }

  Widget crearProyect(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    if (snapData.length != 0) {
      // print('1');
      return Container();
    } else {
      //print('2');
      proyectBloc.postProyect();
      return Container();
    }
  }

  Widget cargaLista(
      List<Person> snapData, PersonBloc personBloc, BuildContext context) {
    return DataTable(
      columns: [
        DataColumn(
          label: Text('Id Usuario'),
        ),
        DataColumn(
          label: Text('Nombre'),
        ),
        DataColumn(
          label: Text('Telefono'),
        ),
        DataColumn(
          label: Text('Salario'),
        ),
        DataColumn(
          label: Text('Rol'),
        ),
        DataColumn(
          label: Text('Acciones'),
        ),
      ],
      rows: this
          .person
          .map(
            (person) => DataRow(
              cells: [
                DataCell(
                  Text(person.id_person.toString()),
                ),
                DataCell(
                  Text(person.nombre),
                ),
                DataCell(
                  Text(person.telefono),
                ),
                DataCell(
                  Text(numberFormat(person.valor_hora)),
                ),
                DataCell(
                  Text(person.rol),
                ),
                DataCell(Row(
                  children: [
                    IconButton(
                      icon: Icon(Icons.edit_outlined),
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) =>
                              EditWorkTeam(data: person.id_person.toString()),
                        ));
                      },
                      color: Color.fromARGB(255, 244, 162, 54),
                    ),
                    IconButton(
                      icon: Icon(Icons.delete),
                      onPressed: () {
                        eliminar(person.id_person);
                      },
                      color: Colors.red,
                    ),
                  ],
                )),
              ],
            ),
          )
          .toList(),
    );
  }

  bool validar() {
    if (funsionalidad != '' && razon != '' && criterios != '') {
      return true;
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Error"),
              content: Text("Llene todos los campos"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Ok'))
              ],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
            );
          },
          barrierDismissible: false);
      return false;
    }
  }

  void getAllHU() {
    personBloc.getAllPersons();
    personBloc.person.listen((List<Person> person) {
      this.person = person;
    });
    //proyectBloc.getProyect();
    proyectBloc.getProyectCorreo();
    proyectBloc.proyect.listen((List<Proyect> proyectListnew) {
      this.proyectList = proyectListnew;
    });
  }

  void pintar(String id) {
    //print(id);
  }

  void eliminar(int id) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Alerta"),
            content: Text("Desea eliminar el usuario " + id.toString() + '?'),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Cancelar')),
              FlatButton(
                  onPressed: () {
                    personBloc.deleterPerson(id).then((bool estado) {
                      if (estado) {
                        getAllHU();
                      } else {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text("Error"),
                                content:
                                    Text("No se pudo eliminar el usuario "),
                                actions: <Widget>[
                                  FlatButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: Text('Ok'))
                                ],
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                              );
                            },
                            barrierDismissible: false);
                      }
                    });
                    Navigator.pop(context);
                  },
                  color: Colors.red,
                  textColor: Colors.white,
                  child: Text('Aceptar'))
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          );
        },
        barrierDismissible: false);
  }

  String numberFormat(x) {
    List<String> parts = x.toString().split('.');
    RegExp re = RegExp(r'\B(?=(\d{3})+(?!\d))');

    parts[0] = parts[0].replaceAll(re, '.');
    if (parts.length == 1) {
    } else {
      parts[1] = parts[1].padRight(2, '0').substring(0, 2);
    }
    return parts.join(',');
  }

  Widget nombreProyecto(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    var lista = snapData.map((proyecto) {
      var ne = {};
      ne['name'] = proyecto.nombre;
      ne['valor'] = proyecto.id_poryect;
      var id = proyecto.nombre.toString();
      return ne;
    }).toList();

    const List<String> list = <String>['One', 'Two', 'Three', 'Four'];
    String dropdownValue = list.first;

    return DropdownButton<Object>(
      value: null,
      icon: const Icon(Icons.arrow_downward),
      elevation: 16,
      hint: Text(Seleccione),
      style: const TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (Object? newValue) {
        // This is called when the user selects an item.
        setState(() {
          var id = newValue.toString().split("-")[1];
          document.cookie = "id=$id; max-age=3600;";
          Seleccione = newValue.toString().split("-")[0];
          getAllHU();
        });
      },
      items: lista.map<DropdownMenuItem<Object>>((value) {
        return DropdownMenuItem<Object>(
          value: value['name'].toString() + '-' + value['valor'].toString(),
          child: Text(value['name']),
        );
      }).toList(),
    );
  }
}
