import 'package:flutter/material.dart';
import 'package:flutter_application_spep/src/bloc/register_bloc.dart';
import 'package:flutter_application_spep/src/ui/business.dart';
import 'package:flutter_application_spep/src/ui/register.dart';
import 'package:flutter_application_spep/src/ui/home.dart';
import 'package:flutter_application_spep/src/bloc/login_bloc.dart';
import 'package:flutter_application_spep/src/ui/workTeam.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:math';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class UserData {
  String username = '';
  String password = '';
}

const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
Random _rnd = Random();

class _LoginPageState extends State<LoginPage> {
  final RegisterBloc registerBloc = RegisterBloc();
  final scaffolKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> globalFormkey = new GlobalKey<FormState>();
  bool hidePassword = true;

  UserData user = UserData();
  final LoginBloc loginBloc = LoginBloc();
  String codigo = "";
  final fieldTextCodigo = TextEditingController();
  final fieldTextCorreo = TextEditingController();
  final fieldTextPass = TextEditingController();

  Future sendEmail() async {
    codigo = getRandomString(8);
    final url = Uri.parse("https://api.emailjs.com/api/v1.0/email/send");
    const serviceId = "service_0lbxx4f";
    const templateId = "template_56m0b6b";
    const userId = "B6GLOlk0LvdPdF8xM";
    final response = await http.post(url,
        headers: {'Content-Type': 'application/json'},
        body: json.encode({
          "service_id": serviceId,
          "template_id": templateId,
          "user_id": userId,
          "template_params": {
            "name": "",
            "subject": "Recuperación de contraseña",
            "message":
                "Hola, este es el código para poder recuperar tu contraseña:  " +
                    codigo,
            "user_email": fieldTextCorreo.text
          }
        }));
    return response.statusCode;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 224, 229, 255),
      key: scaffolKey,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Center(
              child: Container(
                width: 1200,
                padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                margin: EdgeInsets.symmetric(vertical: 85, horizontal: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Theme.of(context).primaryColor,
                  boxShadow: [
                    BoxShadow(
                        color: Theme.of(context).hintColor.withOpacity(0.2),
                        offset: Offset(0, 10),
                        blurRadius: 20)
                  ],
                ),
                child: Form(
                  key: globalFormkey,
                  child: Column(children: <Widget>[
                    SizedBox(
                      height: 25,
                    ),
                    Text(
                      "Ingresa",
                      style: Theme.of(context).textTheme.headline2,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    new TextField(
                      //validator: (input) => !input.contains('@')
                      //? "Email Id should be Valid"
                      //: null,
                      onChanged: (text) {
                        user.username = text;
                      },
                      decoration: new InputDecoration(
                        hintText: "Nombre de usuario",
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color:
                                Theme.of(context).accentColor.withOpacity(0.2),
                          ),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                        prefix: Icon(
                          Icons.child_care,
                          color: Theme.of(context).accentColor,
                        ),
                      ),
                      keyboardType: TextInputType.emailAddress,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    new TextField(
                      keyboardType: TextInputType.text,
                      onChanged: (text) {
                        user.password = text;
                      },
                      //validator: (input) => input.length < 3
                      //  ? "La contraseña debe de tener minimo 3 caracteres"
                      //  : null,
                      obscureText: hidePassword,
                      decoration: new InputDecoration(
                        hintText: "Contraseña",
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color:
                                Theme.of(context).accentColor.withOpacity(0.2),
                          ),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                        prefix: Icon(
                          Icons.security,
                          color: Theme.of(context).accentColor,
                        ),
                        suffixIcon: IconButton(
                          onPressed: () {
                            setState(() {
                              hidePassword = !hidePassword;
                            });
                          },
                          color: Theme.of(context).accentColor.withOpacity(0.4),
                          icon: Icon(hidePassword
                              ? Icons.visibility_off
                              : Icons.visibility),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      margin: const EdgeInsets.only(bottom: 25),
                      child: InkWell(
                          child: Text(
                            '¿No recuerdas tu contraseña?',
                            style: TextStyle(
                              color: Colors.blue,
                              decoration: TextDecoration.underline,
                            ),
                          ),
                          onTap: () => recuperarContasena()),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    FlatButton(
                      padding: EdgeInsets.symmetric(
                        vertical: 12,
                        horizontal: 88,
                      ),
                      onPressed: () {
                        login();
                      },
                      child: Text(
                        'Ingresa',
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Theme.of(context).cursorColor,
                      shape: StadiumBorder(),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    FlatButton(
                      padding: EdgeInsets.symmetric(
                        vertical: 12,
                        horizontal: 88,
                      ),
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => RegisterPage(),
                        ));
                      },
                      child: Text(
                        'Registrate',
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Theme.of(context).accentColor,
                      shape: StadiumBorder(),
                    ),
                  ]),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void login() {
    loginBloc.validateLogin(user.username, user.password).then((bool access) {
      if (access) {
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => WorkTeam(),
        ));
      } else {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("Error"),
                content: Text("No se pudo loguear"),
                actions: <Widget>[
                  FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('Ok'))
                ],
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
              );
            },
            barrierDismissible: false);
      }
    });
  }

  void recuperarContasena() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Ingresa tu correo para recuperar contraseña"),
            content: TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'correo',
              ),
              controller: fieldTextCorreo,
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Cancelar')),
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                    sendEmail();
                    ingresarCodigo();
                  },
                  child: Text('Enviar'))
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          );
        },
        barrierDismissible: false);
  }

  void ingresarCodigo() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Ingresa el código que llega a tu correo"),
            content: TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'código',
              ),
              controller: fieldTextCodigo,
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Cancelar')),
              FlatButton(
                  onPressed: () {
                    //  print(fieldTextCodigo.text);
                    //  print(codigo);
                    if (fieldTextCodigo.text == codigo) {
                      Navigator.pop(context);
                      ingresarPass();
                    } else {
                      errorCodigo();
                    }
                  },
                  child: Text('Siguiente'))
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          );
        },
        barrierDismissible: false);
  }

  void ingresarPass() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Ingresa la núeva contraseña"),
            content: TextField(
              obscureText: hidePassword,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'núeva contraseña',
              ),
              controller: fieldTextPass,
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Cancelar')),
              FlatButton(
                  onPressed: () {
                    registerBloc.updatePassword(
                        fieldTextCorreo.text, fieldTextPass.text);
                    Navigator.pop(context);
                    exito();
                  },
                  child: Text('Aceptar'))
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          );
        },
        barrierDismissible: false);
  }

  void errorCodigo() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Error al ingresar el código"),
            content: Text("Los códigos no considen"),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Aceptar'))
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          );
        },
        barrierDismissible: false);
  }

  void exito() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Éxito"),
            content: Text("Ingresa con tu nombre de usuario y contraseña"),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Aceptar'))
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          );
        },
        barrierDismissible: false);
  }

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
}
