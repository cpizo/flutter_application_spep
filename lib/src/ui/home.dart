import 'package:flutter/material.dart';
import 'package:flutter_application_spep/src/ui/uiDrawer.dart';

class Home extends StatelessWidget {
  List itemsRol = ['1', '2'];
  String dropdownValue = 'One';
  String? value;
  String? valueChoose;
  UiDrawer uiDrawer = UiDrawer();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Home',
        home: Scaffold(
          backgroundColor: Color.fromARGB(255, 224, 229, 255),
          appBar: AppBar(
            title: Text("Home"),
          ),
          body: Container(
              width: 100000,
              child: Image.asset(
                'como-administrar-un-negocio-con-exito.png',
                fit: BoxFit.fill,
              )),
          drawer: uiDrawer,
          // This trailing comma makes auto-formatting nicer for build methods.
        ));
  }
}
