import 'package:flutter/material.dart';
import 'package:flutter_application_spep/src/ui/uiDrawer.dart';
import 'package:flutter_application_spep/src/bloc/historyUser_bloc.dart';
import 'package:flutter_application_spep/src/models/userHistory_model.dart';

class TableHU extends StatefulWidget {
  @override
  _TableHUState createState() => _TableHUState();
}

class UserData {
  String username = '';
  String password = '';
}

class _TableHUState extends State<TableHU> {
  final scaffolKey = GlobalKey<ScaffoldState>();

  GlobalKey<FormState> globalFormkey = new GlobalKey<FormState>();
  bool hidePassword = true;

  UiDrawer uiDrawer = UiDrawer();
  UserData user = UserData();
  final HistoryUserBloc historyUserBloc = HistoryUserBloc();
  final fieldTextRequiero = TextEditingController();
  final fieldTextFinalidad = TextEditingController();
  final fieldTextCriterios = TextEditingController();
  List<String> itemRol = ['cliente'];
  String rol = 'cliente';
  String funsionalidad = '';
  String razon = '';
  String criterios = '';
  List<String> itemComplejidad = ['1', '2', '3', '5', '8', '13', '21', '?'];
  String complejidad = '';
  List<String> itemPrioridad = ['S', 'M', 'C', 'W'];
  String prioridad = '';
  var _TextControllerRequiero;
  List<UserHistory> userHistory = [];

  initialUserHistory() {
    historyUserBloc.getAllHU();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 224, 229, 255),
      key: scaffolKey,
      appBar: AppBar(
        title: Text("Estimación y priorización"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.update),
            onPressed: () {
              historyUserBloc.getAllHU();
            },
          ),
        ],
      ),
      body: StreamBuilder(
        stream: historyUserBloc.userHistory,
        builder: (BuildContext contex, AsyncSnapshot<dynamic> snapData) {
          if (!snapData.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return cargaLista(snapData.data, historyUserBloc, contex);
        },
      ),
      drawer: uiDrawer,
    );
  }

  Widget cargaLista(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    return DataTable(
      columns: [
        DataColumn(
          label: Text('ID Historia de usuario'),
        ),
        DataColumn(
          label: Text('Complejidad'),
        ),
        DataColumn(
          label: Text('Prioridad'),
        ),
      ],
      rows: snapData
          .map(
            (userHistory) => DataRow(
              cells: [
                DataCell(
                  Text('HU-' + userHistory.historiaUsuarioId.toString()),
                ),
                DataCell(
                  Text(userHistory.complejidad),
                ),
                DataCell(
                  Text(userHistory.prioridad),
                ),
              ],
            ),
          )
          .toList(),
    );
  }
}
