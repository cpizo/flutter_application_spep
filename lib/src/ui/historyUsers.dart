import 'package:flutter/material.dart';
import 'package:flutter_application_spep/src/bloc/proyect_bloc.dart';
import 'package:flutter_application_spep/src/models/proyect_model.dart';
import 'package:flutter_application_spep/src/ui/uiDrawer.dart';
import 'package:flutter_application_spep/src/bloc/historyUser_bloc.dart';
import 'package:flutter_application_spep/src/ui/tableHU.dart';
import 'package:flutter_application_spep/src/ui/editHistoryUsers.dart';
import 'package:flutter_application_spep/src/ui/insertHistoryUsers.dart';
import 'package:flutter_application_spep/src/ui/viewHistoryUsers.dart';
import 'package:flutter_application_spep/src/models/userHistory_model.dart';
import 'dart:html';

class HistoryUsers extends StatefulWidget {
  @override
  _HistoryUsersState createState() => _HistoryUsersState();
}

class UserData {
  String username = '';
  String password = '';
}

class _HistoryUsersState extends State<HistoryUsers> {
  final scaffolKey = GlobalKey<ScaffoldState>();

  GlobalKey<FormState> globalFormkey = new GlobalKey<FormState>();
  bool hidePassword = true;

  UiDrawer uiDrawer = UiDrawer();
  UserData user = UserData();
  TableHU tableHU = TableHU();
  final HistoryUserBloc historyUserBloc = HistoryUserBloc();
  final ProyectBloc proyectBloc = ProyectBloc();
  final fieldTextRequiero = TextEditingController();
  final fieldTextFinalidad = TextEditingController();
  final fieldTextCriterios = TextEditingController();
  List<String> itemRol = ['1', '2', '3', '5', '8', '13', '21', '?'];
  String rol = '1';
  String funsionalidad = '';
  String razon = '';
  String criterios = '';
  List<String> itemComplejidad = ['1', '2', '3', '5', '8', '13', '21', '?'];
  String complejidad = '1';
  List<String> itemPrioridad = ['S', 'M', 'C', 'W'];
  String prioridad = 'S';
  List<UserHistory> userHistory = [];
  List<Proyect> proyectList = [];
  var Seleccione = 'Seleccione';

  initialUserHistory() {
    historyUserBloc.getAllHU();
  }

  @override
  void initState() {
    getAllHU();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 224, 229, 255),
      key: scaffolKey,
      appBar: AppBar(
        title: Text("Product Backlog"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.update),
            onPressed: () {
              getAllHU();
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Center(
              child: Container(
                width: 1000,
                padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                margin: EdgeInsets.symmetric(vertical: 35, horizontal: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Theme.of(context).hintColor.withOpacity(0.2),
                        offset: Offset(0, 10),
                        blurRadius: 20)
                  ],
                ),
                child: Column(
                  children: [
                    Center(
                      child: Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'SELECCIONE EL PROYECTO',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 15),
                            ),
                            StreamBuilder(
                              stream: proyectBloc.proyect,
                              builder: (BuildContext contex,
                                  AsyncSnapshot<dynamic> snapData) {
                                if (!snapData.hasData) {
                                  return Center(
                                    child: CircularProgressIndicator(),
                                  );
                                }
                                return nombreProyecto(
                                    snapData.data, proyectBloc, contex);
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(bottom: 25),
                      child: Row(
                        children: [
                          Text('Historia de usuarios',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 25,
                              )),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        FlatButton(
                          padding: EdgeInsets.symmetric(
                            vertical: 22,
                            horizontal: 48,
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => InsertHistoryUsers()),
                            );
                          },
                          child: Text(
                            'Agregar HU',
                            style: TextStyle(color: Colors.white),
                          ),
                          color: Theme.of(context).cursorColor,
                          shape: StadiumBorder(),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        StreamBuilder(
                          stream: historyUserBloc.userHistory,
                          builder: (BuildContext contex,
                              AsyncSnapshot<dynamic> snapData) {
                            if (!snapData.hasData) {
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            }
                            return cargaLista(
                                snapData.data, historyUserBloc, contex);
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      drawer: uiDrawer,
    );
  }

  Widget cargaLista(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    return FittedBox(
        child: DataTable(
      columns: [
        DataColumn(
          label: Text('Id Historia de usuario'),
        ),
        DataColumn(
          label: Text('Rol'),
        ),
        DataColumn(
          label: Text('Funcionalidad'),
        ),
        DataColumn(
          label: Text('Razón'),
        ),
        DataColumn(
          label: Text('Acciones'),
        ),
      ],
      rows: this
          .userHistory
          .map(
            (userHistory) => DataRow(
              cells: [
                DataCell(
                  Text('HU-' + userHistory.historiaUsuarioId.toString()),
                ),
                DataCell(
                  Text(userHistory.rol),
                ),
                DataCell(
                  FittedBox(
                      child: Text(userHistory.funsionalidad.length > 20
                          ? userHistory.funsionalidad.substring(0, 20) + '...'
                          : userHistory.funsionalidad)),
                ),
                DataCell(
                  FittedBox(
                      child: Text(userHistory.razon.length > 20
                          ? userHistory.razon.substring(0, 20) + '...'
                          : userHistory.razon)),
                ),
                DataCell(Row(
                  children: [
                    IconButton(
                      icon: Icon(Icons.remove_red_eye_outlined),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ViewHistoryUsers(
                                  data: userHistory.historiaUsuarioId
                                      .toString())),
                        );
                      },
                      color: Color.fromARGB(255, 236, 170, 71),
                    ),
                    IconButton(
                      icon: Icon(Icons.edit_outlined),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EditHistoryUsers(
                                  data: userHistory.historiaUsuarioId
                                      .toString())),
                        );
                      },
                      color: Colors.blue,
                    ),
                    IconButton(
                      icon: Icon(Icons.delete),
                      onPressed: () {
                        eliminar(userHistory.historiaUsuarioId);
                      },
                      color: Colors.red,
                    ),
                  ],
                )),
              ],
            ),
          )
          .toList(),
    ));
  }

  bool validar() {
    if (funsionalidad != '' && razon != '' && criterios != '') {
      return true;
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Error"),
              content: Text("Llene todos los campos"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Ok'))
              ],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
            );
          },
          barrierDismissible: false);
      return false;
    }
  }

  void getAllHU() {
    historyUserBloc.getAllHU();
    historyUserBloc.userHistory.listen((List<UserHistory> userHistory) {
      //print(userHistory[0].complejidad);
      this.userHistory = userHistory;
    });
    proyectBloc.getProyectCorreo();
    proyectBloc.proyect.listen((List<Proyect> proyectListnew) {
      this.proyectList = proyectListnew;
    });
  }

  void eliminar(int id) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Alerta"),
            content: Text("Desea eliminar la HU " + id.toString() + '?'),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Cancelar')),
              FlatButton(
                  onPressed: () {
                    historyUserBloc.deleterHistory(id).then((bool estado) {
                      if (estado) {
                        getAllHU();
                      } else {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text("Error"),
                                content: Text(
                                    "No se pudo eliminar la HIsotoria de usuario "),
                                actions: <Widget>[
                                  FlatButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: Text('Ok'))
                                ],
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                              );
                            },
                            barrierDismissible: false);
                      }
                    });
                    Navigator.pop(context);
                  },
                  color: Colors.red,
                  textColor: Colors.white,
                  child: Text('Aceptar'))
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          );
        },
        barrierDismissible: false);
  }

  void editHU() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Editar HU'),
            content: Column(
              children: [
                Text('Funcionalidad'),
                TextField(
                  onChanged: (text) {
                    funsionalidad = text;
                  },
                  decoration: InputDecoration(
                      labelText: "Reuiqero/ necesito/ deseo/ espero/ quiero:"),
                )
              ],
            ),
            actions: <Widget>[
              FlatButton(
                color: Colors.red,
                textColor: Colors.white,
                child: Text('CANCEL'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
              FlatButton(
                color: Colors.green,
                textColor: Colors.white,
                child: Text('OK'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
            ],
          );
        });
  }

  Widget nombreProyecto(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    var lista = snapData.map((proyecto) {
      var ne = {};
      ne['name'] = proyecto.nombre;
      ne['valor'] = proyecto.id_poryect;
      var id = proyecto.nombre.toString();
      return ne;
    }).toList();

    const List<String> list = <String>['One', 'Two', 'Three', 'Four'];
    String dropdownValue = list.first;

    return DropdownButton<Object>(
      value: null,
      icon: const Icon(Icons.arrow_downward),
      elevation: 16,
      hint: Text(Seleccione),
      style: const TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (Object? newValue) {
        // This is called when the user selects an item.
        setState(() {
          var id = newValue.toString().split("-")[1];
          document.cookie = "id=$id; max-age=3600;";
          Seleccione = newValue.toString().split("-")[0];
          getAllHU();
        });
      },
      items: lista.map<DropdownMenuItem<Object>>((value) {
        return DropdownMenuItem<Object>(
          value: value['name'].toString() + '-' + value['valor'].toString(),
          child: Text(value['name']),
        );
      }).toList(),
    );
  }
}
