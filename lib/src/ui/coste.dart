import 'package:flutter/material.dart';
import 'package:flutter_application_spep/src/bloc/person_bloc.dart';
import 'package:flutter_application_spep/src/bloc/proyect_bloc.dart';
import 'package:flutter_application_spep/src/bloc/proyect_bloc2.dart';
import 'package:flutter_application_spep/src/models/person_model.dart';
import 'package:flutter_application_spep/src/models/proyect_model.dart';
import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/ui/uiDrawer.dart';
import 'package:flutter_application_spep/src/ui/tableHU.dart';
import 'package:flutter_application_spep/src/bloc/historyUser_bloc.dart';
import 'package:flutter_application_spep/src/models/userHistory_model.dart';
import 'dart:html';

void main() => runApp(new Coste());

// Definir un componente con estado
class Coste extends StatefulWidget {
  @override
  _CosteState createState() => _CosteState();
}

// Al definir un componente con estado, debe crear una clase de estado para el componente, que hereda de la clase de estado
class _CosteState extends State<Coste> {
  final scaffolKey = GlobalKey<ScaffoldState>();

  GlobalKey<FormState> globalFormkey = new GlobalKey<FormState>();
  bool hidePassword = true;

  UiDrawer uiDrawer = UiDrawer();
  UserData user = UserData();
  TableHU tableHU = TableHU();
  final PersonBloc personBloc = PersonBloc();
  final HistoryUserBloc historyUserBloc = HistoryUserBloc();
  final ProyectBloc proyectBloc = ProyectBloc();
  final ProyectBloc2 proyectBloc2 = ProyectBloc2();
  final fieldTextRequiero = TextEditingController();
  final fieldTextFinalidad = TextEditingController();
  final fieldTextCriterios = TextEditingController();
  final fieldTextCantidadSprints = TextEditingController();
  final fieldTextCompromiso = TextEditingController();
  List<String> itemRol = ['1', '2', '3', '5', '8', '13', '21', '?'];
  String rol = '1';
  String funsionalidad = '';
  String razon = '';
  String criterios = '';
  List<String> itemComplejidad = ['1', '2', '3', '5', '8', '13', '21', '?'];
  String complejidad = '1';
  List<String> itemPrioridad = ['S', 'M', 'C', 'W'];
  String prioridad = 'S';
  List<String> itemDuration = ['1', '2', '3', '4'];
  String duration = '1';
  List<UserHistory> userHistory = [];
  List<Proyect> proyect = [];
  int pesoProyecto = 0;
  int compromisoSprint = 0;
  String cantidadSprints = '';
  String duracion = '';
  String equipoTrabajo = '';
  String historiaMax = '';
  String historiaMin = '';
  String nombre = '';
  String peso = '';
  String pivote = '';
  String tiempo = '1';
  int cant_sprint = 0;
  String compromiso = '1';
  List<Person> person = [];
  var Seleccione = 'Seleccione';
  List<Proyect> proyectList = [];

  initialUserHistory() {
    historyUserBloc.getAllHU();
    personBloc.getAllPersons();
  }

  @override
  void initState() {
    getAllHU();
    pintar();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 224, 229, 255),
      key: scaffolKey,
      appBar: AppBar(
        title: Text("Costos del proyecto"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.update),
            onPressed: () {
              getAllHU();
              pintar();
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: FittedBox(
          child: Column(
            children: <Widget>[
              Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                Container(
                  width: 950,
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.symmetric(vertical: 40, horizontal: 20),
                  margin: EdgeInsets.symmetric(vertical: 35, horizontal: 80),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                          color: Theme.of(context).hintColor.withOpacity(0.2),
                          offset: Offset(0, 10),
                          blurRadius: 20)
                    ],
                  ),
                  child: Column(
                    children: [
                      Center(
                        child: Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'SELECCIONE EL PROYECTO',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15),
                              ),
                              StreamBuilder(
                                stream: proyectBloc2.proyects,
                                builder: (BuildContext contex,
                                    AsyncSnapshot<dynamic> snapData) {
                                  if (!snapData.hasData) {
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                  return nombreProyecto(
                                      snapData.data, proyectBloc2, contex);
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(bottom: 25),
                        child: Row(
                          children: [
                            Text('Equipo de trabajo',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25,
                                )),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          StreamBuilder(
                            stream: personBloc.person,
                            builder: (BuildContext contex,
                                AsyncSnapshot<dynamic> snapData) {
                              if (!snapData.hasData) {
                                return Center(
                                  child: CircularProgressIndicator(),
                                );
                              }
                              return cargaLista(
                                  snapData.data, personBloc, contex);
                            },
                          ),
                        ],
                      ),
                      StreamBuilder(
                        stream: proyectBloc.proyect,
                        builder: (BuildContext contex,
                            AsyncSnapshot<dynamic> snapData) {
                          if (!snapData.hasData) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                          return cargaData(snapData.data, proyectBloc, contex);
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  width: 600,
                  padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                  margin: EdgeInsets.symmetric(vertical: 45, horizontal: 20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Color.fromARGB(255, 255, 255, 255),
                    boxShadow: [
                      BoxShadow(
                          color: Theme.of(context).hintColor.withOpacity(0.2),
                          offset: Offset(0, 10),
                          blurRadius: 20)
                    ],
                  ),
                  child: FittedBox(
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(bottom: 25),
                          child: Row(
                            children: [
                              Text('Destalles de costo',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 25,
                                  )),
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            Container(
                              width: 250,
                              padding: const EdgeInsets.all(20),
                              margin: const EdgeInsets.only(left: 0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        bottom: 5.0, left: 0),
                                    child: Text(
                                      'VALOR SIN PLANILLA',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20),
                                    ),
                                  ),
                                  StreamBuilder(
                                    stream: personBloc.person,
                                    builder: (BuildContext contex,
                                        AsyncSnapshot<dynamic> snapData) {
                                      if (!snapData.hasData) {
                                        return Center(
                                          child: CircularProgressIndicator(),
                                        );
                                      }
                                      return cargaCosteEquipo(
                                          snapData.data, personBloc, contex);
                                    },
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.only(
                                          bottom: 5.0, left: 0),
                                      child: Text(
                                        'VALOR CON PLANILLA',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20),
                                      )),
                                  StreamBuilder(
                                    stream: personBloc.person,
                                    builder: (BuildContext contex,
                                        AsyncSnapshot<dynamic> snapData) {
                                      if (!snapData.hasData) {
                                        return Center(
                                          child: CircularProgressIndicator(),
                                        );
                                      }
                                      return cargaCosteEquipoPlanilla(
                                          snapData.data, personBloc, contex);
                                    },
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.only(
                                          bottom: 5.0, left: 0),
                                      child: Text(
                                        'IVA',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20),
                                      )),
                                  StreamBuilder(
                                    stream: personBloc.person,
                                    builder: (BuildContext contex,
                                        AsyncSnapshot<dynamic> snapData) {
                                      if (!snapData.hasData) {
                                        return Center(
                                          child: CircularProgressIndicator(),
                                        );
                                      }
                                      return cargaCosteIVA(
                                          snapData.data, personBloc, contex);
                                    },
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: 250,
                              padding: const EdgeInsets.all(20),
                              margin: const EdgeInsets.only(left: 60),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                      padding: const EdgeInsets.only(
                                          bottom: 5.0, left: 0),
                                      child: Text(
                                        'COSTE DEL PROYECTO',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20),
                                      )),
                                  StreamBuilder(
                                    stream: personBloc.person,
                                    builder: (BuildContext contex,
                                        AsyncSnapshot<dynamic> snapData) {
                                      if (!snapData.hasData) {
                                        return Center(
                                          child: CircularProgressIndicator(),
                                        );
                                      }
                                      return cargaCosteTotal(
                                          snapData.data, personBloc, contex);
                                    },
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ])
            ],
          ),
        ),
      ),
      drawer: uiDrawer,
    );
  }

  Widget cargaLista(
      List<Person> snapData, PersonBloc personBloc, BuildContext context) {
    return DataTable(
      columns: [
        DataColumn(
          label: Text('ID usuario'),
        ),
        DataColumn(
          label: Text('Nombre'),
        ),
        DataColumn(
          label: Text('Rol'),
        ),
        DataColumn(
          label: Text('Salario'),
        ),
        DataColumn(
          label: Text('Valor por día'),
        ),
        DataColumn(
          label: Text('Valor por hora'),
        ),
      ],
      rows: this
          .person
          .map(
            (person) => DataRow(
              cells: [
                DataCell(
                  Text(person.id_person.toString()),
                ),
                DataCell(
                  Text(person.nombre),
                ),
                DataCell(
                  Text(person.rol),
                ),
                DataCell(
                  Text(numberFormat(person.valor_hora)),
                ),
                DataCell(
                  Text(numberFormat(int.parse(person.valor_hora) / 30)),
                ),
                DataCell(
                  Text(numberFormat((int.parse(person.valor_hora) / 30) / 8)),
                ),
              ],
            ),
          )
          .toList(),
    );
  }

  bool validar() {
    if (funsionalidad != '' && razon != '' && criterios != '') {
      return true;
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Error"),
              content: Text("Llene todos los campos"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Ok'))
              ],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
            );
          },
          barrierDismissible: false);
      return false;
    }
  }

  Widget cargaCosteEquipo(
      List<Person> snapData, PersonBloc personBloc, BuildContext context) {
    var lista = snapData.map((person) {
      int.parse(person.valor_hora);
      return person.valor_hora;
    });

    var valor_hora = lista, suma = 0;
    valor_hora.forEach((numero) {
      suma += int.parse(numero);
    });

    return Container(
      child: StreamBuilder(
        stream: proyectBloc.proyect,
        builder: (BuildContext contex, AsyncSnapshot<dynamic> snapData) {
          if (!snapData.hasData) {
            getAllHU();
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return calCostoProyecto(snapData.data, proyectBloc, contex, suma);
        },
      ),
    );
  }

  Widget cargaCosteEquipoPlanilla(
      List<Person> snapData, PersonBloc personBloc, BuildContext context) {
    var lista = snapData.map((person) {
      int.parse(person.valor_hora);
      return person.valor_hora;
    });

    var valor_hora = lista, suma = 0;
    valor_hora.forEach((numero) {
      suma += int.parse(numero);
    });

    return Container(
      child: StreamBuilder(
        stream: proyectBloc.proyect,
        builder: (BuildContext contex, AsyncSnapshot<dynamic> snapData) {
          if (!snapData.hasData) {
            getAllHU();
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return calCostoProyectoPlanilla(
              snapData.data, proyectBloc, contex, suma);
        },
      ),
    );
  }

  Widget cargaCosteIVA(
      List<Person> snapData, PersonBloc personBloc, BuildContext context) {
    var lista = snapData.map((person) {
      int.parse(person.valor_hora);
      return person.valor_hora;
    });

    var valor_hora = lista, suma = 0;
    valor_hora.forEach((numero) {
      suma += int.parse(numero);
    });

    return Container(
      child: StreamBuilder(
        stream: proyectBloc.proyect,
        builder: (BuildContext contex, AsyncSnapshot<dynamic> snapData) {
          if (!snapData.hasData) {
            getAllHU();
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return calCostoProyectoIVA(snapData.data, proyectBloc, contex, suma);
        },
      ),
    );
  }

  Widget cargaCosteTotal(
      List<Person> snapData, PersonBloc personBloc, BuildContext context) {
    var lista = snapData.map((person) {
      int.parse(person.valor_hora);
      return person.valor_hora;
    });

    var valor_hora = lista, suma = 0;
    valor_hora.forEach((numero) {
      suma += int.parse(numero);
    });

    return Container(
      child: StreamBuilder(
        stream: proyectBloc.proyect,
        builder: (BuildContext contex, AsyncSnapshot<dynamic> snapData) {
          if (!snapData.hasData) {
            getAllHU();
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return calCostoProyectoTotal(
              snapData.data, proyectBloc, contex, suma);
        },
      ),
    );
  }

  Widget cargaCosteDECLARANTE(
      List<Person> snapData, PersonBloc personBloc, BuildContext context) {
    var lista = snapData.map((person) {
      int.parse(person.valor_hora);
      return person.valor_hora;
    });

    var valor_hora = lista, suma = 0;
    valor_hora.forEach((numero) {
      suma += int.parse(numero);
    });

    return Container(
      child: StreamBuilder(
        stream: proyectBloc.proyect,
        builder: (BuildContext contex, AsyncSnapshot<dynamic> snapData) {
          if (!snapData.hasData) {
            getAllHU();
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return calCostoProyectoDECLARANTE(
              snapData.data, proyectBloc, contex, suma);
        },
      ),
    );
  }

  Widget rentabilidad(
      List<Person> snapData, PersonBloc personBloc, BuildContext context) {
    var lista = snapData.map((person) {
      int.parse(person.valor_hora);
      return person.valor_hora;
    });

    var valor_hora = lista, suma = 0;
    valor_hora.forEach((numero) {
      suma += int.parse(numero);
    });

    return Container(
      child: StreamBuilder(
        stream: proyectBloc.proyect,
        builder: (BuildContext contex, AsyncSnapshot<dynamic> snapData) {
          if (!snapData.hasData) {
            getAllHU();
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return calRentabilidad(snapData.data, proyectBloc, contex, suma);
        },
      ),
    );
  }

  Widget costoTotal(
      List<Person> snapData, PersonBloc personBloc, BuildContext context) {
    var lista = snapData.map((person) {
      int.parse(person.valor_hora);
      return person.valor_hora;
    });

    var valor_hora = lista, suma = 0;
    valor_hora.forEach((numero) {
      suma += int.parse(numero);
    });

    return Container(
      child: StreamBuilder(
        stream: proyectBloc.proyect,
        builder: (BuildContext contex, AsyncSnapshot<dynamic> snapData) {
          if (!snapData.hasData) {
            getAllHU();
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return calTotal(snapData.data, proyectBloc, contex, suma);
        },
      ),
    );
  }

  Widget cargaHUMin(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    var lista = snapData.map((userHistory) {
      var id = userHistory.historiaUsuarioId.toString();
      return id;
    }).toList();

    //humin = lista[0].toString();
    return DropdownButton<String>(
      items: lista.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text('HU-' + value),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          historiaMin = newValue!;
          guardarProyect();
          getAllHU();
          pintar();
        });
      },
      hint: Text('HU-' + historiaMin),
      disabledHint: Text("Disabled"),
      elevation: 8,
      style: TextStyle(color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
      icon: Icon(Icons.arrow_drop_down_circle),
      iconDisabledColor: Colors.red,
      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
      isExpanded: true,
    );
  }

  Widget cargaHUMax(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    var lista = snapData.map((userHistory) {
      var id = userHistory.historiaUsuarioId.toString();
      return id;
    }).toList();

    var initialValue = '';

    initialValue = lista[1].toString();
    return DropdownButton<String>(
      items: lista.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text('HU-' + value),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          historiaMax = newValue!;
          guardarProyect();
          getAllHU();
          pintar();
        });
      },
      hint: Text('HU-' + historiaMax),
      disabledHint: Text("Disabled"),
      elevation: 8,
      style: TextStyle(color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
      icon: Icon(Icons.arrow_drop_down_circle),
      iconDisabledColor: Colors.red,
      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
      isExpanded: true,
    );
  }

  Widget cargaPivote(List<UserHistory> snapData,
      HistoryUserBloc historyUserBloc, BuildContext context) {
    var lista = snapData.map((userHistory) {
      var id = userHistory.historiaUsuarioId.toString();
      return id;
    }).toList();

    var initialValue = '';

    initialValue = lista[3].toString();
    return DropdownButton<String>(
      items: lista.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text('HU-' + value),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          pivote = newValue!;
          guardarProyect();
          getAllHU();
          pintar();
        });
      },
      hint: Text('HU-' + pivote),
      disabledHint: Text("Disabled"),
      elevation: 8,
      style: TextStyle(color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
      icon: Icon(Icons.arrow_drop_down_circle),
      iconDisabledColor: Colors.red,
      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
      isExpanded: true,
    );
  }

  Widget cargaSprint(List<UserHistory> snapData,
      HistoryUserBloc historyUserBloc, BuildContext context) {
    //  print('final');
    return FlatButton(
      padding: EdgeInsets.symmetric(
        vertical: 22,
        horizontal: 48,
      ),
      onPressed: () {
        calcularSprints(snapData);
      },
      child: Text(
        'Calular',
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).cursorColor,
      shape: StadiumBorder(),
    );
  }

  Widget cargaCompromiso(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    //  print('final');
    return Padding(
        padding: const EdgeInsets.only(bottom: 5.0, left: 45.0),
        child: TextFormField(
            enabled: false,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            initialValue: snapData[0].compromiso,
            decoration: InputDecoration(border: InputBorder.none),
            onChanged: (text) {
              if (text.length > 0) {
                compromiso = text;
                guardarProyect();
              } else {
                compromiso = '0';
              }
            }));
  }

  Widget cargaNumSprint(List<UserHistory> snapData,
      HistoryUserBloc historyUserBloc, BuildContext context) {
    calcularSprintss(snapData);
    //  print('final');
    return TextFormField(
        controller: fieldTextCantidadSprints,
        decoration: InputDecoration(border: InputBorder.none),
        enabled: false,
        onChanged: (text) {
          cantidadSprints = text;
        });
  }

  Widget cargaTiempo(List<UserHistory> snapData,
      HistoryUserBloc historyUserBloc, BuildContext context) {
    //  print('final');
    return DropdownButton<String>(
      items: itemDuration.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value + ' Semana(s)'),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          tiempo = newValue!;
          guardarProyect();
          getAllHU();
          pintar();
        });
      },
      hint: Text(tiempo + ' Semana(s)'),
      disabledHint: Text("Disabled"),
      elevation: 8,
      style: TextStyle(color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
      icon: Icon(Icons.arrow_drop_down_circle),
      iconDisabledColor: Colors.red,
      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
      isExpanded: true,
    );
  }

  void getAllHU() {
    historyUserBloc.getAllHU();
    proyectBloc.getProyect();
    personBloc.getAllPersons();
    personBloc.person.listen((List<Person> person) {
      this.person = person;
    });
    proyectBloc2.getProyectCorreo();
    proyectBloc2.proyects.listen((List<Proyect> proyectListnew) {
      this.proyectList = proyectListnew;
    });
  }

  void pintar() {
    historyUserBloc.getAllHU();
    historyUserBloc.userHistory.listen((List<UserHistory> userHistory) {
      this.userHistory = userHistory;
    });
  }

  void calcularSprints(snapData) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Alerta"),
            content: Text(
                "Si calcula, la panatalla sprint se actualizará con base en el sistema"),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Cancelar')),
              FlatButton(
                  onPressed: () {
                    alertaSprintok(snapData);
                    Navigator.pop(context);
                  },
                  child: Text('Ok'))
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          );
        },
        barrierDismissible: false);
  }

  void calcularSprintss(snapData) {
    var numbSprint = 1;
    var contador = 0;

    snapData.forEach((numero) {
      contador += int.parse(numero.complejidad);

      if (contador <= int.parse(compromiso)) {
        // print('sprint ' +            numbSprint.toString() +        '  Hu_id ' +            numero.historiaUsuarioId.toString());
      } else {
        numbSprint += 1;
        contador = int.parse(numero.complejidad);
        // print('sprint ' +            numbSprint.toString() +            '  Hu_id ' +            numero.historiaUsuarioId.toString());
      }
    });
    duracion = (int.parse(tiempo) * numbSprint).toString();

    fieldTextCantidadSprints.text = numbSprint.toString();
  }

  void guardarProyect() {
    fieldTextCompromiso.text = compromiso;
    proyectBloc
        .putProyect(
            duracion,
            equipoTrabajo,
            historiaMax,
            historiaMin,
            nombre,
            peso,
            pivote,
            tiempo,
            compromiso,
            '${DateTime.now()}',
            cant_sprint,
            proyect[0].proyectoId,
            proyect[0].id_poryect)
        .then((value) => {getAllHU(), pintar()});
  }

  Widget cargaData(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    proyect = snapData;

    duracion = proyect[0].duracion;
    historiaMax = proyect[0].historiaMax;
    historiaMin = proyect[0].historiaMin;
    peso = proyect[0].peso;
    pivote = proyect[0].pivote;
    tiempo = proyect[0].tiempo;
    compromiso = proyect[0].compromiso;

    return Container();
  }

  Widget AlertCalcular(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    proyect = snapData;

    duracion = proyect[0].duracion;
    historiaMax = proyect[0].historiaMax;
    historiaMin = proyect[0].historiaMin;
    peso = proyect[0].peso;
    pivote = proyect[0].pivote;
    tiempo = proyect[0].tiempo;
    compromiso = proyect[0].compromiso;
    cant_sprint = proyect[0].cant_sprint;
    return Container();
  }

  void alertaSprintok(snapData) {
    var numbSprint = 1;
    var contador = 0;
    snapData.forEach((numero) {
      contador += int.parse(numero.complejidad);

      if (contador <= int.parse(compromiso)) {
        historyUserBloc.updateUserHistory(
          numero.rol,
          numero.funsionalidad,
          numero.razon,
          numero.criterioAceptacion,
          numero.complejidad,
          numero.prioridad,
          numbSprint.toString(),
          numero.encargado,
          numero.estado,
          numero.historiaUsuarioId,
        );
      } else {
        numbSprint += 1;
        contador = int.parse(numero.complejidad);
        historyUserBloc.updateUserHistory(
          numero.rol,
          numero.funsionalidad,
          numero.razon,
          numero.criterioAceptacion,
          numero.complejidad,
          numero.prioridad,
          numbSprint.toString(),
          numero.encargado,
          numero.estado,
          numero.historiaUsuarioId,
        );
      }
    });
    duracion = (int.parse(tiempo) * numbSprint).toString();

    fieldTextCantidadSprints.text = numbSprint.toString();

    guardarProyect();
  }

  void guardarHU(hu) {
    historyUserBloc
        .updateUserHistory(
          hu.rol,
          hu.funsionalidad,
          hu.razon,
          hu.criterioAceptacion,
          hu.complejidad,
          hu.prioridad,
          hu.sprint,
          hu.encargado,
          hu.estado,
          hu.historiaUsuarioId,
        )
        .then((value) => {getAllHU(), pintar()});
  }

  Widget cargaNumberSprint(List<UserHistory> snapData,
      HistoryUserBloc historyUserBloc, BuildContext context) {
    var listaSprint = [];
    var contador = 0;

    for (var i = 0; i < int.parse(snapData[snapData.length - 1].sprint); i++) {
      contador++;
      listaSprint.add(contador);
    }
    return Center(
        child: Column(
            children: listaSprint
                .map((item) => Container(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding:
                              const EdgeInsets.only(bottom: 5.0, left: 15.0),
                          child: Text(
                            'Sprint ' + item.toString(),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          ),
                        ),
                        StreamBuilder(
                          stream: historyUserBloc.userHistory,
                          builder: (BuildContext contex,
                              AsyncSnapshot<dynamic> snapData) {
                            if (!snapData.hasData) {
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            }
                            return cargaPeso(snapData.data, historyUserBloc,
                                contex, item.toString());
                          },
                        ),
                      ],
                    )))
                .toList()));
  }

  Widget cargaPeso(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context, id) {
    var peso = 0;
    for (var i = 0; i < snapData.length; i++) {
      if (snapData[i].sprint == id) {
        peso = peso + int.parse(snapData[i].complejidad);
      }
    }

    return Padding(
        padding: const EdgeInsets.only(bottom: 25.0, left: 45.0),
        child: Text(
          'Peso: ' + peso.toString(),
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18,
            fontFamily: "Sans",
            color: Colors.black54,
            letterSpacing: 0.5,
          ),
        ));
  }

  Widget calCostoProyecto(List<Proyect> snapData, ProyectBloc proyectBloc,
      BuildContext context, valor_equipo) {
    return Padding(
        padding: const EdgeInsets.only(bottom: 5.0, left: 45.0),
        child: TextFormField(
          style: TextStyle(fontSize: 20),
          decoration: InputDecoration(border: InputBorder.none),
          initialValue: 'COP ' +
              numberFormat(
                  valor_equipo * (int.parse(snapData[0].duracion) * 0.25)),
        ));
  }

  Widget calCostoProyectoPlanilla(List<Proyect> snapData,
      ProyectBloc proyectBloc, BuildContext context, valor_equipo) {
    return Padding(
        padding: const EdgeInsets.only(bottom: 5.0, left: 45.0),
        child: TextFormField(
          style: TextStyle(fontSize: 20),
          decoration: InputDecoration(border: InputBorder.none),
          initialValue: 'COP ' +
              numberFormat(((valor_equipo * 0.4) + valor_equipo) *
                  (int.parse(snapData[0].duracion) * 0.25)),
        ));
  }

  Widget calCostoProyectoIVA(List<Proyect> snapData, ProyectBloc proyectBloc,
      BuildContext context, valor_equipo) {
    return Padding(
        padding: const EdgeInsets.only(bottom: 5.0, left: 45.0),
        child: TextFormField(
          style: TextStyle(fontSize: 20),
          decoration: InputDecoration(border: InputBorder.none),
          initialValue: 'COP ' +
              numberFormat(((valor_equipo * 0.19)) *
                  (int.parse(snapData[0].duracion) * 0.25)),
        ));
  }

  Widget calCostoProyectoDECLARANTE(List<Proyect> snapData,
      ProyectBloc proyectBloc, BuildContext context, valor_equipo) {
    return Padding(
        padding: const EdgeInsets.only(bottom: 5.0, left: 45.0),
        child: TextFormField(
          style: TextStyle(fontSize: 20),
          decoration: InputDecoration(border: InputBorder.none),
          initialValue: 'COP ' +
              numberFormat(((valor_equipo * 0.11)) *
                  (int.parse(snapData[0].duracion) * 0.25)),
        ));
  }

  Widget calRentabilidad(List<Proyect> snapData, ProyectBloc proyectBloc,
      BuildContext context, valor_equipo) {
    //print((valor_equipo * 0.25));
    return Padding(
        padding: const EdgeInsets.only(bottom: 5.0, left: 45.0),
        child: TextFormField(
          style: TextStyle(fontSize: 20),
          decoration: InputDecoration(border: InputBorder.none),
          initialValue: 'COP ' +
              numberFormat((((valor_equipo * 0.4) +
                          (valor_equipo * 0.11) +
                          (valor_equipo * 0.19) +
                          valor_equipo) *
                      0.25) *
                  (int.parse(snapData[0].duracion) * 0.25)),
        ));
  }

  Widget calCostoProyectoTotal(List<Proyect> snapData, ProyectBloc proyectBloc,
      BuildContext context, valor_equipo) {
    //print((valor_equipo * 0.25));
    return Padding(
        padding: const EdgeInsets.only(bottom: 5.0, left: 45.0),
        child: TextFormField(
          style: TextStyle(fontSize: 20),
          decoration: InputDecoration(border: InputBorder.none),
          initialValue: 'COP ' +
              numberFormat(((valor_equipo * 0.4) +
                      (valor_equipo * 0.19) +
                      valor_equipo) *
                  (int.parse(snapData[0].duracion) * 0.25)),
        ));
  }

  Widget calTotal(List<Proyect> snapData, ProyectBloc proyectBloc,
      BuildContext context, valor_equipo) {
    // print((valor_equipo * 0.25));
    return Padding(
        padding: const EdgeInsets.only(bottom: 5.0, left: 45.0),
        child: TextFormField(
          style: TextStyle(fontSize: 20),
          decoration: InputDecoration(border: InputBorder.none),
          initialValue: 'COP ' +
              numberFormat(((valor_equipo * 0.4) +
                      (valor_equipo * 0.11) +
                      (valor_equipo * 0.19) +
                      (((valor_equipo * 0.4) +
                              (valor_equipo * 0.11) +
                              (valor_equipo * 0.19) +
                              valor_equipo) *
                          0.25) +
                      valor_equipo) *
                  (int.parse(snapData[0].duracion) * 0.25)),
        ));
  }

  String numberFormat(x) {
    List<String> parts = x.toString().split('.');
    RegExp re = RegExp(r'\B(?=(\d{3})+(?!\d))');

    parts[0] = parts[0].replaceAll(re, '.');
    if (parts.length == 1) {
    } else {
      parts[1] = parts[1].padRight(2, '0').substring(0, 2);
    }
    return parts.join(',');
  }

  Widget nombreProyecto(
      List<Proyect> snapData, ProyectBloc2 proyectBloc2, BuildContext context) {
    var lista = snapData.map((proyecto) {
      var ne = {};
      ne['name'] = proyecto.nombre;
      ne['valor'] = proyecto.id_poryect;
      var id = proyecto.nombre.toString();
      return ne;
    }).toList();

    const List<String> list = <String>['One', 'Two', 'Three', 'Four'];
    String dropdownValue = list.first;

    return DropdownButton<Object>(
      value: null,
      icon: const Icon(Icons.arrow_downward),
      elevation: 16,
      hint: Text(Seleccione),
      style: const TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (Object? newValue) {
        // This is called when the user selects an item.
        setState(() {
          var id = newValue.toString().split("-")[1];
          document.cookie = "id=$id; max-age=3600;";
          Seleccione = newValue.toString().split("-")[0];
          getAllHU();
        });
      },
      items: lista.map<DropdownMenuItem<Object>>((value) {
        return DropdownMenuItem<Object>(
          value: value['name'].toString() + '-' + value['valor'].toString(),
          child: Text(value['name']),
        );
      }).toList(),
    );
  }
}
