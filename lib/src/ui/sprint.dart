import 'package:flutter/material.dart';
import 'package:flutter_application_spep/src/bloc/person_bloc.dart';
import 'package:flutter_application_spep/src/bloc/proyect_bloc.dart';
import 'package:flutter_application_spep/src/bloc/proyect_bloc2.dart';
import 'package:flutter_application_spep/src/models/person_model.dart';
import 'package:flutter_application_spep/src/ui/uiDrawer.dart';
import 'package:flutter_application_spep/src/ui/tableHU.dart';
import 'package:flutter_application_spep/src/ui/editSprint.dart';
import 'package:flutter_application_spep/src/bloc/historyUser_bloc.dart';
import 'package:flutter_application_spep/src/models/userHistory_model.dart';
import 'package:flutter_application_spep/src/models/proyect_model.dart';
import 'dart:html';

class Sprint extends StatefulWidget {
  @override
  _SprintState createState() => _SprintState();
}

class UserData {
  String username = '';
  String password = '';
}

class _SprintState extends State<Sprint> {
  final scaffolKey = GlobalKey<ScaffoldState>();

  GlobalKey<FormState> globalFormkey = new GlobalKey<FormState>();
  bool hidePassword = true;
  DateTime selectedDate = DateTime.now();
  UiDrawer uiDrawer = UiDrawer();
  UserData user = UserData();
  TableHU tableHU = TableHU();

  final ProyectBloc2 proyectBloc2 = ProyectBloc2();
  final HistoryUserBloc historyUserBloc = HistoryUserBloc();
  final PersonBloc personBloc = PersonBloc();
  final ProyectBloc proyectBloc = ProyectBloc();
  final fieldTextRequiero = TextEditingController();
  final fieldTextFinalidad = TextEditingController();
  final fieldTextCriterios = TextEditingController();
  final fieldTextCantidadSprints = TextEditingController();
  List<String> itemRol = ['1', '2', '3', '5', '8', '13', '21', '?'];
  String rol = '1';
  String funsionalidad = '';
  String razon = '';
  String criterios = '';
  List<String> itemComplejidad = ['1', '2', '3', '5', '8', '13', '21', '?'];
  String complejidad = '1';
  List<String> itemPrioridad = ['S', 'M', 'C', 'W'];
  String prioridad = 'S';
  List<String> itemDuration = ['1', '2', '3', '4'];
  List<String> itemEstado = [
    'No iniciado',
    'En proceso',
    'En pruebas',
    'Terminado'
  ];
  String duration = '1';
  List<UserHistory> userHistory = [];
  String nombre = 'seleccione';
  List<String> listPerson = [];
  int pesoProyecto = 0;
  int compromisoSprint = 0;
  String cantidadSprints = '';
  String estado = 'Seleccione';
  bool isChecked = true;
  var Seleccione = 'Seleccione';
  List<Proyect> proyectList = [];

  initialUserHistory() {
    historyUserBloc.getAllHU();
  }

  @override
  void initState() {
    getAllHU();
    getAllPerson();
    getAllProyect();
    pintar();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 224, 229, 255),
      key: scaffolKey,
      appBar: AppBar(
        title: Text("Lista de Sprint"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.update),
            onPressed: () {
              getAllHU();
              getAllPerson();
              getAllProyect();
              pintar();
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: 500,
              padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
              margin: EdgeInsets.symmetric(vertical: 35, horizontal: 20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: Theme.of(context).hintColor.withOpacity(0.2),
                      offset: Offset(0, 10),
                      blurRadius: 20)
                ],
              ),
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'SELECCIONE EL PROYECTO',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                    ),
                    StreamBuilder(
                      stream: proyectBloc2.proyects,
                      builder: (BuildContext contex,
                          AsyncSnapshot<dynamic> snapData) {
                        if (!snapData.hasData) {
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                        return nombreProyecto(
                            snapData.data, proyectBloc2, contex);
                      },
                    ),
                  ],
                ),
              ),
            ),
            StreamBuilder(
              stream: proyectBloc.proyect,
              builder: (BuildContext contex, AsyncSnapshot<dynamic> snapData) {
                if (!snapData.hasData) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                return cargaSprint(snapData.data, proyectBloc, contex);
              },
            ),
            StreamBuilder(
              stream: personBloc.person,
              builder: (BuildContext contex, AsyncSnapshot<dynamic> snapData) {
                if (!snapData.hasData) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                return encargado(snapData.data, historyUserBloc, contex);
              },
            ),
          ],
        ),
      ),
      drawer: uiDrawer,
    );
  }

  Widget cargaSprint(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    var listaSprint = [];
    var contador = 0;

    if (snapData.length != 0) {
      for (var i = 0; i < snapData[0].cant_sprint; i++) {
        contador++;
        listaSprint.add(contador);
      }
    }

    //getAllProyect();

    return Center(
        child: Column(
            children: listaSprint
                .map((item) => Container(
                      width: 900,
                      padding:
                          EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                      margin:
                          EdgeInsets.symmetric(vertical: 35, horizontal: 20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color:
                                  Theme.of(context).hintColor.withOpacity(0.2),
                              offset: Offset(0, 10),
                              blurRadius: 20)
                        ],
                      ),
                      child: Column(
                        children: [
                          Text('Sprint ' + item.toString(),
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 25,
                              )),
                          StreamBuilder(
                            stream: proyectBloc.proyect,
                            builder: (BuildContext contex,
                                AsyncSnapshot<dynamic> snapData) {
                              if (!snapData.hasData) {
                                return Center(
                                  child: CircularProgressIndicator(),
                                );
                              }
                              return fechas(
                                  snapData.data, proyectBloc, contex, item);
                            },
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                  margin:
                                      const EdgeInsets.only(right: 0, top: 0),
                                  child: FlatButton(
                                    padding: EdgeInsets.symmetric(
                                      vertical: 22,
                                      horizontal: 48,
                                    ),
                                    onPressed: () {
                                      Navigator.of(context)
                                          .push(MaterialPageRoute(
                                        builder: (context) => EditSprint(),
                                      ));
                                    },
                                    child: Text(
                                      'Editar',
                                      style: TextStyle(
                                          color: Color.fromARGB(
                                              255, 255, 255, 255)),
                                    ),
                                    color: Color.fromARGB(255, 244, 162, 54),
                                    shape: StadiumBorder(),
                                  )),
                            ],
                          ),
                          StreamBuilder(
                            stream: historyUserBloc.userHistory,
                            builder: (BuildContext contex,
                                AsyncSnapshot<dynamic> snapData) {
                              if (!snapData.hasData) {
                                return Center(
                                  child: CircularProgressIndicator(),
                                );
                              }
                              return cargaLista(snapData.data, historyUserBloc,
                                  contex, item.toString());
                            },
                          ),
                        ],
                      ),
                    ))
                .toList()));
  }

  Widget cargaTabla(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    var lista = snapData.map((userHistory) {
      var id = userHistory.historiaUsuarioId.toString();
      return id;
    }).toList();

    var initialValue = '';

    initialValue = lista[3].toString();
    return DropdownButton<String>(
      value: initialValue,
      items: lista.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text('HU-' + value),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          initialValue = newValue!;
        });
      },
      hint: Text("Select item"),
      disabledHint: Text("Disabled"),
      elevation: 8,
      style: TextStyle(color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
      icon: Icon(Icons.arrow_drop_down_circle),
      iconDisabledColor: Colors.red,
      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
      isExpanded: true,
    );
  }

  void getAllHU() {
    historyUserBloc.getAllHU();
    proyectBloc2.getProyectCorreo();
    proyectBloc2.proyects.listen((List<Proyect> proyectListnew) {
      this.proyectList = proyectListnew;
    });
  }

  void getAllPerson() {
    personBloc.getAllPersons();
  }

  void getAllProyect() {
    proyectBloc.getProyect();
  }

  void pintar() {
    historyUserBloc.getAllHU();
    historyUserBloc.userHistory.listen((List<UserHistory> userHistory) {
      this.userHistory = userHistory;
    });
  }

  int calcularSprints() {
    fieldTextCantidadSprints.text =
        (pesoProyecto / compromisoSprint).toStringAsFixed(0);
    return 5;
  }

  Widget cargaLista(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context, id) {
    var listaporSprint = [];
    var peso = 0;

    bool isChecked = false;
    for (var i = 0; i < snapData.length; i++) {
      if (snapData[i].sprint == id) {
        listaporSprint.add(snapData[i]);
      }
    }

    for (var i = 0; i < snapData.length; i++) {
      if (snapData[i].sprint == id) {
        peso = peso + int.parse(snapData[i].complejidad);
      }
    }

    return Container(
        margin: const EdgeInsets.only(bottom: 25),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Column(mainAxisAlignment: MainAxisAlignment.end, children: [
            DataTable(
              showCheckboxColumn: true,
              columns: [
                DataColumn(
                  label: Text('ID'),
                ),
                DataColumn(
                  label: Text('Complejidad'),
                ),
                DataColumn(
                  label: Text('Prioridad'),
                ),
                DataColumn(
                  label: Text('Encargado'),
                ),
                DataColumn(
                  label: Text('Estado'),
                ),
              ],
              rows: listaporSprint
                  .map(
                    (userHistory) => DataRow(
                      cells: [
                        DataCell(
                          Text(
                              'HU-' + userHistory.historiaUsuarioId.toString()),
                        ),
                        DataCell(
                          Text(userHistory.complejidad),
                          // Text(userHistory.complejidad),
                        ),
                        DataCell(
                          Text(userHistory.prioridad),
                        ),
                        DataCell(
                          DropdownButton<String>(
                            value: null,
                            items: listPerson
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                            onChanged: (newValue) {
                              setState(() {
                                userHistory.encargado = newValue!;
                                agregarEncargado(userHistory);
                              });
                            },
                            hint: Text(userHistory.encargado),
                            disabledHint: Text("Disabled"),
                            elevation: 8,
                            style: TextStyle(
                                color: Color.fromARGB(255, 76, 94, 175),
                                fontSize: 16),
                            icon: Icon(Icons.arrow_drop_down_circle),
                            iconDisabledColor: Colors.red,
                            iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
                            isExpanded: true,
                          ),
                        ),
                        DataCell(
                          DropdownButton<String>(
                            items: itemEstado
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                            onChanged: (newValue) {
                              setState(() {
                                userHistory.estado = newValue!;
                                agregarEstado(userHistory);
                              });
                            },
                            hint: Text(userHistory.estado),
                            disabledHint: Text("Disabled"),
                            elevation: 8,
                            style: TextStyle(
                                color: Color.fromARGB(255, 76, 94, 175),
                                fontSize: 16),
                            icon: Icon(Icons.arrow_drop_down_circle),
                            iconDisabledColor: Colors.red,
                            iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
                            isExpanded: true,
                          ),
                        ),
                      ],
                    ),
                  )
                  .toList(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.only(right: 500, top: 40),
                  child: Text('Peso del sprint: ' + peso.toString(),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 19,
                      )),
                ),
              ],
            )
          ])
        ]));
  }

  Widget encargado(List<Person> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    getAllProyect();
    var lista = snapData.map((person) {
      var id = person.nombre.toString();
      return id;
    }).toList();

    listPerson = lista;
    return Text('');
  }

  Widget fechas(List<Proyect> snapData, ProyectBloc proyectBloc,
      BuildContext context, item) {
    int numberSprint = item;
    DateTime dateini = DateTime.parse(snapData[0].fecha_inicio);
    DateTime dateend = DateTime.parse(snapData[0].fecha_inicio);

    //dateini = dateini.add(        Duration(days: (int.parse(snapData[0].tiempo) * 7) * numberSprint - 1));

    dateend = dateend.add(
        Duration(days: (int.parse(snapData[0].tiempo) * 7) * numberSprint));
    dateini = dateini.add(Duration(
        days: (int.parse(snapData[0].tiempo) * 7) * (numberSprint - 1)));

    //print(dateini);
    // print(dateend);
    return Column(
      children: [
        Text('Fecha inicio: ' +
            "${dateini.day}/${dateini.month}/${dateini.year}"),
        Text('Fecha fin: ' + "${dateend.day}/${dateend.month}/${dateend.year}"),
      ],
    );
  }

  void agregarEncargado(userHistory) {
    historyUserBloc.updateUserHistory(
      userHistory.rol,
      userHistory.funsionalidad,
      userHistory.razon,
      userHistory.criterioAceptacion,
      userHistory.complejidad,
      userHistory.prioridad,
      userHistory.sprint,
      userHistory.encargado,
      userHistory.estado,
      userHistory.historiaUsuarioId,
    );
  }

  void agregarEstado(userHistory) {
    historyUserBloc.updateUserHistory(
      userHistory.rol,
      userHistory.funsionalidad,
      userHistory.razon,
      userHistory.criterioAceptacion,
      userHistory.complejidad,
      userHistory.prioridad,
      userHistory.sprint,
      userHistory.encargado,
      userHistory.estado,
      userHistory.historiaUsuarioId,
    );
  }

  _selectDate(BuildContext context) async {
    final DateTime? selected = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(2010),
      lastDate: DateTime(2025),
    );
    if (selected != null && selected != selectedDate)
      setState(() {
        selectedDate = selected;
      });
  }

  Widget nombreProyecto(
      List<Proyect> snapData, ProyectBloc2 proyectBloc2, BuildContext context) {
    var lista = snapData.map((proyecto) {
      var ne = {};
      ne['name'] = proyecto.nombre;
      ne['valor'] = proyecto.id_poryect;
      var id = proyecto.nombre.toString();
      return ne;
    }).toList();

    const List<String> list = <String>['One', 'Two', 'Three', 'Four'];
    String dropdownValue = list.first;

    return DropdownButton<Object>(
      value: null,
      icon: const Icon(Icons.arrow_downward),
      elevation: 16,
      hint: Text(Seleccione),
      style: const TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (Object? newValue) {
        // This is called when the user selects an item.
        setState(() {
          var id = newValue.toString().split("-")[1];
          document.cookie = "id=$id; max-age=3600;";
          Seleccione = newValue.toString().split("-")[0];
          getAllHU();
        });
      },
      items: lista.map<DropdownMenuItem<Object>>((value) {
        return DropdownMenuItem<Object>(
          value: value['name'].toString() + '-' + value['valor'].toString(),
          child: Text(value['name']),
        );
      }).toList(),
    );
  }
}
