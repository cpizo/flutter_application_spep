import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_application_spep/src/bloc/person_bloc.dart';
import 'package:flutter_application_spep/src/bloc/proyect_bloc.dart';
import 'package:flutter_application_spep/src/bloc/proyect_bloc2.dart';
import 'package:flutter_application_spep/src/models/person_model.dart';
import 'package:flutter_application_spep/src/models/proyect_model.dart';
import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/repository/getToken.dart';
import 'package:flutter_application_spep/src/ui/editWorkTeam.dart';
import 'package:flutter_application_spep/src/ui/insertWorkTeam.dart';
import 'package:flutter_application_spep/src/ui/uiDrawer.dart';
import 'package:flutter_application_spep/src/ui/tableHU.dart';
import 'package:flutter_application_spep/src/bloc/historyUser_bloc.dart';
import 'package:flutter_application_spep/src/models/userHistory_model.dart';
import 'dart:html';

void main() => runApp(new ConfigBusiness());

// Definir un componente con estado
class ConfigBusiness extends StatefulWidget {
  @override
  _ConfigBusinessState createState() => _ConfigBusinessState();
}

// Al definir un componente con estado, debe crear una clase de estado para el componente, que hereda de la clase de estado
class _ConfigBusinessState extends State<ConfigBusiness> {
  final scaffolKey = GlobalKey<ScaffoldState>();

  GlobalKey<FormState> globalFormkey = new GlobalKey<FormState>();
  bool hidePassword = true;

  UiDrawer uiDrawer = UiDrawer();
  UserData user = UserData();
  TableHU tableHU = TableHU();

  final ProyectBloc2 proyectBloc2 = ProyectBloc2();
  CookieManager cookieManager = CookieManager();
  Random random = new Random();
  final PersonBloc personBloc = PersonBloc();
  final ProyectBloc proyectBloc = ProyectBloc();
  final fieldTextRequiero = TextEditingController();
  final fieldTextFinalidad = TextEditingController();
  final fieldTextCriterios = TextEditingController();
  final fieldTextCantidadSprints = TextEditingController();
  List<String> itemRol = ['1', '2', '3', '5', '8', '13', '21', '?'];
  String rol = '1';
  String funsionalidad = '';
  String razon = '';
  String criterios = '';
  List<String> itemComplejidad = ['1', '2', '3', '5', '8', '13', '21', '?'];
  String complejidad = '1';
  List<String> itemPrioridad = ['S', 'M', 'C', 'W'];
  String prioridad = 'S';
  List<String> itemDuration = ['1', '2', '3', '4'];
  String duration = '1';
  List<UserHistory> userHistory = [];
  int pesoProyecto = 0;
  int compromisoSprint = 0;
  String cantidadSprints = '';
  String nombre = '';
  List<String> listPerson = [];
  final fieldTextNombre = TextEditingController();
  var Seleccione = 'Seleccione';

  List<Proyect> proyectList = [];
  List<Person> person = [];
  List<Proyect> proyecto = [];

  initialUserHistory() {
    proyectBloc.getProyect();
  }

  @override
  void initState() {
    getAllProyect();
    getAllHU();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 224, 229, 255),
      key: scaffolKey,
      appBar: AppBar(
        title: Text("Datos Generales del proyecto"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.update),
            onPressed: () {
              getAllProyect();
              getAllHU();
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Center(
              child: Container(
                width: 900,
                padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                margin: EdgeInsets.symmetric(vertical: 45, horizontal: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Color.fromARGB(255, 255, 255, 255),
                  boxShadow: [
                    BoxShadow(
                        color: Theme.of(context).hintColor.withOpacity(0.2),
                        offset: Offset(0, 10),
                        blurRadius: 20)
                  ],
                ),
                child: Column(
                  children: [
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'SELECCIONE EL PROYECTO',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 15),
                          ),
                          StreamBuilder(
                            stream: proyectBloc2.proyects,
                            builder: (BuildContext contex,
                                AsyncSnapshot<dynamic> snapData) {
                              if (!snapData.hasData) {
                                return Center(
                                  child: CircularProgressIndicator(),
                                );
                              }
                              return nombreProyecto2(
                                  snapData.data, proyectBloc2, contex);
                            },
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(bottom: 25),
                      child: Row(
                        children: [
                          Text('Resumen del proyecto',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 25,
                              )),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          width: 300,
                          padding: const EdgeInsets.all(20),
                          margin: const EdgeInsets.only(left: 60),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'NOMBRE DEL PROYECTO',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15),
                              ),
                              StreamBuilder(
                                stream: proyectBloc.proyect,
                                builder: (BuildContext contex,
                                    AsyncSnapshot<dynamic> snapData) {
                                  if (!snapData.hasData) {
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                  return nombreProyecto(
                                      snapData.data, proyectBloc, contex);
                                },
                              ),
                              Text(
                                'PESO DEL PROYECTO',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15),
                              ),
                              StreamBuilder(
                                stream: proyectBloc.proyect,
                                builder: (BuildContext contex,
                                    AsyncSnapshot<dynamic> snapData) {
                                  if (!snapData.hasData) {
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                  return cargaText(
                                      snapData.data, proyectBloc, contex);
                                },
                              ),
                              Text(
                                'DURACIÓN EN SEMANAS DEL PROYECTO',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15),
                              ),
                              StreamBuilder(
                                stream: proyectBloc.proyect,
                                builder: (BuildContext contex,
                                    AsyncSnapshot<dynamic> snapData) {
                                  if (!snapData.hasData) {
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                  return cargaDuracion(
                                      snapData.data, proyectBloc, contex);
                                },
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: 300,
                          padding: const EdgeInsets.all(20),
                          margin: const EdgeInsets.only(left: 60),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'COMPROMISO POR SPRINT',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15),
                              ),
                              StreamBuilder(
                                stream: proyectBloc.proyect,
                                builder: (BuildContext contex,
                                    AsyncSnapshot<dynamic> snapData) {
                                  if (!snapData.hasData) {
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                  return cargaCompromiso(
                                      snapData.data, proyectBloc, contex);
                                },
                              ),
                              Text(
                                'CANTIDAD DE INTEGRANTES',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15),
                              ),
                              StreamBuilder(
                                stream: personBloc.person,
                                builder: (BuildContext contex,
                                    AsyncSnapshot<dynamic> snapData) {
                                  if (!snapData.hasData) {
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                  return cargaIntegrantes(
                                      snapData.data, personBloc, contex);
                                },
                              ),
                              Text(
                                'COSTO DEL PROYECTO',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15),
                              ),
                              StreamBuilder(
                                stream: personBloc.person,
                                builder: (BuildContext contex,
                                    AsyncSnapshot<dynamic> snapData) {
                                  if (!snapData.hasData) {
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                  return costoProyecto(
                                      snapData.data, personBloc, contex);
                                },
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Center(
              child: Container(
                  width: 900,
                  padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                  margin: EdgeInsets.symmetric(vertical: 45, horizontal: 20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Color.fromARGB(255, 255, 255, 255),
                    boxShadow: [
                      BoxShadow(
                          color: Theme.of(context).hintColor.withOpacity(0.2),
                          offset: Offset(0, 10),
                          blurRadius: 20)
                    ],
                  ),
                  child: Row(children: [
                    Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(bottom: 25),
                          child: Row(
                            children: [
                              Text('Lista de Proyectos',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 25,
                                  )),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            FlatButton(
                              padding: EdgeInsets.symmetric(
                                vertical: 18,
                                horizontal: 38,
                              ),
                              onPressed: () {
                                nuevoProyecto();
                              },
                              child: Text(
                                'Crear nuevo Proyecto',
                                style: TextStyle(color: Colors.white),
                              ),
                              color: Theme.of(context).cursorColor,
                              shape: StadiumBorder(),
                            ),
                          ],
                        ),
                        Row(children: [
                          StreamBuilder(
                            stream: proyectBloc.proyect,
                            builder: (BuildContext contex,
                                AsyncSnapshot<dynamic> snapData) {
                              if (!snapData.hasData) {
                                return Center(
                                  child: CircularProgressIndicator(),
                                );
                              }
                              return cargaListaProyectos(
                                  snapData.data, proyectBloc2, contex);
                            },
                          ),
                        ]),
                      ],
                    )
                  ])),
            )
          ],
        ),
      ),
      drawer: uiDrawer,
    );
  }

  Widget cargaLista(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    return DataTable(
      columns: [
        DataColumn(
          label: Text('Id Historia de usuario'),
        ),
        DataColumn(
          label: Text('Complejidad'),
        ),
        DataColumn(
          label: Text('Prioridad'),
        ),
        DataColumn(
          label: Text('Ver más'),
        ),
      ],
      rows: this
          .userHistory
          .map(
            (userHistory) => DataRow(
              cells: [
                DataCell(
                  Text('HU-' + userHistory.historiaUsuarioId.toString()),
                ),
                DataCell(
                  DropdownButton<String>(
                    value: userHistory.complejidad,
                    items:
                        itemRol.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        userHistory.complejidad = newValue!;
                      });
                    },
                    hint: Text("Select item"),
                    disabledHint: Text("Disabled"),
                    elevation: 8,
                    style: TextStyle(
                        color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
                    icon: Icon(Icons.arrow_drop_down_circle),
                    iconDisabledColor: Colors.red,
                    iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
                    isExpanded: true,
                  ),
                  // Text(userHistory.complejidad),
                ),
                DataCell(
                  DropdownButton<String>(
                    value: userHistory.prioridad,
                    items: itemPrioridad
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        userHistory.prioridad = newValue!;
                      });
                    },
                    hint: Text("Select item"),
                    disabledHint: Text("Disabled"),
                    elevation: 8,
                    style: TextStyle(
                        color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
                    icon: Icon(Icons.arrow_drop_down_circle),
                    iconDisabledColor: Colors.red,
                    iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
                    isExpanded: true,
                  ),
                ),
                DataCell(
                  IconButton(
                    icon: Icon(Icons.remove_red_eye_outlined),
                    onPressed: () {},
                    color: Color.fromARGB(255, 236, 170, 71),
                  ),
                )
              ],
            ),
          )
          .toList(),
    );
  }

  Widget cargaListaProyectos(
      List<Proyect> snapData, ProyectBloc2 proyectBloc2, BuildContext context) {
    return DataTable(
      columns: [
        DataColumn(
          label: Text('ID'),
        ),
        DataColumn(
          label: Text('Nombre'),
        ),
        DataColumn(
          label: Text('Peso del proyecto'),
        ),
        DataColumn(
          label: Text('Cantidad de Sprint'),
        ),
        DataColumn(
          label: Text('Fecha de inicio'),
        ),
        DataColumn(
          label: Text('Acciones'),
        ),
      ],
      rows: this
          .proyectList
          .map(
            (proyecto) => DataRow(
              cells: [
                DataCell(
                  Text(proyecto.proyectoId.toString()),
                ),
                DataCell(
                  Text(proyecto.nombre),
                ),
                DataCell(
                  Text(proyecto.peso),
                ),
                DataCell(
                  Text(numberFormat(proyecto.cant_sprint)),
                ),
                DataCell(
                  Text(DateTime.parse(proyecto.fecha_inicio).day.toString() +
                      "/" +
                      DateTime.parse(proyecto.fecha_inicio).month.toString() +
                      "/" +
                      DateTime.parse(proyecto.fecha_inicio).year.toString()),
                ),
                DataCell(Row(
                  children: [
                    IconButton(
                      icon: Icon(Icons.edit_outlined),
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text("Actualice el nombre del proyecto"),
                                content: TextFormField(
                                  initialValue: proyecto.nombre,
                                  onChanged: (text) {
                                    proyecto.nombre = text;
                                  },
                                  decoration:
                                      InputDecoration(labelText: "Nombre"),
                                ),
                                actions: <Widget>[
                                  FlatButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: Text('Cancelar')),
                                  FlatButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                        proyectBloc
                                            .putProyect(
                                                proyecto.duracion,
                                                proyecto.nombre,
                                                proyecto.historiaMax,
                                                proyecto.historiaMin,
                                                proyecto.nombre,
                                                proyecto.peso,
                                                proyecto.pivote,
                                                proyecto.tiempo,
                                                proyecto.compromiso,
                                                proyecto.fecha_inicio,
                                                proyecto.cant_sprint,
                                                proyecto.proyectoId,
                                                proyecto.id_poryect)
                                            .then((value) => {getAllHU()});
                                      },
                                      color: Colors.red,
                                      textColor: Colors.white,
                                      child: Text('Aceptar'))
                                ],
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                              );
                            },
                            barrierDismissible: false);
                      },
                      color: Color.fromARGB(255, 244, 162, 54),
                    ),
                    IconButton(
                      icon: Icon(Icons.delete),
                      onPressed: () {
                        eliminar(proyecto.proyectoId);
                      },
                      color: Colors.red,
                    ),
                  ],
                )),
              ],
            ),
          )
          .toList(),
    );
  }

  void editarP(int id, String name) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Actualice el nombre del proyecto"),
            content: TextFormField(
              initialValue: name,
              onChanged: (text) {
                name = text;
              },
              decoration: InputDecoration(labelText: "Nombre"),
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Cancelar')),
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  color: Colors.red,
                  textColor: Colors.white,
                  child: Text('Aceptar'))
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          );
        },
        barrierDismissible: false);
  }

  void eliminar(int id) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Alerta"),
            content: Text("Desea eliminar el proyecto " + id.toString() + '?'),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Cancelar')),
              FlatButton(
                  onPressed: () {
                    proyectBloc.deleterProyect(id).then((bool estado) {
                      if (estado) {
                        getAllProyect();
                        getAllHU();
                      } else {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text("Error"),
                                content:
                                    Text("No se pudo eliminar el usuario "),
                                actions: <Widget>[
                                  FlatButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: Text('Ok'))
                                ],
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                              );
                            },
                            barrierDismissible: false);
                      }
                    });
                    Navigator.pop(context);
                  },
                  color: Colors.red,
                  textColor: Colors.white,
                  child: Text('Aceptar'))
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          );
        },
        barrierDismissible: false);
  }

  void getAllHU() {
    proyectBloc.getProyect();
    proyectBloc.proyect.listen((List<Proyect> proyecto) {
      this.proyecto = proyecto;
    });

    proyectBloc2.getProyectCorreo();
    proyectBloc2.proyects.listen((List<Proyect> proyectListnew) {
      this.proyectList = proyectListnew;
    });
  }

  bool validar() {
    if (funsionalidad != '' && razon != '' && criterios != '') {
      return true;
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Error"),
              content: Text("Llene todos los campos"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Ok'))
              ],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
            );
          },
          barrierDismissible: false);
      return false;
    }
  }

  Widget cargaText(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    if (snapData.length != 0) {
      return TextFormField(
        decoration: InputDecoration(border: InputBorder.none),
        enabled: false,
        initialValue: snapData[0].peso + ' puntos',
        onChanged: (text) {
          razon = text;
        },
      );
    } else {
      return TextFormField(
        decoration: InputDecoration(border: InputBorder.none),
        enabled: false,
        initialValue: '0',
        onChanged: (text) {
          razon = text;
        },
      );
    }
  }

  Widget cargaDuracion(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    if (snapData.length != 0) {
      return TextFormField(
        decoration: InputDecoration(border: InputBorder.none),
        enabled: false,
        initialValue: snapData[0].duracion + ' Semanas',
        onChanged: (text) {
          razon = text;
        },
      );
    } else {
      return TextFormField(
        decoration: InputDecoration(border: InputBorder.none),
        enabled: false,
        initialValue: '0 Semanas',
        onChanged: (text) {
          razon = text;
        },
      );
    }
  }

  Widget cargaCompromiso(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    if (snapData.length != 0) {
      return TextFormField(
        decoration: InputDecoration(border: InputBorder.none),
        enabled: false,
        initialValue: snapData[0].compromiso + ' Sprint(s)',
        onChanged: (text) {
          razon = text;
        },
      );
    } else {
      return TextFormField(
        decoration: InputDecoration(border: InputBorder.none),
        enabled: false,
        initialValue: '0  Sprint(s)',
        onChanged: (text) {
          razon = text;
        },
      );
    }
  }

  Widget cargaHUMin(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    var lista = snapData.map((userHistory) {
      var id = userHistory.historiaUsuarioId.toString();
      return id;
    }).toList();

    var initialValue = '';

    initialValue = lista[0].toString();
    return DropdownButton<String>(
      value: initialValue,
      items: lista.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text('HU-' + value),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          initialValue = newValue!;
        });
      },
      hint: Text("Select item"),
      disabledHint: Text("Disabled"),
      elevation: 8,
      style: TextStyle(color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
      icon: Icon(Icons.arrow_drop_down_circle),
      iconDisabledColor: Colors.red,
      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
      isExpanded: true,
    );
  }

  Widget cargaHUMax(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    var lista = snapData.map((userHistory) {
      var id = userHistory.historiaUsuarioId.toString();
      return id;
    }).toList();

    var initialValue = '';

    initialValue = lista[1].toString();
    return DropdownButton<String>(
      value: initialValue,
      items: lista.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text('HU-' + value),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          initialValue = newValue!;
        });
      },
      hint: Text("Select item"),
      disabledHint: Text("Disabled"),
      elevation: 8,
      style: TextStyle(color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
      icon: Icon(Icons.arrow_drop_down_circle),
      iconDisabledColor: Colors.red,
      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
      isExpanded: true,
    );
  }

  Widget cargaPivote(List<UserHistory> snapData,
      HistoryUserBloc historyUserBloc, BuildContext context) {
    var lista = snapData.map((userHistory) {
      var id = userHistory.historiaUsuarioId.toString();
      return id;
    }).toList();

    var initialValue = '';

    initialValue = lista[3].toString();
    return DropdownButton<String>(
      value: initialValue,
      items: lista.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text('HU-' + value),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          initialValue = newValue!;
        });
      },
      hint: Text("Select item"),
      disabledHint: Text("Disabled"),
      elevation: 8,
      style: TextStyle(color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
      icon: Icon(Icons.arrow_drop_down_circle),
      iconDisabledColor: Colors.red,
      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
      isExpanded: true,
    );
  }

  void getAllProyect() {
    proyectBloc.getProyect();
    personBloc.getAllPersons();
  }

  void calcularSprints() {
    fieldTextCantidadSprints.text =
        (pesoProyecto / compromisoSprint).toStringAsFixed(0);
  }

  Widget nombreProyecto(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    if (snapData.length != 0) {
      return TextFormField(
        decoration: InputDecoration(border: InputBorder.none),
        enabled: false,
        initialValue: snapData[0].nombre,
        onChanged: (text) {
          razon = text;
        },
      );
    } else {
      return TextFormField(
        decoration: InputDecoration(border: InputBorder.none),
        enabled: false,
        initialValue: '',
        onChanged: (text) {
          razon = text;
        },
      );
    }
  }

  Widget cargaIntegrantes(
      List<Person> snapData, PersonBloc personBloc, BuildContext context) {
    return TextFormField(
        decoration: InputDecoration(border: InputBorder.none),
        initialValue: snapData.length.toString() + ' Integrantes',
        onChanged: (text) {
          compromisoSprint = int.parse(text);
        });
  }

  Widget nombreProyecto2(
      List<Proyect> snapData, ProyectBloc2 proyectBloc2, BuildContext context) {
    var lista = snapData.map((proyecto) {
      var ne = {};
      ne['name'] = proyecto.nombre;
      ne['valor'] = proyecto.id_poryect;
      var id = proyecto.nombre.toString();
      return ne;
    }).toList();

    const List<String> list = <String>['One', 'Two', 'Three', 'Four'];
    String dropdownValue = list.first;

    return DropdownButton<Object>(
      value: null,
      icon: const Icon(Icons.arrow_downward),
      elevation: 16,
      hint: Text(Seleccione),
      style: const TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (Object? newValue) {
        // This is called when the user selects an item.
        setState(() {
          var id = newValue.toString().split("-")[1];
          document.cookie = "id=$id; max-age=3600;";
          Seleccione = newValue.toString().split("-")[0];
          getAllHU();
          getAllProyect();
        });
      },
      items: lista.map<DropdownMenuItem<Object>>((value) {
        return DropdownMenuItem<Object>(
          value: value['name'].toString() + '-' + value['valor'].toString(),
          child: Text(value['name']),
        );
      }).toList(),
    );
  }

  Widget costoProyecto(
      List<Person> snapData, PersonBloc personBloc, BuildContext context) {
    var lista = snapData.map((person) {
      int.parse(person.valor_hora);
      return person.valor_hora;
    });

    var valor_hora = lista, suma = 0;
    valor_hora.forEach((numero) {
      suma += int.parse(numero);
    });

    return Container(
      child: StreamBuilder(
        stream: proyectBloc.proyect,
        builder: (BuildContext contex, AsyncSnapshot<dynamic> snapData) {
          if (!snapData.hasData) {
            getAllProyect();
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return calCostoProyecto(snapData.data, proyectBloc, contex, suma);
        },
      ),
    );
  }

  Widget calCostoProyecto(List<Proyect> snapData, ProyectBloc proyectBloc,
      BuildContext context, valor_equipo) {
    // print(numberFormat(valor_equipo * int.parse(snapData[0].duracion) * 40));
    return TextFormField(
      decoration: InputDecoration(border: InputBorder.none),
      initialValue: 'COP ' +
          numberFormat(
              ((valor_equipo * 0.4) + (valor_equipo * 0.19) + valor_equipo) *
                  (int.parse(snapData[0].duracion) * 0.25)),
    );
  }

  Widget guardar(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    return FlatButton(
      padding: EdgeInsets.symmetric(
        vertical: 22,
        horizontal: 48,
      ),
      onPressed: () {
        proyectBloc
            .putProyect(
                snapData[0].duracion,
                '0',
                snapData[0].historiaMax,
                snapData[0].historiaMin,
                nombre,
                snapData[0].peso,
                snapData[0].pivote,
                snapData[0].tiempo,
                snapData[0].compromiso,
                snapData[0].fecha_inicio,
                snapData[0].cant_sprint,
                snapData[0].proyectoId,
                int.parse(cookieManager.getCookie('id')))
            .then((ApiResponse apiResponse) {
          if (apiResponse.statusResponse == 200) {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text("Éxito"),
                    content: Text("Se guardaron los datos con éxito"),
                    actions: <Widget>[
                      FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text('Ok'))
                    ],
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                  );
                },
                barrierDismissible: false);
          } else {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text("Error"),
                    content: Text("No se pudo guardar el dato con éxito"),
                    actions: <Widget>[
                      FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text('Ok'))
                    ],
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                  );
                },
                barrierDismissible: false);
          }
        });
      },
      child: Text(
        'Guardar',
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).cursorColor,
      shape: StadiumBorder(),
    );
  }

  void guardarNombre() {}

  String numberFormat(x) {
    List<String> parts = x.toString().split('.');
    RegExp re = RegExp(r'\B(?=(\d{3})+(?!\d))');

    parts[0] = parts[0].replaceAll(re, '.');
    if (parts.length == 1) {
    } else {
      parts[1] = parts[1].padRight(2, '0').substring(0, 2);
    }
    return parts.join(',');
  }

  void cambiarProyecto(proyect) {}

  void crearProyecto() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Ingresa el nombre de su nuevo proyecto"),
            content: TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'nombre',
              ),
              controller: fieldTextNombre,
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Cancelar')),
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Crear'))
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          );
        },
        barrierDismissible: false);
  }

  void nuevoProyecto() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Ingresa el nombre de su nuevo proyecto"),
            content: TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'nombre',
              ),
              controller: fieldTextNombre,
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Cancelar')),
              FlatButton(
                  onPressed: () {
                    crearProyecto2();
                    Navigator.pop(context);
                    getAllProyect();
                    getAllHU();
                  },
                  child: Text('Continuar'))
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          );
        },
        barrierDismissible: false);
  }

  void crearProyecto2() {
    var newId = random.nextInt(90000);
    proyectBloc.postProyectNew(newId, fieldTextNombre.text);
  }
}
