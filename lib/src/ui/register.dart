import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter_application_spep/src/ui/login.dart';
import 'package:flutter_application_spep/src/bloc/register_bloc.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class UserData {
  String username = '';
  String password = '';
  String password2 = '';
  String email = '';
}

class _RegisterPageState extends State<RegisterPage> {
  UserData user = UserData();
  final RegisterBloc registerBloc = RegisterBloc();
  final scaffolKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> globalFormkey = new GlobalKey<FormState>();
  bool hidePassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 224, 229, 255),
      key: scaffolKey,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Center(
              child: Container(
                width: 1200,
                padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                margin: EdgeInsets.symmetric(vertical: 85, horizontal: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Theme.of(context).primaryColor,
                  boxShadow: [
                    BoxShadow(
                        color: Theme.of(context).hintColor.withOpacity(0.2),
                        offset: Offset(0, 10),
                        blurRadius: 20)
                  ],
                ),
                child: Form(
                  key: globalFormkey,
                  child: Column(children: <Widget>[
                    SizedBox(
                      height: 25,
                    ),
                    Text(
                      "Registrate",
                      style: Theme.of(context).textTheme.headline2,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    new TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      onChanged: (text) {
                        user.email = text;
                      },
                      decoration: new InputDecoration(
                        hintText: "Correo",
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color:
                                Theme.of(context).accentColor.withOpacity(0.2),
                          ),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                        prefix: Icon(
                          Icons.email,
                          color: Theme.of(context).accentColor,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    new TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      onChanged: (text) {
                        user.username = text;
                      },
                      decoration: new InputDecoration(
                        hintText: "nombre de usuario",
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color:
                                Theme.of(context).accentColor.withOpacity(0.2),
                          ),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                        prefix: Icon(
                          Icons.child_care,
                          color: Theme.of(context).accentColor,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    new TextFormField(
                      keyboardType: TextInputType.text,
                      onChanged: (text) {
                        user.password = text;
                      },
                      obscureText: hidePassword,
                      decoration: new InputDecoration(
                        hintText: "Contraseña",
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color:
                                Theme.of(context).accentColor.withOpacity(0.2),
                          ),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                        prefix: Icon(
                          Icons.security,
                          color: Theme.of(context).accentColor,
                        ),
                        suffixIcon: IconButton(
                          onPressed: () {
                            setState(() {
                              hidePassword = !hidePassword;
                            });
                          },
                          color: Theme.of(context).accentColor.withOpacity(0.4),
                          icon: Icon(hidePassword
                              ? Icons.visibility_off
                              : Icons.visibility),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    new TextFormField(
                      keyboardType: TextInputType.text,
                      onChanged: (text) {
                        user.password2 = text;
                      },
                      obscureText: hidePassword,
                      decoration: new InputDecoration(
                        hintText: "Confirmar Contraseña",
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color:
                                Theme.of(context).accentColor.withOpacity(0.2),
                          ),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                        prefix: Icon(
                          Icons.security,
                          color: Theme.of(context).accentColor,
                        ),
                        suffixIcon: IconButton(
                          onPressed: () {
                            setState(() {
                              hidePassword = !hidePassword;
                            });
                          },
                          color: Theme.of(context).accentColor.withOpacity(0.4),
                          icon: Icon(hidePassword
                              ? Icons.visibility_off
                              : Icons.visibility),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    FlatButton(
                      padding: EdgeInsets.symmetric(
                        vertical: 12,
                        horizontal: 88,
                      ),
                      onPressed: () {
                        register();
                      },
                      child: Text(
                        'Registrate',
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Theme.of(context).cursorColor,
                      shape: StadiumBorder(),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    FlatButton(
                      padding: EdgeInsets.symmetric(
                        vertical: 12,
                        horizontal: 88,
                      ),
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => LoginPage(),
                        ));
                      },
                      child: Text(
                        'Volver al Login',
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Theme.of(context).accentColor,
                      shape: StadiumBorder(),
                    ),
                  ]),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void register() {
    if (validar()) {
      if (user.password == user.password2) {
        registerBloc
            .validateRegister(user.username, user.password, user.email)
            .then((bool access) {
          if (access) {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text("Éxito"),
                    content:
                        Text("Logueate con tu nombre de usuario y contraseña"),
                    actions: <Widget>[
                      FlatButton(
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => LoginPage(),
                            ));
                          },
                          child: Text('Ok'))
                    ],
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                  );
                },
                barrierDismissible: false);
          } else {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text("Error"),
                    content: Text("Hubo un error al registrarte"),
                    actions: <Widget>[
                      FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text('Ok'))
                    ],
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                  );
                },
                barrierDismissible: false);
          }
        });
      } else {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("Error"),
                content: Text("Las contraseñas no conciden"),
                actions: <Widget>[
                  FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('Ok'))
                ],
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
              );
            },
            barrierDismissible: false);
      }
    }
  }

  bool validar() {
    if (validateEmail(user.email) &&
        user.password != '' &&
        user.username != '') {
      return true;
    } else {
      return false;
    }
  }

  bool validateEmail(String? value) {
    String pattern =
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?)*$";
    RegExp regex = RegExp(pattern);
    if (value == null || value.isEmpty || !regex.hasMatch(value)) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Error"),
              content: Text("Ingrese correo valido"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Ok'))
              ],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
            );
          },
          barrierDismissible: false);
      return false;
    } else {
      return true;
    }
  }
}
