import 'package:flutter/material.dart';
import 'package:flutter_application_spep/src/models/proyect_model.dart';
import 'package:flutter_application_spep/src/ui/uiDrawer.dart';
import 'package:flutter_application_spep/src/ui/tableHU.dart';
import 'package:flutter_application_spep/src/bloc/historyUser_bloc.dart';
import 'package:flutter_application_spep/src/bloc/proyect_bloc.dart';
import 'package:flutter_application_spep/src/models/userHistory_model.dart';

class EditSprint extends StatefulWidget {
  @override
  _EditSprintState createState() => _EditSprintState();
}

class UserData {
  String username = '';
  String password = '';
}

class _EditSprintState extends State<EditSprint> {
  final scaffolKey = GlobalKey<ScaffoldState>();

  GlobalKey<FormState> globalFormkey = new GlobalKey<FormState>();
  bool hidePassword = true;

  UiDrawer uiDrawer = UiDrawer();
  UserData user = UserData();
  TableHU tableHU = TableHU();
  final HistoryUserBloc historyUserBloc = HistoryUserBloc();
  final ProyectBloc proyectBloc = ProyectBloc();
  final fieldTextRequiero = TextEditingController();
  final fieldTextFinalidad = TextEditingController();
  final fieldTextCriterios = TextEditingController();
  final fieldTextCantidadSprints = TextEditingController();
  final fieldTextCompromiso = TextEditingController();
  List<String> itemRol = ['1', '2', '3', '5', '8', '13', '21', '?'];
  String rol = '1';
  String funsionalidad = '';
  String razon = '';
  String criterios = '';
  List<String> itemComplejidad = ['1', '2', '3', '5', '8', '13', '21', '?'];
  String complejidad = '1';
  List<String> itemPrioridad = ['S', 'M', 'C', 'W'];
  String prioridad = 'S';
  List<String> itemDuration = ['1', '2', '3', '4'];
  String duration = '1';
  List<UserHistory> userHistory = [];
  List<Proyect> proyect = [];
  int pesoProyecto = 0;
  int compromisoSprint = 0;
  String cantidadSprints = '';
  String duracion = '';
  String equipoTrabajo = '';
  String historiaMax = '';
  String historiaMin = '';
  String nombre = '';
  String peso = '';
  String pivote = '';
  String tiempo = '1';
  int cant_sprint = 0;
  String compromiso = '1';

  initialUserHistory() {
    historyUserBloc.getAllHU();
  }

  @override
  void initState() {
    getAllHU();
    pintar();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 224, 229, 255),
      key: scaffolKey,
      appBar: AppBar(
        title: Text("Editar Sprints"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.update),
            onPressed: () {
              getAllHU();
              pintar();
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              Container(
                width: 700,
                alignment: Alignment.topLeft,
                padding: EdgeInsets.symmetric(vertical: 40, horizontal: 20),
                margin: EdgeInsets.symmetric(vertical: 35, horizontal: 80),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Theme.of(context).hintColor.withOpacity(0.2),
                        offset: Offset(0, 10),
                        blurRadius: 20)
                  ],
                ),
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(bottom: 25),
                      child: Row(
                        children: [
                          Text('Historia de usuarios',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 25,
                              )),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        StreamBuilder(
                          stream: historyUserBloc.userHistory,
                          builder: (BuildContext contex,
                              AsyncSnapshot<dynamic> snapData) {
                            if (!snapData.hasData) {
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            }
                            return cargaLista(
                                snapData.data, historyUserBloc, contex);
                          },
                        ),
                      ],
                    ),
                    StreamBuilder(
                      stream: proyectBloc.proyect,
                      builder: (BuildContext contex,
                          AsyncSnapshot<dynamic> snapData) {
                        if (!snapData.hasData) {
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                        return cargaData(snapData.data, proyectBloc, contex);
                      },
                    ),
                  ],
                ),
              ),
              Container(
                width: 600,
                padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                margin: EdgeInsets.symmetric(vertical: 45, horizontal: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Color.fromARGB(255, 255, 255, 255),
                  boxShadow: [
                    BoxShadow(
                        color: Theme.of(context).hintColor.withOpacity(0.2),
                        offset: Offset(0, 10),
                        blurRadius: 20)
                  ],
                ),
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(bottom: 25),
                      child: Row(
                        children: [
                          Text('Peso del Sprint',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 25,
                              )),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Column(
                          children: [
                            StreamBuilder(
                              stream: historyUserBloc.userHistory,
                              builder: (BuildContext contex,
                                  AsyncSnapshot<dynamic> snapData) {
                                if (!snapData.hasData) {
                                  return Center(
                                    child: CircularProgressIndicator(),
                                  );
                                }
                                return cargaNumberSprint(
                                    snapData.data, historyUserBloc, contex);
                              },
                            ),
                          ],
                        ),
                        Container(
                          width: 300,
                          padding: const EdgeInsets.all(20),
                          margin: const EdgeInsets.only(left: 60),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    bottom: 5.0, left: 45.0),
                                child: Text(
                                  'PESO DEL PROYECTO',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20),
                                ),
                              ),
                              StreamBuilder(
                                stream: historyUserBloc.userHistory,
                                builder: (BuildContext contex,
                                    AsyncSnapshot<dynamic> snapData) {
                                  if (!snapData.hasData) {
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                  return cargaText(
                                      snapData.data, historyUserBloc, contex);
                                },
                              ),
                              Padding(
                                  padding: const EdgeInsets.only(
                                      bottom: 5.0, left: 45.0),
                                  child: Text(
                                    'COMPROMISO POR SPRINT',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                  )),
                              StreamBuilder(
                                stream: proyectBloc.proyect,
                                builder: (BuildContext contex,
                                    AsyncSnapshot<dynamic> snapData) {
                                  if (!snapData.hasData) {
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                  return cargaCompromiso(
                                      snapData.data, proyectBloc, contex);
                                },
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ])
          ],
        ),
      ),
      drawer: uiDrawer,
    );
  }

  Widget cargaLista(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    List<String> listaSprint = [];
    var contador = 0;

    for (var i = 0; i < int.parse(snapData[snapData.length - 1].sprint); i++) {
      contador++;
      listaSprint.add(contador.toString());
    }

    return DataTable(
      columns: [
        DataColumn(
          label: Text('ID Historia de usuario'),
        ),
        DataColumn(
          label: Text('Complejidad'),
        ),
        DataColumn(
          label: Text('Prioridad'),
        ),
        DataColumn(
          label: Text('Sprint'),
        ),
      ],
      rows: this
          .userHistory
          .map(
            (userHistory) => DataRow(
              cells: [
                DataCell(
                  Text('HU-' + userHistory.historiaUsuarioId.toString()),
                ),
                DataCell(
                  Text(userHistory.complejidad),
                  // Text(userHistory.complejidad),
                ),
                DataCell(
                  Text(userHistory.prioridad),
                ),
                DataCell(
                  DropdownButton<String>(
                    value: userHistory.sprint,
                    items: listaSprint
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        userHistory.sprint = newValue!;
                        guardarHU(userHistory);
                      });
                    },
                    hint: Text("Select item"),
                    disabledHint: Text("Disabled"),
                    elevation: 8,
                    style: TextStyle(
                        color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
                    icon: Icon(Icons.arrow_drop_down_circle),
                    iconDisabledColor: Colors.red,
                    iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
                    isExpanded: true,
                  ),
                )
              ],
            ),
          )
          .toList(),
    );
  }

  bool validar() {
    if (funsionalidad != '' && razon != '' && criterios != '') {
      return true;
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Error"),
              content: Text("Llene todos los campos"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Ok'))
              ],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
            );
          },
          barrierDismissible: false);
      return false;
    }
  }

  Widget cargaText(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    var lista = snapData.map((userHistory) {
      int.parse(userHistory.complejidad);
      return userHistory.complejidad;
    });

    var numeros = lista, suma = 0;
    numeros.forEach((numero) {
      suma += int.parse(numero);
    });

    peso = suma.toString();
    return Padding(
        padding: const EdgeInsets.only(bottom: 25.0, left: 45.0),
        child: TextFormField(
          decoration: InputDecoration(border: InputBorder.none),
          enabled: false,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          initialValue: peso,
          onChanged: (text) {
            razon = text;
          },
        ));
  }

  Widget cargaHUMin(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    var lista = snapData.map((userHistory) {
      var id = userHistory.historiaUsuarioId.toString();
      return id;
    }).toList();

    //humin = lista[0].toString();
    return DropdownButton<String>(
      items: lista.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text('HU-' + value),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          historiaMin = newValue!;
          guardarProyect();
          getAllHU();
          pintar();
        });
      },
      hint: Text('HU-' + historiaMin),
      disabledHint: Text("Disabled"),
      elevation: 8,
      style: TextStyle(color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
      icon: Icon(Icons.arrow_drop_down_circle),
      iconDisabledColor: Colors.red,
      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
      isExpanded: true,
    );
  }

  Widget cargaHUMax(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context) {
    var lista = snapData.map((userHistory) {
      var id = userHistory.historiaUsuarioId.toString();
      return id;
    }).toList();

    var initialValue = '';

    initialValue = lista[1].toString();
    return DropdownButton<String>(
      items: lista.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text('HU-' + value),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          historiaMax = newValue!;
          guardarProyect();
          getAllHU();
          pintar();
        });
      },
      hint: Text('HU-' + historiaMax),
      disabledHint: Text("Disabled"),
      elevation: 8,
      style: TextStyle(color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
      icon: Icon(Icons.arrow_drop_down_circle),
      iconDisabledColor: Colors.red,
      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
      isExpanded: true,
    );
  }

  Widget cargaPivote(List<UserHistory> snapData,
      HistoryUserBloc historyUserBloc, BuildContext context) {
    var lista = snapData.map((userHistory) {
      var id = userHistory.historiaUsuarioId.toString();
      return id;
    }).toList();

    var initialValue = '';

    initialValue = lista[3].toString();
    return DropdownButton<String>(
      items: lista.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text('HU-' + value),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          pivote = newValue!;
          guardarProyect();
          getAllHU();
          pintar();
        });
      },
      hint: Text('HU-' + pivote),
      disabledHint: Text("Disabled"),
      elevation: 8,
      style: TextStyle(color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
      icon: Icon(Icons.arrow_drop_down_circle),
      iconDisabledColor: Colors.red,
      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
      isExpanded: true,
    );
  }

  Widget cargaSprint(List<UserHistory> snapData,
      HistoryUserBloc historyUserBloc, BuildContext context) {
    //  print('final');
    return FlatButton(
      padding: EdgeInsets.symmetric(
        vertical: 22,
        horizontal: 48,
      ),
      onPressed: () {
        calcularSprints(snapData);
      },
      child: Text(
        'Calular',
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).cursorColor,
      shape: StadiumBorder(),
    );
  }

  Widget cargaCompromiso(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    //  print('final');
    return Padding(
        padding: const EdgeInsets.only(bottom: 5.0, left: 45.0),
        child: TextFormField(
            enabled: false,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            initialValue: snapData[0].compromiso,
            decoration: InputDecoration(border: InputBorder.none),
            onChanged: (text) {
              if (text.length > 0) {
                compromiso = text;
                guardarProyect();
              } else {
                compromiso = '0';
              }
            }));
  }

  Widget cargaNumSprint(List<UserHistory> snapData,
      HistoryUserBloc historyUserBloc, BuildContext context) {
    calcularSprintss(snapData);
    //  print('final');
    return TextFormField(
        controller: fieldTextCantidadSprints,
        decoration: InputDecoration(border: InputBorder.none),
        enabled: false,
        onChanged: (text) {
          cantidadSprints = text;
        });
  }

  Widget cargaTiempo(List<UserHistory> snapData,
      HistoryUserBloc historyUserBloc, BuildContext context) {
    //  print('final');
    return DropdownButton<String>(
      items: itemDuration.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value + ' Semana(s)'),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          tiempo = newValue!;
          guardarProyect();
          getAllHU();
          pintar();
        });
      },
      hint: Text(tiempo + ' Semana(s)'),
      disabledHint: Text("Disabled"),
      elevation: 8,
      style: TextStyle(color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
      icon: Icon(Icons.arrow_drop_down_circle),
      iconDisabledColor: Colors.red,
      iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
      isExpanded: true,
    );
  }

  void getAllHU() {
    historyUserBloc.getAllHU();
    proyectBloc.getProyect();
  }

  void pintar() {
    historyUserBloc.getAllHU();
    historyUserBloc.userHistory.listen((List<UserHistory> userHistory) {
      this.userHistory = userHistory;
    });
  }

  void calcularSprints(snapData) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Alerta"),
            content: Text(
                "Si calcula, la panatalla sprint se actualizará con base en el sistema"),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Cancelar')),
              FlatButton(
                  onPressed: () {
                    alertaSprintok(snapData);
                    Navigator.pop(context);
                  },
                  child: Text('Ok'))
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          );
        },
        barrierDismissible: false);
  }

  void calcularSprintss(snapData) {
    var numbSprint = 1;
    var contador = 0;

    snapData.forEach((numero) {
      contador += int.parse(numero.complejidad);

      if (contador <= int.parse(compromiso)) {
        // print('sprint ' +            numbSprint.toString() +        '  Hu_id ' +            numero.historiaUsuarioId.toString());
      } else {
        numbSprint += 1;
        contador = int.parse(numero.complejidad);
        // print('sprint ' +            numbSprint.toString() +            '  Hu_id ' +            numero.historiaUsuarioId.toString());
      }
    });
    duracion = (int.parse(tiempo) * numbSprint).toString();

    fieldTextCantidadSprints.text = numbSprint.toString();
  }

  void guardarProyect() {
    fieldTextCompromiso.text = compromiso;
    proyectBloc
        .putProyect(
            duracion,
            equipoTrabajo,
            historiaMax,
            historiaMin,
            nombre,
            peso,
            pivote,
            tiempo,
            compromiso,
            '${DateTime.now()}',
            cant_sprint,
            proyect[0].proyectoId,
            proyect[0].id_poryect)
        .then((value) => {getAllHU(), pintar()});
  }

  Widget cargaData(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    proyect = snapData;

    duracion = proyect[0].duracion;
    historiaMax = proyect[0].historiaMax;
    historiaMin = proyect[0].historiaMin;
    peso = proyect[0].peso;
    pivote = proyect[0].pivote;
    tiempo = proyect[0].tiempo;
    compromiso = proyect[0].compromiso;

    return Container();
  }

  Widget AlertCalcular(
      List<Proyect> snapData, ProyectBloc proyectBloc, BuildContext context) {
    proyect = snapData;

    duracion = proyect[0].duracion;
    historiaMax = proyect[0].historiaMax;
    historiaMin = proyect[0].historiaMin;
    peso = proyect[0].peso;
    pivote = proyect[0].pivote;
    tiempo = proyect[0].tiempo;
    compromiso = proyect[0].compromiso;
    cant_sprint = proyect[0].cant_sprint;
    return Container();
  }

  void alertaSprintok(snapData) {
    var numbSprint = 1;
    var contador = 0;
    snapData.forEach((numero) {
      contador += int.parse(numero.complejidad);

      if (contador <= int.parse(compromiso)) {
        historyUserBloc.updateUserHistory(
          numero.rol,
          numero.funsionalidad,
          numero.razon,
          numero.criterioAceptacion,
          numero.complejidad,
          numero.prioridad,
          numbSprint.toString(),
          numero.encargado,
          numero.estado,
          numero.historiaUsuarioId,
        );
      } else {
        numbSprint += 1;
        contador = int.parse(numero.complejidad);
        historyUserBloc.updateUserHistory(
          numero.rol,
          numero.funsionalidad,
          numero.razon,
          numero.criterioAceptacion,
          numero.complejidad,
          numero.prioridad,
          numbSprint.toString(),
          numero.encargado,
          numero.estado,
          numero.historiaUsuarioId,
        );
      }
    });
    duracion = (int.parse(tiempo) * numbSprint).toString();

    fieldTextCantidadSprints.text = numbSprint.toString();

    guardarProyect();
  }

  void guardarHU(hu) {
    historyUserBloc
        .updateUserHistory(
          hu.rol,
          hu.funsionalidad,
          hu.razon,
          hu.criterioAceptacion,
          hu.complejidad,
          hu.prioridad,
          hu.sprint,
          hu.encargado,
          hu.estado,
          hu.historiaUsuarioId,
        )
        .then((value) => {getAllHU(), pintar()});
  }

  Widget cargaNumberSprint(List<UserHistory> snapData,
      HistoryUserBloc historyUserBloc, BuildContext context) {
    var listaSprint = [];
    var contador = 0;

    for (var i = 0; i < int.parse(snapData[snapData.length - 1].sprint); i++) {
      contador++;
      listaSprint.add(contador);
    }
    return Center(
        child: Column(
            children: listaSprint
                .map((item) => Container(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding:
                              const EdgeInsets.only(bottom: 5.0, left: 15.0),
                          child: Text(
                            'Sprint ' + item.toString(),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          ),
                        ),
                        StreamBuilder(
                          stream: historyUserBloc.userHistory,
                          builder: (BuildContext contex,
                              AsyncSnapshot<dynamic> snapData) {
                            if (!snapData.hasData) {
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            }
                            return cargaPeso(snapData.data, historyUserBloc,
                                contex, item.toString());
                          },
                        ),
                      ],
                    )))
                .toList()));
  }

  Widget cargaPeso(List<UserHistory> snapData, HistoryUserBloc historyUserBloc,
      BuildContext context, id) {
    var peso = 0;
    for (var i = 0; i < snapData.length; i++) {
      if (snapData[i].sprint == id) {
        peso = peso + int.parse(snapData[i].complejidad);
      }
    }

    return Padding(
        padding: const EdgeInsets.only(bottom: 25.0, left: 45.0),
        child: Text(
          'Peso: ' + peso.toString(),
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18,
            fontFamily: "Sans",
            color: Colors.black54,
            letterSpacing: 0.5,
          ),
        ));
  }
}
