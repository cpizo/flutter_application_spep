import 'package:flutter/material.dart';
import 'package:flutter_application_spep/src/bloc/person_bloc.dart';
import 'package:flutter_application_spep/src/bloc/workTeam_bloc.dart';
import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/ui/uiDrawer.dart';
import 'package:flutter_application_spep/src/models/person_model.dart';
import 'package:flutter_application_spep/src/ui/workTeam.dart';

void main() => runApp(new EditWorkTeam());

// Definir un componente con estado
class EditWorkTeam extends StatefulWidget {
  var data;
  EditWorkTeam({this.data});
  static const routeName = '/extractArguments';
  @override
  _EditWorkTeamState createState() => _EditWorkTeamState();
}

// Al definir un componente con estado, debe crear una clase de estado para el componente, que hereda de la clase de estado
class _EditWorkTeamState extends State<EditWorkTeam> {
  UiDrawer uiDrawer = UiDrawer();
  final WorkTeamBloc workTeamBloc = WorkTeamBloc();
  final PersonBloc personBloc = PersonBloc();

  String text = "Click Me!";
  String nombre = "";
  String direccion = "";
  String telefono = "";
  String valor_hora = "";
  String rol = 'Desarrollador';
  List<String> itemRol = [
    'Desarrollador',
    'Desarrollador senior',
    'Testing',
    'Scrum master',
    'Arquitecto de software'
  ];
  List<Person> person = [];

  changeText() {
    if (text == "Click Me!") {
      setState(() {
        text = "Hello World!";
      });
    } else {
      setState(() {
        text = "Click Me!";
      });
    }
  }

  @override
  void initState() {
    getAllPerson();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Test",
      home: Scaffold(
        backgroundColor: Color.fromARGB(255, 224, 229, 255),
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Editar participante"),
        ),
        body: Center(
          // InkWell es un widget integrado en Flutter, que se usa para agregar eventos de clic a otros widgets, y habrá un efecto dominó al hacer clic
          child: Container(
            width: 1200,
            padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
            margin: EdgeInsets.symmetric(vertical: 85, horizontal: 20),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Theme.of(context).hintColor.withOpacity(0.2),
                    offset: Offset(0, 10),
                    blurRadius: 20)
              ],
            ),
            child: StreamBuilder(
              stream: personBloc.person,
              builder: (BuildContext contex, AsyncSnapshot<dynamic> snapData) {
                if (!snapData.hasData) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                return cargaUser(snapData.data, personBloc, contex);
              },
            ),
          ),
        ),
        drawer: uiDrawer,
      ),
    );
  }

  bool validar() {
    if (nombre != '' && telefono != '' && direccion != '') {
      return true;
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Error"),
              content: Text("Llene todos los campos"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Ok'))
              ],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
            );
          },
          barrierDismissible: false);
      return false;
    }
  }

  void agregarParticipante() {
    if (validar()) {
      personBloc
          .postPerson(nombre, telefono, direccion, rol, valor_hora)
          .then((ApiResponse apiResponse) {
        if (apiResponse.statusResponse == 200) {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text("Éxito"),
                  content: Text("Participante actualizado éxitosamente"),
                  actions: <Widget>[
                    FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text('Ok'))
                  ],
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                );
              },
              barrierDismissible: false);
        } else {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text("Error"),
                  content: Text(apiResponse.message),
                  actions: <Widget>[
                    FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text('Ok'))
                  ],
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                );
              },
              barrierDismissible: false);
        }
      });
    }
  }

  Widget cargaUser(
      List<Person> snapData, PersonBloc personBloc, BuildContext context) {
    var per;

    snapData.forEach((person) {
      if (person.id_person.toString() == widget.data) {
        per = person;
      }
    });

    return FittedBox(
        child: Column(children: [
      Row(
        children: [
          Text('Editar participante en el equipo de trabajo',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 25,
              )),
        ],
      ),
      Row(
        children: [
          Column(
            children: [
              Row(
                children: [
                  Container(
                    width: 500,
                    padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                    child: TextFormField(
                      initialValue: per.nombre,
                      onChanged: (text) {
                        per.nombre = text;
                      },
                      decoration:
                          InputDecoration(labelText: "Nombre del participante"),
                    ),
                  )
                ],
              ),
              Row(
                children: [
                  Container(
                    width: 500,
                    padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                    child: TextFormField(
                      initialValue: per.telefono,
                      onChanged: (text) {
                        per.telefono = text;
                      },
                      decoration: InputDecoration(labelText: "Telefono"),
                    ),
                  )
                ],
              ),
              Row(
                children: [
                  Container(
                    width: 500,
                    padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                    child: TextFormField(
                      initialValue: per.valor_hora,
                      onChanged: (text) {
                        per.valor_hora = text;
                      },
                      decoration: InputDecoration(labelText: "Salario"),
                    ),
                  )
                ],
              ),
            ],
          ),
          Column(
            children: [
              Row(
                children: [
                  Container(
                    width: 500,
                    margin: const EdgeInsets.only(top: 0),
                    padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                    child: TextFormField(
                      initialValue: per.direccion,
                      onChanged: (text) {
                        per.direccion = text;
                      },
                      decoration: InputDecoration(labelText: "Dirección"),
                    ),
                  )
                ],
              ),
              Container(
                margin: const EdgeInsets.only(top: 25),
                child: Text(
                  'Seleccione Rol',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                ),
              ),
              Container(
                width: 450,
                margin: const EdgeInsets.only(bottom: 45, top: 0),
                child: DropdownButton<String>(
                  items: itemRol.map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      per.rol = newValue!;
                    });
                  },
                  hint: Text(per.rol),
                  disabledHint: Text("Disabled"),
                  elevation: 8,
                  style: TextStyle(
                      color: Color.fromARGB(255, 76, 94, 175), fontSize: 16),
                  icon: Icon(Icons.arrow_drop_down_circle),
                  iconDisabledColor: Colors.red,
                  iconEnabledColor: Color.fromARGB(255, 76, 91, 175),
                  isExpanded: true,
                ),
              ),
            ],
          ),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FlatButton(
            padding: EdgeInsets.symmetric(
              vertical: 22,
              horizontal: 48,
            ),
            onPressed: () {
              editPerson(per);
            },
            child: Text(
              'Editar',
              style: TextStyle(color: Colors.white),
            ),
            color: Color.fromARGB(255, 244, 162, 54),
            shape: StadiumBorder(),
          ),
        ],
      )
    ]));
  }

  void getAllPerson() {
    personBloc.getAllPersons();
    personBloc.person.listen((List<Person> person) {
      this.person = person;
    });
  }

  void editPerson(person) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Alerta"),
            content: Text("Desear actualizar el usuario?"),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Cancelar')),
              FlatButton(
                  onPressed: () {
                    personBloc
                        .putPerson(
                            person.nombre,
                            person.telefono,
                            person.direccion,
                            person.rol,
                            person.valor_hora,
                            person.id_person)
                        .then((value) => {getAllPerson()});
                    getAllPerson();
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => WorkTeam()),
                    );
                  },
                  child: Text('Actualizar'))
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          );
        },
        barrierDismissible: false);
  }
}
