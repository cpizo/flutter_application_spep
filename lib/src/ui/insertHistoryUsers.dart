import 'package:flutter/material.dart';
import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/ui/historyUsers.dart';
import 'package:flutter_application_spep/src/ui/uiDrawer.dart';
import 'package:flutter_application_spep/src/bloc/historyUser_bloc.dart';

class InsertHistoryUsers extends StatefulWidget {
  @override
  _InsertHistoryUsersState createState() => _InsertHistoryUsersState();
}

class UserData {
  String username = '';
  String password = '';
}

class _InsertHistoryUsersState extends State<InsertHistoryUsers> {
  final scaffolKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> globalFormkey = new GlobalKey<FormState>();
  bool hidePassword = true;

  UiDrawer uiDrawer = UiDrawer();
  UserData user = UserData();
  final HistoryUserBloc historyUserBloc = HistoryUserBloc();
  final fieldTextRequiero = TextEditingController();
  final fieldTextFinalidad = TextEditingController();
  final fieldTextCriterios = TextEditingController();
  List<String> itemRol = ['cliente'];
  String rol = 'cliente';
  String funsionalidad = '';
  String razon = '';
  String criterios = '';
  List<String> itemComplejidad = ['1', '2', '3', '5', '8', '13', '21', '?'];
  String complejidad = '1';
  List<String> itemPrioridad = ['S', 'M', 'C', 'W'];
  String prioridad = 'S';
  var _TextControllerRequiero;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 224, 229, 255),
      key: scaffolKey,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => HistoryUsers(),
          )),
        ),
        centerTitle: true,
        title: Text("Product Backlog"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Center(
              child: Container(
                width: 1200,
                padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                margin: EdgeInsets.symmetric(vertical: 45, horizontal: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Color.fromARGB(255, 255, 255, 255),
                  boxShadow: [
                    BoxShadow(
                        color: Theme.of(context).hintColor.withOpacity(0.2),
                        offset: Offset(0, 10),
                        blurRadius: 20)
                  ],
                ),
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(bottom: 25),
                      child: Row(
                        children: [
                          Text('Agregar Historia de usuarios',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 25,
                              )),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Column(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Seleccione Rol',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15),
                                ),
                                Container(
                                  width: 450,
                                  margin: const EdgeInsets.only(bottom: 5),
                                  child: DropdownButton<String>(
                                    value: rol,
                                    items: itemRol
                                        .map<DropdownMenuItem<String>>(
                                            (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                    onChanged: (newValue) {
                                      setState(() {
                                        rol = newValue!;
                                      });
                                    },
                                    hint: Text("Select item"),
                                    disabledHint: Text("Disabled"),
                                    elevation: 8,
                                    style: TextStyle(
                                        color: Color.fromARGB(255, 76, 94, 175),
                                        fontSize: 16),
                                    icon: Icon(Icons.arrow_drop_down_circle),
                                    iconDisabledColor: Colors.red,
                                    iconEnabledColor:
                                        Color.fromARGB(255, 76, 91, 175),
                                    isExpanded: true,
                                  ),
                                ),
                                Container(
                                  width: 450,
                                  margin: const EdgeInsets.only(bottom: 15),
                                  child: TextField(
                                    controller: fieldTextRequiero,
                                    onChanged: (text) {
                                      funsionalidad = text;
                                    },
                                    decoration: InputDecoration(
                                        labelText:
                                            "Requiero/ necesito/ deseo/ espero/ quiero:"),
                                  ),
                                ),
                                Container(
                                  width: 450,
                                  child: TextField(
                                    controller: fieldTextFinalidad,
                                    onChanged: (text) {
                                      razon = text;
                                    },
                                    decoration: InputDecoration(
                                        labelText: "Con la finalidad de:"),
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 600,
                              padding: const EdgeInsets.all(20),
                              margin: const EdgeInsets.only(left: 60),
                              child: TextField(
                                  controller: fieldTextCriterios,
                                  onChanged: (text) {
                                    criterios = text;
                                  },
                                  keyboardType: TextInputType.multiline,
                                  maxLines: 10,
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      hintText:
                                          "Ingrese criterios de aceptación:",
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1,
                                              color: Color.fromARGB(
                                                  255, 82, 128, 255))))),
                            )
                          ],
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        FlatButton(
                          padding: EdgeInsets.symmetric(
                            vertical: 22,
                            horizontal: 48,
                          ),
                          onPressed: () {
                            agregarHU();
                          },
                          child: Text(
                            'Agregar HU',
                            style: TextStyle(color: Colors.white),
                          ),
                          color: Theme.of(context).cursorColor,
                          shape: StadiumBorder(),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      drawer: uiDrawer,
    );
  }

  bool validar() {
    if (funsionalidad != '' && razon != '' && criterios != '') {
      return true;
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Error"),
              content: Text("Llene todos los campos"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Ok'))
              ],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
            );
          },
          barrierDismissible: false);
      return false;
    }
  }

  void agregarHU() {
    if (validar()) {
      historyUserBloc
          .postHU(rol, funsionalidad, razon, criterios, complejidad, prioridad,
              '1', '')
          .then((ApiResponse apiResponse) {
        if (apiResponse.statusResponse == 200) {
          fieldTextRequiero.clear();
          fieldTextFinalidad.clear();
          fieldTextCriterios.clear();
          funsionalidad = '';
          razon = '';
          criterios = '';
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text("Éxito"),
                  content: Text("Se registro la historia de usuario"),
                  actions: <Widget>[
                    FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text('Ok'))
                  ],
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                );
              },
              barrierDismissible: false);
        } else {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text("Error"),
                  content: Text("No se pudo registrar la historia de usuario"),
                  actions: <Widget>[
                    FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text('Ok'))
                  ],
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                );
              },
              barrierDismissible: false);
        }
      });
    }
  }
}
