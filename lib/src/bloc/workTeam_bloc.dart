import 'dart:async';
import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/repository/getToken.dart';
import 'package:flutter_application_spep/src/repository/workTeam_Api_Service.dart';
import 'package:flutter_application_spep/src/models/person_model.dart';

class WorkTeamBloc {
  CookieManager cookieManager = CookieManager();
  Future<ApiResponse> postWorkTeam(
      String nombre, String telefono, String direccion, String rol) async {
    Person _personObject = Person(
        nombre: nombre,
        telefono: telefono,
        direccion: direccion,
        id_poryect: int.parse(cookieManager.getCookie('id')));
    final _repository = WorkTeamAPiService();
    var apiResponse = await _repository.insertWorkTeam(_personObject);
    return apiResponse;
  }
}
