import 'dart:async';
import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/models/person_model.dart';
import 'package:flutter_application_spep/src/repository/getToken.dart';
import 'package:flutter_application_spep/src/repository/repository/GeneralRepository.dart';

class PersonBloc {
  final _personController = StreamController<List<Person>>.broadcast();
  CookieManager cookieManager = CookieManager();

  List<Person>? _initialData;

  Stream<List<Person>> get person =>
      _personController.stream.asBroadcastStream();

  Future<ApiResponse> postPerson(String nombre, String telefono,
      String direccion, String rol, String valor_hora) async {
    Person _personObject = Person(
        nombre: nombre,
        telefono: telefono,
        direccion: direccion,
        rol: rol,
        valor_hora: valor_hora,
        id_poryect: int.parse(cookieManager.getCookie('id')));
    final _repository = GeneralRepository();
    var apiResponse = await _repository.insertPerson(_personObject);

    return apiResponse;
  }

  Future getAllPersons() async {
    final _repository = GeneralRepository();
    var apiResponse = await _repository.getAllPerson();

    if (apiResponse.statusResponse == 200) {
      _initialData = apiResponse.object as List<Person>; //as List<UserHistory>;
      _personController.add(_initialData!);
      //print(_userHistoryController.stream);
    } else {
      _initialData = [];
      _personController.add([]);
    }
    //print(apiResponse.object);
  }

  Future<ApiResponse> putPerson(String nombre, String telefono,
      String direccion, String rol, String valor_hora, int id) async {
    Person _personObject = Person(
        nombre: nombre,
        telefono: telefono,
        direccion: direccion,
        rol: rol,
        valor_hora: valor_hora,
        id_person: id,
        id_poryect: int.parse(cookieManager.getCookie('id')));
    final _repository = GeneralRepository();

    var apiResponse = await _repository.updatePerson(_personObject);

    return apiResponse;
  }

  Future<bool> deleterPerson(int id) async {
    Person _personObject = Person(id_person: id);
    final _repository = GeneralRepository();

    var apiResponse = await _repository.deletePerson(_personObject);

    //if (apiResponse.statusResponse == 200) {
    return true;
    //} else {
    //return false;
    //}
  }
}
