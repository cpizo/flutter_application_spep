import 'dart:async';

import 'package:flutter_application_spep/src/models/login_model.dart';
import 'package:flutter_application_spep/src/repository/repository/GeneralRepository.dart';

class LoginBloc {
  Future<bool> validateLogin(String username, String password) async {
    Login _loginObject = Login(username: username, password: password);
    final _repository = GeneralRepository();
    var apiResponse = await _repository.signin(_loginObject);
    if (apiResponse.statusResponse == 200) {
      return true;
    } else {
      return false;
    }
  }
}
