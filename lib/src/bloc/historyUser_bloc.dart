import 'dart:async';

import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/models/userHistory_model.dart';
import 'package:flutter_application_spep/src/repository/getToken.dart';
import 'package:flutter_application_spep/src/repository/repository/GeneralRepository.dart';

class HistoryUserBloc {
  final _userHistoryController =
      StreamController<List<UserHistory>>.broadcast();
  List<UserHistory>? _initialData;
  Stream<List<UserHistory>> get userHistory =>
      _userHistoryController.stream.asBroadcastStream();
  CookieManager cookieManager = CookieManager();

  Future<ApiResponse> postHU(
      String rol,
      String funsionalidad,
      String razon,
      String criterios,
      String complejidad,
      String prioridad,
      String sprint,
      String encargado) async {
    UserHistory _userHistoryObject = UserHistory(
        rol: rol,
        funsionalidad: funsionalidad,
        razon: razon,
        criterioAceptacion: criterios,
        complejidad: complejidad,
        prioridad: prioridad,
        sprint: sprint,
        encargado: encargado,
        id_poryect: int.parse(cookieManager.getCookie('id')));
    final _repository = GeneralRepository();
    var apiResponse = await _repository.insertUserHistory(_userHistoryObject);

    return apiResponse;
  }

  Future<ApiResponse> updateUserHistory(
      String rol,
      String funsionalidad,
      String razon,
      String criterios,
      String complejidad,
      String prioridad,
      String sprint,
      String encargado,
      String estado,
      int id) async {
    UserHistory _userHistoryObject = UserHistory(
        rol: rol,
        funsionalidad: funsionalidad,
        razon: razon,
        criterioAceptacion: criterios,
        complejidad: complejidad,
        prioridad: prioridad,
        sprint: sprint,
        encargado: encargado,
        estado: estado,
        historiaUsuarioId: id,
        id_poryect: int.parse(cookieManager.getCookie('id')));
    final _repository = GeneralRepository();

    var apiResponse = await _repository.updateUserHistory(_userHistoryObject);

    return apiResponse;
  }

  Future getAllHU() async {
    final _repository = GeneralRepository();
    var apiResponse = await _repository.getAllUserHistory();
    if (apiResponse.statusResponse == 200) {
      _initialData = apiResponse.object as List<UserHistory>;

      _userHistoryController.add(_initialData!);
    } else {
      _initialData = [];
      _userHistoryController.add([]);
    }
  }

  Future<bool> deleterHistory(int id) async {
    UserHistory _userHistoryObject = UserHistory(historiaUsuarioId: id);
    final _repository = GeneralRepository();

    var apiResponse = await _repository.deleteUserHistory(_userHistoryObject);

    if (apiResponse.statusResponse == 200) {
      return true;
    } else {
      return false;
    }
  }
}
