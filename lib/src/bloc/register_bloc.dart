import 'dart:async';
import 'package:flutter_application_spep/src/models/register_model.dart';
import 'package:flutter_application_spep/src/repository/register_Api_Service.dart';

class RegisterBloc {
  Future<bool> validateRegister(
      String username, String password, String email) async {
    Register _registerObject =
        Register(username: username, password: password, email: email);
    final _repository = RegisterAPiService(_registerObject);
    var apiResponse = await _repository.signup();
    if (apiResponse.statusResponse == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> updatePassword(String email, String password) async {
    Register _registerObject =
        Register(username: 'username', password: password, email: email);
    final _repository = RegisterAPiService(_registerObject);
    var apiResponse = await _repository.updatePassword();
    if (apiResponse.statusResponse == 200) {
      return true;
    } else {
      return false;
    }
  }
}
