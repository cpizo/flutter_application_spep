import 'dart:async';
import 'package:flutter_application_spep/src/models/proyect_model.dart';
import 'package:flutter_application_spep/src/models/utils/apiresponse_model.dart';
import 'package:flutter_application_spep/src/repository/getToken.dart';
import 'package:flutter_application_spep/src/repository/repository/GeneralRepository.dart';

class ProyectBloc2 {
  CookieManager cookieManager = CookieManager();
  final _proyectController = StreamController<List<Proyect>>.broadcast();
  List<Proyect>? _initialData;
  Stream<List<Proyect>> get proyects =>
      _proyectController.stream.asBroadcastStream();

  Future<ApiResponse> postProyect() async {
    Proyect _proyectObject = Proyect(
        duracion: '1',
        historiaMax: '1',
        historiaMin: '1',
        nombre: 'nuevo proyecto',
        peso: '0',
        pivote: '1',
        tiempo: '1',
        compromiso: '0',
        correo: cookieManager.getCookie('email'),
        fecha_inicio: '2022-09-14 00:00:00.000',
        cant_sprint: 0,
        id_poryect: int.parse(cookieManager.getCookie('id')));
    final _repository = GeneralRepository();
    var apiResponse = await _repository.insertProyect(_proyectObject);

    return apiResponse;
  }

  Future<ApiResponse> postProyectNew(id, name) async {
    Proyect _proyectObject = Proyect(
        duracion: '1',
        historiaMax: '1',
        historiaMin: '1',
        nombre: name,
        peso: '0',
        pivote: '1',
        tiempo: '1',
        compromiso: '0',
        correo: cookieManager.getCookie('email'),
        fecha_inicio: '2022-07-14 00:00:00.000',
        cant_sprint: 0,
        id_poryect: id);
    final _repository = GeneralRepository();
    var apiResponse = await _repository.insertProyect(_proyectObject);

    return apiResponse;
  }

  Future<ApiResponse> putProyect(
      String duracion,
      String equipoTrabajo,
      String historiaMax,
      String historiaMin,
      String nombre,
      String peso,
      String pivote,
      String tiempo,
      String compromiso,
      String fecha_inicio,
      int cant_sprint,
      int id) async {
    Proyect _proyectObject = Proyect(
        duracion: duracion,
        historiaMax: historiaMax,
        historiaMin: historiaMin,
        nombre: nombre,
        peso: peso,
        pivote: pivote,
        tiempo: tiempo,
        correo: cookieManager.getCookie('email'),
        compromiso: compromiso,
        fecha_inicio: fecha_inicio,
        cant_sprint: cant_sprint,
        proyectoId: id,
        id_poryect: int.parse(cookieManager.getCookie('id')));
    final _repository = GeneralRepository();
    var apiResponse = await _repository.updateProyect(_proyectObject);

    return apiResponse;
  }

  Future<ApiResponse> putProyectName(String nombre) async {
    Proyect _proyectObject = Proyect(
      nombre: nombre,
      proyectoId: 1,
    );
    final _repository = GeneralRepository();
    var apiResponse = await _repository.updateProyect(_proyectObject);

    return apiResponse;
  }

  Future getProyect() async {
    final _repository = GeneralRepository();
    var apiResponse = await _repository.getAllProyect();
    if (apiResponse.statusResponse == 200) {
      _initialData = apiResponse.object as List<Proyect>;
      _proyectController.add(_initialData!);
    } else {
      _initialData = [];
      _proyectController.add([]);
    }
  }

  Future getProyectCorreo() async {
    Proyect _proyectObject = Proyect(
        duracion: '1',
        historiaMax: '1',
        historiaMin: '1',
        nombre: 'Nombre del proyecto',
        peso: '0',
        pivote: '1',
        tiempo: '1',
        compromiso: '0',
        correo: cookieManager.getCookie('email'),
        fecha_inicio: '2022-07-14 00:00:00.000',
        cant_sprint: 0,
        id_poryect: int.parse(cookieManager.getCookie('id')));
    final _repository = GeneralRepository();
    var apiResponse = await _repository.getProyect(_proyectObject);
    if (apiResponse.statusResponse == 200) {
      _initialData = apiResponse.object as List<Proyect>;
      _proyectController.add(_initialData!);
    } else {
      _initialData = [];
      _proyectController.add([]);
    }
  }
}
